//use chrono::{DateTime, Utc};
use serde::{Deserialize, Serialize};
//use std::fmt;
use uuid::Uuid;

#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct CaseLoad {
    pub name: String,
    pub uuid: Uuid,
    pub client_uuids: Vec<String>,
    pub employees: Vec<String>,
}

impl CaseLoad {
    pub fn new(name: String) -> Self {
        Self {
            name: name,
            uuid: Uuid::new_v4(),
            employees: Vec::new(),
            client_uuids: Vec::new(),
        }
    }
    pub fn add_employee(&mut self, employee_username: String) {
        if !self.employees.contains(&employee_username) {
            self.employees.push(employee_username);
        }
    }
    pub fn add_client(&mut self, client_uuid: String) {
        if !self.client_uuids.contains(&client_uuid) {
            self.client_uuids.push(client_uuid);
        }
    }
    pub fn remove_client(&mut self, client_uuid: String) {
        self.client_uuids.retain(|x| x != &client_uuid);
    }
    pub fn remove_employee(&mut self, employee_username: String) {
        self.employees.retain(|x| x != &employee_username);
    }
}
