use chrono::{DateTime, NaiveDate, Utc};
use serde::{Deserialize, Serialize};
use std::collections::BTreeMap;
use std::fmt;
use uuid::Uuid;
//use crate::OfficeLocation;
#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct Client {
    pub first_name: String,
    pub auth_num: String,
    pub last_name: String,
    pub uuid: Uuid,
    pub service_area: ServiceArea,
    pub employer: Option<String>,
    pub supervisor: Option<String>,
    pub vr_counselor_support_coordinator: Option<String>,
    pub service_provided: ClientService,
    pub rate: f64,
    pub duration_authed: std::time::Duration,
    pub start_date: DateTime<Utc>,
    pub end_date: DateTime<Utc>,
    pub mmu: Option<usize>,
    pub billing_office: crate::reports::BillingOffice,
    pub service_type: Option<crate::reports::billing_tracking_vr_jd::Service>,
    pub assoc_service_uuids: BTreeMap<ClientService, String>,
    //pub office: crate::employee::OfficeLocation,
}

#[derive(Debug, Clone, Serialize, Deserialize, PartialEq, Eq, Ord, PartialOrd)]
pub enum BestContact {
    Client,
    Mother,
    Father,
    LegalGuardian,
    Friend,
    Sibling,
    GrandParent,
    Other,
}

impl BestContact {
    pub fn from(val: impl Into<String>) -> Option<Self> {
        let best_contact_string = val.into();
        let best_contact = best_contact_string.as_str();
        if best_contact == "client" {
            Some(Self::Client)
        } else if best_contact == "mother" {
            Some(Self::Mother)
        } else if best_contact == "father" {
            Some(Self::Father)
        } else if best_contact == "legal_guardian" {
            Some(Self::LegalGuardian)
        } else if best_contact == "friend" {
            Some(Self::Friend)
        } else if best_contact == "sibling" {
            Some(Self::Sibling)
        } else if best_contact == "grandparent" {
            Some(Self::GrandParent)
        } else if best_contact == "other" {
            Some(Self::Other)
        } else {
            None
        }
    }
}

#[derive(Debug, Clone, Serialize, Deserialize, PartialEq, Eq, Ord, PartialOrd)]
pub enum Benefits {
    SSI,
    SSDI,
    CDB,
    DWB,
    Medicare,
    Medicaid,
}

impl Benefits {
    pub fn from(val: impl Into<String>) -> Option<Self> {
        let benefits_string = val.into();
        let benefits = benefits_string.as_str();
        if benefits == "ssi" {
            Some(Self::SSI)
        } else if benefits == "ssdi" {
            Some(Self::SSDI)
        } else if benefits == "cdb" {
            Some(Self::CDB)
        } else if benefits == "dwb" {
            Some(Self::DWB)
        } else if benefits == "medicare" {
            Some(Self::Medicare)
        } else if benefits == "medicaid" {
            Some(Self::Medicaid)
        } else {
            None
        }
    }
}

#[derive(Debug, Clone, Serialize, Deserialize, PartialEq, Eq, Ord, PartialOrd)]
pub enum HighestEducation {
    Elementary,
    JrHigh,
    CertificateOfCompletion,
    PostHighCertificate,
    HighSchoolDiploma,
    GED,
    SomeCollege,
    Associates,
    Bachelors,
    Masters,
    PhD,
}

impl HighestEducation {
    pub fn from(val: impl Into<String>) -> Option<Self> {
        let highest_edu_string = val.into();
        let highest_edu = highest_edu_string.as_str();
        if highest_edu == "elementary" {
            Some(Self::Elementary)
        } else if highest_edu == "jrhigh" {
            Some(Self::JrHigh)
        } else if highest_edu == "cert_completion" {
            Some(Self::CertificateOfCompletion)
        } else if highest_edu == "post_high_cert" {
            Some(Self::PostHighCertificate)
        } else if highest_edu == "hsd" {
            Some(Self::HighSchoolDiploma)
        } else if highest_edu == "ged" {
            Some(Self::GED)
        } else if highest_edu == "some_college" {
            Some(Self::SomeCollege)
        } else if highest_edu == "assoc" {
            Some(Self::Associates)
        } else if highest_edu == "bachelors" {
            Some(Self::Bachelors)
        } else if highest_edu == "masters" {
            Some(Self::Masters)
        } else if highest_edu == "phd" {
            Some(Self::PhD)
        } else {
            None
        }
    }
}

#[derive(Debug, Clone, Serialize, Deserialize, PartialEq, Eq, Ord, PartialOrd)]
pub struct WaitingListClient {
    pub uuid: Uuid,
    pub intake_data: IntakeData,
}

impl WaitingListClient {
    pub fn new(intake_data: IntakeData) -> Self {
        Self {
            uuid: Uuid::new_v4(),
            intake_data: intake_data,
        }
    }
}

#[derive(Debug, Clone, Serialize, Deserialize, PartialEq, Eq, Ord, PartialOrd)]
pub struct IntakeData {
    pub ase_fname: String,
    pub ase_lname: String,
    pub client_fname: String,
    pub client_lname: String,
    pub best_contact_pn: String,
    pub best_pn_belongs: BestContact,
    pub best_pn_fname: String,
    pub best_pn_lname: String,
    pub secondary_pn: String,
    pub email: String,
    pub street_address: String,
    pub address_line_2: String,
    pub city: String,
    pub zip: String,
    pub state: String,
    pub country: String,
    pub dob: NaiveDate,
    pub ssn: String,
    pub ss_card: Option<Vec<u8>>,
    pub state_license: Option<Vec<u8>>,
    pub benefits: Vec<Benefits>,
    //pub medicare: bool,
    //pub medicaid: bool,
    pub sfb_other: bool,
    pub other_benefits: Option<String>,
    pub dspd: bool,
    pub sc_fname: Option<String>,
    pub sc_lname: Option<String>,
    pub dspd_wait: bool,
    pub ddeposit: bool,
    pub criminalb: bool,
    pub crim_back_details: Option<String>,
    pub mil_serve: bool,
    pub mil_serve_details: Option<String>,
    pub highest_edu: HighestEducation,
    pub driv_license: bool,
    pub reli_vehicle: bool,
    pub transp_assist: bool,
    pub state_id: bool,
    pub comf_diagn: bool,
    pub diags: Option<String>,
    pub behave_plan: bool,
    pub behave_fname: Option<String>,
    pub behave_lname: Option<String>,
    pub behave_email: Option<String>,
    pub behave_pn: Option<String>,
    pub resume: Option<Vec<u8>>,
    pub cover_letter: Option<Vec<u8>>,
    pub letter_rec: Option<Vec<u8>>,
    pub iep: Option<Vec<u8>>,
    pub behave_sup_plan: Option<Vec<u8>>,
    pub psy_eval: Option<Vec<u8>>,
}

#[derive(Debug, Clone, Serialize, Deserialize, PartialEq, Eq, Ord, PartialOrd)]
pub enum ClientService {
    JobCoaching,
    LifeSkills,
    JobDevelopment,
    ServiceBrokering,
    WsaTier1,
    WsaTier2,
    Discovery,
    SlNaturalSupports,
    SlHourly,
    SeIndividual,
    EmploymentPreparation,
    MotorTransPayment,
    DsIndividual,
    DsGroup,
}

impl std::fmt::Display for ClientService {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match self {
            Self::JobCoaching => write!(f, "jc"),
            Self::LifeSkills => write!(f, "ls"),
            Self::JobDevelopment => write!(f, "jd"),
            Self::ServiceBrokering => write!(f, "sb"),
            Self::WsaTier1 => write!(f, "wsatier1"),
            Self::WsaTier2 => write!(f, "wsatier2"),
            Self::Discovery => write!(f, "discovery"),
            Self::SlNaturalSupports => write!(f, "sln"),
            Self::SlHourly => write!(f, "slh"),
            Self::SeIndividual => write!(f, "sei"),
            Self::EmploymentPreparation => write!(f, "epr"),
            Self::MotorTransPayment => write!(f, "mtp"),
            Self::DsIndividual => write!(f, "dsi"),
            Self::DsGroup => write!(f, "dsg"),
        }
    }
}
impl ClientService {
    pub fn from(client_service: impl Into<String>) -> Self {
        let service = client_service.into();
        if service.to_lowercase() == "jc" {
            Self::JobCoaching
        } else if service.to_lowercase() == "ls" {
            Self::LifeSkills
        } else if service.to_lowercase() == "jd" {
            Self::JobDevelopment
        } else if service.to_lowercase() == "sb" {
            Self::ServiceBrokering
        } else if service.to_lowercase() == "wsatier1" {
            Self::WsaTier1
        } else if service.to_lowercase() == "wsatier2" {
            Self::WsaTier2
        } else if service.to_lowercase() == "discovery" {
            Self::Discovery
        } else if service.to_lowercase() == "sln" {
            Self::SlNaturalSupports
        } else if service.to_lowercase() == "slh" {
            Self::SlHourly
        } else if service.to_lowercase() == "sei" {
            Self::SeIndividual
        } else if service.to_lowercase() == "epr" {
            Self::EmploymentPreparation
        } else if service.to_lowercase() == "mtp" {
            Self::MotorTransPayment
        } else if service.to_lowercase() == "dsi" {
            Self::DsIndividual
        } else {
            //if service.to_lowercase() == "dsg" {
            Self::DsGroup
        }
    }
    pub fn client_service_string(&self) -> String {
        match self {
            Self::JobCoaching => String::from("JC"),
            Self::LifeSkills => String::from("LS"),
            Self::JobDevelopment => String::from("JD"),
            Self::ServiceBrokering => String::from("SB"),
            Self::WsaTier1 => String::from("WSA Tier 1"),
            Self::WsaTier2 => String::from("WSA Tier 2"),
            Self::Discovery => String::from("Discovery"),
            Self::SlNaturalSupports => String::from("SLN"),
            Self::SlHourly => String::from("SLH"),
            Self::SeIndividual => String::from("SEI"),
            Self::EmploymentPreparation => String::from("EPR"),
            Self::MotorTransPayment => String::from("MTP"),
            Self::DsIndividual => String::from("DSI"),
            Self::DsGroup => String::from("DSG"),
        }
    }
    pub fn is_authorization(&self) -> bool {
        match self {
            Self::JobCoaching => true,
            Self::LifeSkills => true,
            Self::JobDevelopment => true,
            Self::ServiceBrokering => true,
            Self::WsaTier1 => true,
            Self::WsaTier2 => true,
            Self::Discovery => true,
            Self::SlNaturalSupports => false,
            Self::SlHourly => false,
            Self::SeIndividual => false,
            Self::EmploymentPreparation => false,
            Self::MotorTransPayment => false,
            Self::DsIndividual => false,
            Self::DsGroup => false,
        }
    }
    pub fn list_all() -> Vec<ClientService> {
        vec![
            Self::JobCoaching,
            Self::LifeSkills,
            Self::JobDevelopment,
            Self::ServiceBrokering,
            Self::WsaTier1,
            Self::WsaTier2,
            Self::Discovery,
            Self::SlNaturalSupports,
            Self::SlHourly,
            Self::SeIndividual,
            Self::EmploymentPreparation,
            Self::MotorTransPayment,
            Self::DsIndividual,
            Self::DsGroup,
        ]
    }
    pub fn list_all_authorization_types() -> Vec<ClientService> {
        Self::list_all()
            .into_iter()
            .filter(|x| x.is_authorization())
            .collect()
    }
    pub fn list_all_budget_line_types() -> Vec<ClientService> {
        Self::list_all()
            .into_iter()
            .filter(|x| !x.is_authorization())
            .collect()
    }
}

impl Client {
    pub fn new(
        client_uuid: Uuid,
        auth_num: String,
        first_name: String,
        last_name: String,
        service_area: ServiceArea,
        employer: Option<String>,
        supervisor: Option<String>,
        vr_counselor_support_coordinator: Option<String>,
        service_provided: ClientService,
        rate: f64,
        duration_authed: std::time::Duration,
        start_date: DateTime<Utc>,
        end_date: DateTime<Utc>,
        mmu: Option<usize>,
        billing_office: crate::reports::BillingOffice,
        service_type: Option<crate::reports::billing_tracking_vr_jd::Service>,
        //office: crate::employee::OfficeLocation,
        assoc_service_uuids: BTreeMap<ClientService, String>,
    ) -> Self {
        Self {
            auth_num: auth_num,
            first_name: first_name,
            last_name: last_name,
            uuid: client_uuid,
            service_area: service_area,
            vr_counselor_support_coordinator: vr_counselor_support_coordinator,
            employer: employer,
            supervisor: supervisor,
            service_provided: service_provided,
            rate: rate,
            duration_authed: duration_authed,
            start_date: start_date,
            end_date: end_date,
            mmu: mmu,
            billing_office: billing_office,
            service_type: service_type,
            //office: office,
            assoc_service_uuids: assoc_service_uuids,
        }
    }
}
#[derive(Serialize, Deserialize, Debug, Clone, PartialEq, Eq)]
pub enum ServiceArea {
    Orem,
    SpanishFork,
    SaltLakeCity,
    StGeorge,
    Custom(String),
}

impl std::fmt::Display for ServiceArea {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match self {
            Self::Orem => write!(f, "orem"),
            Self::SpanishFork => write!(f, "spanishfork"),
            Self::SaltLakeCity => write!(f, "saltlakecity"),
            Self::StGeorge => write!(f, "stgeorge"),
            Self::Custom(x) => write!(f, "{}", x.replace(".", "").replace(" ", "").to_lowercase()),
        }
    }
}
impl ServiceArea {
    pub fn from(office: impl Into<String>, service_area_list: Vec<Self>) -> Option<Self> {
        let location = office.into();
        /*
        if location.to_lowercase() == "orem" {
            Self::Orem
        } else if location.to_lowercase() == "spanishfork"
            || location.to_lowercase() == "spanish fork"
        {
            Self::SpanishFork
        } else if location.to_lowercase() == "saltlakecity"
            || location.to_lowercase() == "salt lake city"
        {
            Self::SaltLakeCity
        } else if location.to_lowercase() == "stgeorge" || location.to_lowercase() == "st. george" {
            Self::StGeorge
        } else {

        }
        */
        for service_area in service_area_list {
            if format!("{service_area}")
                .replace(".", "")
                .replace(" ", "")
                .to_lowercase()
                == location
            {
                return Some(service_area);
            }
        }
        None
    }
    pub fn get_name(&self) -> String {
        match self {
            Self::Orem => String::from("Orem"),
            Self::SpanishFork => String::from("Spanish Fork"),
            Self::SaltLakeCity => String::from("Salt Lake City"),
            Self::StGeorge => String::from("St. George"),
            Self::Custom(x) => x.clone(),
        }
    }
    pub fn list_all() -> Vec<Self> {
        vec![
            Self::Orem,
            Self::SaltLakeCity,
            Self::SpanishFork,
            Self::StGeorge,
        ]
    }
}
/*
impl From<ServiceArea> for Option<crate::employee::OfficeLocation> {
    fn from(val: ServiceArea) -> Option<crate::employee::OfficeLocation> {
        match val {
            ServiceArea::Orem => Some(crate::employee::OfficeLocation::Orem),
            ServiceArea::SaltLakeCity => Some(crate::employee::OfficeLocation::SaltLakeCity),
            ServiceArea::StGeorge => Some(crate::employee::OfficeLocation::StGeorge),
            ServiceArea::SpanishFork => Some(crate::employee::OfficeLocation::SpanishFork),
            _ => None,
        }
    }
}
*/
