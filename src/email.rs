use lettre::{
    message::Mailbox,
    transport::smtp::authentication::{Credentials, Mechanism},
    SmtpTransport, Transport,
};

macro_rules! email_bcc {
    ($transport:expr, $mail_box:expr, $subject:expr, $message:expr, $( $bcc_mail_box:expr ),+) => {
        {
            let msg = lettre::message::MessageBuilder::new()
                .from(
                    "Allies Supported Employment <noreply@alliessupportedemployment.com>"
                        .parse()
                        .unwrap(),
                )
            .to($mail_box)
            $(
                .bcc($bcc_mail_box)
            )+
            .subject($subject)
            .body($message)
            .unwrap();
            $transport.send(&msg).unwrap();
        }
    };
}

macro_rules! email_cc {
    ($transport:expr, $mail_box:expr, $subject:expr, $message:expr, $( $bcc_mail_box:expr ),+) => {
        {
            let msg = lettre::message::MessageBuilder::new()
                .from(
                    "Allies Supported Employment <noreply@alliessupportedemployment.com>"
                        .parse()
                        .unwrap(),
                )
            .to($mail_box)
            $(
                .cc($bcc_mail_box)
            )+
            .subject($subject)
            .body($message)
            .unwrap();
            $transport.send(&msg).unwrap();
        }
    };
}

pub fn send_email(
    //token: impl Into<String>,
    transport: &SmtpTransport,
    //name: impl Into<String>,
    //email: impl Into<String>,
    mail_boxes: Vec<Mailbox>,
    use_cc: Option<bool>,
    subject: impl Into<String>,
    message: impl Into<String>,
) {
    //let name_email = format!("{} <{}>", name.into(), email.into());
    let mut msg = lettre::message::MessageBuilder::new()
        .from(
            "Allies Supported Employment <noreply@alliessupportedemployment.com>"
                .parse()
                .unwrap(),
        )
        //.to(name_email.as_str().parse().unwrap())
        .to(mail_boxes[0].clone());
    msg = match use_cc {
        Some(true) => {
            for m_box in mail_boxes[1..].into_iter() {
                msg = msg.cc(m_box.clone());
            }
            msg
        }
        Some(false) => {
            for m_box in mail_boxes[1..].into_iter() {
                msg = msg.bcc(m_box.clone());
            }
            msg
        }
        _ => msg,
    };
    let msg_built = msg
        .subject(subject.into().as_str())
        .body(message.into())
        .unwrap();
    /*
    SmtpTransport::starttls_relay("smtp.gmail.com")
        .unwrap()
        .credentials(Credentials::new(
            "noreply@alliessupportedemployment.com".to_string(),
            token.into(),
        ))
        .authentication(vec![Mechanism::Xoauth2])
        .build();
    */
    transport.send(&msg_built).unwrap();
}
