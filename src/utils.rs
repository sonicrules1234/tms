use actix_web::{http::header::ContentType, HttpResponse};
pub fn respond_html(html: impl Into<String>) -> HttpResponse {
    let data = html.into();
    //println!("{data}");
    HttpResponse::Ok()
        .content_type(ContentType::html())
        .body(data)
}
pub fn respond_text(text: impl Into<String>) -> HttpResponse {
    HttpResponse::Ok()
        .content_type(ContentType::plaintext())
        .body(text.into())
}
