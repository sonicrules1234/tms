use chrono::{DateTime, /*Datelike, TimeZone, Timelike,*/ Utc};
use lettre::message::Mailbox;
use serde::{Deserialize, Serialize};
#[derive(Debug, Clone, Serialize, Deserialize, PartialEq, Eq, Ord, PartialOrd)]
pub struct ScheduledNotification {
    pub subject: String,
    pub msg_type: NotificationType,
    //pub name: String,
    //pub email: String,
    pub mail_boxes: Vec<Mailbox>,
    pub use_cc: Option<bool>,
    pub message: String,
    pub scheduled_for: DateTime<Utc>,
    pub reschedule_in: Option<std::time::Duration>,
}

#[derive(Debug, Clone, Serialize, Deserialize, PartialEq, Eq, Ord, PartialOrd)]
pub enum NotificationType {
    WSA14_7(uuid::Uuid, DateTime<Utc>),
    Discovery30(uuid::Uuid, DateTime<Utc>),
}

impl ScheduledNotification {
    pub fn new(
        dur: chrono::Duration,
        reschedule_in: Option<std::time::Duration>,
        msg_type: NotificationType,
        mail_boxes: Vec<Mailbox>,
        use_cc: Option<bool>,
        //name: impl Into<String>,
        //email: impl Into<String>,
        subject: impl Into<String>,
        message: impl Into<String>,
    ) -> Self {
        //let now = Utc::now();
        let date_time = crate::get_now_minutes();
        //Utc.ymd(now.year(), now.month(), now.day())
        //    .and_hms(now.hour(), now.minute(), 0);
        let then = date_time.checked_add_signed(dur).unwrap();
        Self {
            msg_type: msg_type,
            subject: subject.into(),
            use_cc: use_cc,
            mail_boxes: mail_boxes,
            //name: name.into(),
            //email: email.into(),
            message: message.into(),
            scheduled_for: then,
            reschedule_in: reschedule_in,
        }
    }
    pub fn from_datetime(
        scheduled_for: DateTime<Utc>,
        reschedule_in: Option<std::time::Duration>,
        msg_type: NotificationType,
        mail_boxes: Vec<Mailbox>,
        use_cc: Option<bool>,
        subject: impl Into<String>,
        message: impl Into<String>,
    ) -> Self {
        Self {
            msg_type: msg_type,
            subject: subject.into(),
            use_cc: use_cc,
            mail_boxes: mail_boxes,
            //name: name.into(),
            //email: email.into(),
            message: message.into(),
            scheduled_for: scheduled_for,
            reschedule_in: reschedule_in,
        }
    }
}
