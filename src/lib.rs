pub mod employee;
pub use employee::*;
pub mod caseload;
pub mod client;
pub mod email;
pub mod reports;
pub mod scheduled_notifications;
pub mod utils;
use chrono::{DateTime, Datelike, TimeZone, Timelike, Utc};

pub fn get_now_minutes() -> DateTime<Utc> {
    let now = Utc::now();
    Utc.ymd(now.year(), now.month(), now.day())
        .and_hms(now.hour(), now.minute(), 0)
}
