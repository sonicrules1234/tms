use acid_store::repo::{value::ValueRepo, Commit, OpenMode, OpenOptions, ResourceLimit};
use acid_store::store::DirectoryConfig;
//use actix_multipart::Multipart;
use actix_web::{
    cookie::Cookie, get, post, services, web, App, HttpRequest, HttpResponse, HttpServer,
};
use askama::Template;
use chrono::Weekday;
use crossbeam_channel::{unbounded, Receiver, Sender};
use futures_util::StreamExt as _;
use google_oauth2::GoogleOAuth2;
use headless_chrome::{Browser, LaunchOptionsBuilder};
use lettre::{
    message::Mailbox,
    transport::smtp::authentication::{Credentials, Mechanism},
    Address, SmtpTransport, Transport,
};
use rustls::{
    //client::{ServerCertVerified, ServerCertVerifier},
    Certificate,
    /*ClientConfig,*/ PrivateKey,
    ServerConfig, //, ServerName,
};
use rustls_pemfile::{certs, pkcs8_private_keys};
use serde::{Deserialize, Serialize};
use std::convert::TryInto;
use std::io::{BufReader, Read, Write};
use tms::client::WaitingListClient;
use tms::*;
//use tms::client::ClientService;
use std::collections::BTreeMap;
//use tms::*;
//use std::collections::BTreeMap;
use chrono::{offset::Utc, DateTime, Datelike, NaiveDate, TimeZone, Timelike};
use chrono_tz::{Tz, TZ_VARIANTS};
use std::path::PathBuf;
use uuid::Uuid;
const LOGO_PNG: &[u8] = include_bytes!("../static/logo.png");
const DWS_USOR_PNG: &[u8] = include_bytes!("../static/dws_usor.png");
const PROUD_PARTNER_PNG: &[u8] = include_bytes!("../static/proud_partner.png");
//const UC: &str = "X";
macro_rules! get_value {
    ($x:expr, $y:literal) => {
        read_value(get_app_data($x), format!($y).as_str())
    };
}

macro_rules! key_exists {
    ($x:expr, $y:literal) => {
        key_exists(get_app_data($x), format!($y).as_str())
    };
}

macro_rules! get_value_tx {
    ($x:expr, $y:literal) => {
        read_value_tx($x, format!($y).as_str())
    };
}

macro_rules! key_exists_tx {
    ($x:expr, $y:literal) => {
        key_exists_tx($x, format!($y).as_str())
    };
}

fn calculate_num_days_month(date: NaiveDate) -> usize {
    let end_date: NaiveDate;
    if date.month() == 12 {
        end_date = NaiveDate::from_ymd(date.year() + 1, 1, 1);
    } else {
        end_date = NaiveDate::from_ymd(date.year(), date.month() + 1, 1);
    }
    end_date
        .signed_duration_since(NaiveDate::from_ymd(date.year(), date.month(), 1))
        .num_days()
        .try_into()
        .unwrap()
}

fn calculate_hour_quarters(duration: chrono::Duration) -> String {
    let num_hours = duration.num_hours();
    let minutes = duration
        .checked_sub(&chrono::Duration::hours(num_hours))
        .unwrap();
    //let num_minutes = minutes.num_minutes();
    if minutes
        < chrono::Duration::minutes(7)
            .checked_add(&chrono::Duration::seconds(30))
            .unwrap()
    {
        format!("{num_hours}")
    } else if minutes
        < chrono::Duration::minutes(22)
            .checked_add(&chrono::Duration::seconds(30))
            .unwrap()
    {
        format!("{num_hours}.25")
    } else if minutes
        < chrono::Duration::minutes(37)
            .checked_add(&chrono::Duration::seconds(30))
            .unwrap()
    {
        format!("{num_hours}.5")
    } else if minutes
        < chrono::Duration::minutes(52)
            .checked_add(&chrono::Duration::seconds(30))
            .unwrap()
    {
        format!("{num_hours}.75")
    } else {
        format!("{num_hours}")
    }
}

async fn update_monthly_jc_report(req: HttpRequest, username: String, client_uuid: String) {
    if key_exists!(
        req.clone(),
        "/client_list/{client_uuid}/report_data/ongoing_supports/jc_monthly_partial"
    )
    .await
    {
        let time_zone: Tz = get_value!(req.clone(), "/employee_list/{username}/timezone").unwrap();
        let partial_report: reports::ongoing_supports::monthly_jc_report::ReportBuilder =
            get_value!(
                req.clone(),
                "/client_list/{client_uuid}/report_data/ongoing_supports/jc_monthly_partial"
            )
            .unwrap();
        if partial_report.is_overdue(time_zone) {
            if !key_exists!(
                req.clone(),
                "/client_list/{client_uuid}/report_data/overdue/ongoing_supports/jc_monthly"
            )
            .await
            {
                write_value(get_app_data(req.clone()), format!("/client_list/{client_uuid}/report_data/overdue/ongoing_supports/jc_monthly").as_str(), Vec::<reports::ongoing_supports::monthly_jc_report::ReportBuilder>::new()).await.unwrap();
            }
            let mut overdue_reports: Vec<
                reports::ongoing_supports::monthly_jc_report::ReportBuilder,
            > = get_value!(
                req.clone(),
                "/client_list/{client_uuid}/report_data/overdue/ongoing_supports/jc_monthly"
            )
            .unwrap();
            overdue_reports.push(partial_report.clone());
            // TODO: Make these two a batch operation
            write_value(
                get_app_data(req.clone()),
                format!(
                    "/client_list/{client_uuid}/report_data/overdue/ongoing_supports/jc_monthly"
                )
                .as_str(),
                overdue_reports.clone(),
            )
            .await
            .unwrap();
            remove_value(
                get_app_data(req.clone()),
                format!(
                    "/client_list/{client_uuid}/report_data/ongoing_supports/jc_monthly_partial"
                ),
            )
            .await
            .unwrap();
        }
    }
}

enum SyncMessage {
    Remove(String, Sender<SyncMessage>),
    Insert(String, Vec<u8>, Sender<SyncMessage>),
    Get(String, Sender<SyncMessage>),
    Shutdown,
    GetSuccess(Vec<u8>),
    InsertSuccess,
    BatchInsert(Vec<(String, Vec<u8>)>, Sender<SyncMessage>),
    BatchInsertError(acid_store::Error),
    BatchInsertSuccess,
    RemoveResponse(bool),
    InsertError(acid_store::Error),
    GetError(acid_store::Error),
    KeyExists(String, Sender<SyncMessage>),
    KeyExistsResponse(bool),
    AppendToSchedule(
        scheduled_notifications::ScheduledNotification,
        Sender<SyncMessage>,
    ),
    RemoveFromSchedule(
        scheduled_notifications::ScheduledNotification,
        Sender<SyncMessage>,
    ),
    SchedAppendResponse(Option<acid_store::Error>),
    SchedRemoveResponse(Option<acid_store::Error>),
}

macro_rules! batch_write_value {
    ($req:expr, $( ($key:literal, $val:expr) ),+) => {
        {
            let mut send_this_vec: Vec<(String, Vec<u8>)> = Vec::new();
            $(
                send_this_vec.push((format!($key), rmp_serde::to_vec(&$val).unwrap()));
            )+
            let (tx, rx) = unbounded::<SyncMessage>();
            $req
                .send(SyncMessage::BatchInsert(send_this_vec, tx.clone()))
                .unwrap();
            match rx.recv().unwrap() {
                SyncMessage::BatchInsertSuccess => Ok(()),
                SyncMessage::BatchInsertError(e) => Err(e),
                _ => panic!("Received response we shouldn't have for batch_write_value"),
            }
        }
    };
}

macro_rules! batch_write_value_tx {
    ($db_tx:expr, $( ($key:literal, $val:expr) ),+) => {
        {
            let mut send_this_vec: Vec<(String, Vec<u8>)> = Vec::new();
            $(
                send_this_vec.push((format!($key), rmp_serde::to_vec(&$val).unwrap()));
            )+
            let (tx, rx) = unbounded::<SyncMessage>();
            $db_tx
                .send(SyncMessage::BatchInsert(send_this_vec, tx.clone()))
                .unwrap();
            match rx.recv().unwrap() {
                SyncMessage::BatchInsertSuccess => Ok(()),
                SyncMessage::BatchInsertError(e) => Err(e),
                _ => panic!("Received response we shouldn't have for batch_write_value"),
            }
        }
    };
}
async fn check_cookies_and_username<P, F>(
    req: HttpRequest,
    f: P,
    permission: impl Into<String>,
) -> HttpResponse
where
    F: std::future::Future<Output = actix_web::HttpResponse>,
    P: Fn(HttpRequest, String) -> F,
{
    match check_cookies(req.clone()).await {
        Ok(username) => {
            if get_employee_record(req.clone(), username.clone())
                .unwrap()
                .unwrap()
                .is_allowed(permission.into())
            {
                f(req.clone(), username.clone()).await
            } else {
                error_page("Employee Permissions", "Access Denied").await
            }
        }
        Err(SessionError::NotRegistered) => {
            error_page("Employee Sessions", "No such employee").await
        }
        Err(SessionError::SessionIDMismatch) => HttpResponse::SeeOther()
            .insert_header(("Location", "/"))
            .finish(),
        Err(SessionError::NotLoggedIn) => redirect("/"),
        Err(SessionError::NoCookies) => HttpResponse::SeeOther()
            .insert_header(("Location", "/"))
            .finish(),
    }
}

async fn check_cookies_and_username_req_form<P, F, T>(
    req: HttpRequest,
    form: web::Form<T>,
    f: P,
    permission: impl Into<String>,
) -> HttpResponse
where
    F: std::future::Future<Output = actix_web::HttpResponse>,
    P: Fn(String, HttpRequest, web::Form<T>) -> F,
{
    match check_cookies(req.clone()).await {
        Ok(username) => {
            if get_employee_record(req.clone(), username.clone())
                .unwrap()
                .unwrap()
                .is_allowed(permission.into())
            {
                f(username.clone(), req.clone(), form).await
            } else {
                error_page("Employee Permissions", "Access Denied").await
            }
        }
        Err(SessionError::NotRegistered) => {
            error_page("Employee Sessions", "No such employee").await
        }
        Err(SessionError::SessionIDMismatch) => HttpResponse::SeeOther()
            .insert_header(("Location", "/"))
            .finish(),
        Err(SessionError::NotLoggedIn) => redirect("/"),
        Err(SessionError::NoCookies) => HttpResponse::SeeOther()
            .insert_header(("Location", "/"))
            .finish(),
    }
}

async fn check_cookies_and_username_req_payload<P, F>(
    req: HttpRequest,
    payload: web::Payload,
    f: P,
    permission: impl Into<String>,
) -> HttpResponse
where
    F: std::future::Future<Output = actix_web::HttpResponse>,
    P: Fn(String, HttpRequest, web::Payload) -> F,
{
    match check_cookies(req.clone()).await {
        Ok(username) => {
            if get_employee_record(req.clone(), username.clone())
                .unwrap()
                .unwrap()
                .is_allowed(permission.into())
            {
                f(username.clone(), req.clone(), payload).await
            } else {
                error_page("Employee Permissions", "Access Denied").await
            }
        }
        Err(SessionError::NotRegistered) => {
            error_page("Employee Sessions", "No such employee").await
        }
        Err(SessionError::SessionIDMismatch) => HttpResponse::SeeOther()
            .insert_header(("Location", "/"))
            .finish(),
        Err(SessionError::NotLoggedIn) => redirect("/"),
        Err(SessionError::NoCookies) => HttpResponse::SeeOther()
            .insert_header(("Location", "/"))
            .finish(),
    }
}

async fn check_cookies_and_username_path<P, F, T>(
    req: HttpRequest,
    path: web::Path<T>,
    f: P,
    permission: impl Into<String>,
) -> HttpResponse
where
    F: std::future::Future<Output = actix_web::HttpResponse>,
    P: Fn(String, HttpRequest, web::Path<T>) -> F,
{
    match check_cookies(req.clone()).await {
        Ok(username) => {
            if get_employee_record(req.clone(), username.clone())
                .unwrap()
                .unwrap()
                .is_allowed(permission.into())
            {
                f(username.clone(), req.clone(), path).await
            } else {
                error_page("Employee Permissions", "Access Denied").await
            }
        }
        Err(SessionError::NotRegistered) => {
            error_page("Employee Sessions", "No such employee").await
        }
        Err(SessionError::SessionIDMismatch) => HttpResponse::SeeOther()
            .insert_header(("Location", "/"))
            .finish(),
        Err(SessionError::NotLoggedIn) => redirect("/"),
        Err(SessionError::NoCookies) => HttpResponse::SeeOther()
            .insert_header(("Location", "/"))
            .finish(),
    }
}

async fn write_value(
    app_data: web::Data<Sender<SyncMessage>>,
    key: impl Into<String>,
    val: impl Serialize,
) -> Result<(), acid_store::Error> {
    let key_string = key.into();
    let (tx, rx) = unbounded::<SyncMessage>();
    app_data
        .send(SyncMessage::Insert(
            key_string.clone(),
            rmp_serde::to_vec(&val).unwrap(),
            tx.clone(),
        ))
        .unwrap();
    match rx.recv().unwrap() {
        SyncMessage::InsertSuccess => Ok(()),
        SyncMessage::InsertError(e) => Err(e),
        _ => panic!("Received response we shouldn't have for write_value(\"{key_string}\")"),
    }
}

async fn append_to_schedule(
    //req: HttpRequest,
    db_tx: Sender<SyncMessage>,
    scheduled_notification: scheduled_notifications::ScheduledNotification,
) -> Result<(), acid_store::Error> {
    //let app_data = get_app_data(req);
    let (tx, rx) = unbounded::<SyncMessage>();
    //app_data
    db_tx
        .send(SyncMessage::AppendToSchedule(
            scheduled_notification,
            tx.clone(),
        ))
        .unwrap();
    match rx.recv().unwrap() {
        SyncMessage::SchedAppendResponse(Some(e)) => Err(e),
        SyncMessage::SchedAppendResponse(None) => Ok(()),
        _ => panic!("Received response we shouldn't have for append_to_schedule"),
    }
}

async fn remove_from_schedule(
    //req: HttpRequest,
    db_tx: Sender<SyncMessage>,
    scheduled_notification: scheduled_notifications::ScheduledNotification,
) -> Result<(), acid_store::Error> {
    //let app_data = get_app_data(req);
    let (tx, rx) = unbounded::<SyncMessage>();
    //app_data
    db_tx
        .send(SyncMessage::RemoveFromSchedule(
            scheduled_notification,
            tx.clone(),
        ))
        .unwrap();
    match rx.recv().unwrap() {
        SyncMessage::SchedRemoveResponse(Some(e)) => Err(e),
        SyncMessage::SchedRemoveResponse(None) => Ok(()),
        _ => panic!("Received response we shouldn't have for remove_from_schedule"),
    }
}

async fn append_to_schedule_req(
    //req: HttpRequest,
    req: HttpRequest,
    scheduled_notification: scheduled_notifications::ScheduledNotification,
) -> Result<(), acid_store::Error> {
    //let app_data = get_app_data(req);
    let (tx, rx) = unbounded::<SyncMessage>();
    let db_tx = get_app_data(req);
    db_tx
        .send(SyncMessage::AppendToSchedule(
            scheduled_notification,
            tx.clone(),
        ))
        .unwrap();
    match rx.recv().unwrap() {
        SyncMessage::SchedAppendResponse(Some(e)) => Err(e),
        SyncMessage::SchedAppendResponse(None) => Ok(()),
        _ => panic!("Received response we shouldn't have for append_to_schedule"),
    }
}

async fn remove_from_schedule_req(
    //req: HttpRequest,
    req: HttpRequest,
    scheduled_notification: scheduled_notifications::ScheduledNotification,
) -> Result<(), acid_store::Error> {
    let db_tx = get_app_data(req);
    let (tx, rx) = unbounded::<SyncMessage>();
    //app_data
    db_tx
        .send(SyncMessage::RemoveFromSchedule(
            scheduled_notification,
            tx.clone(),
        ))
        .unwrap();
    match rx.recv().unwrap() {
        SyncMessage::SchedRemoveResponse(Some(e)) => Err(e),
        SyncMessage::SchedRemoveResponse(None) => Ok(()),
        _ => panic!("Received response we shouldn't have for remove_from_schedule"),
    }
}
async fn remove_value(
    app_data: web::Data<Sender<SyncMessage>>,
    key: impl Into<String>,
) -> anyhow::Result<bool> {
    let key_string = key.into();
    let (tx, rx) = unbounded::<SyncMessage>();
    app_data
        .send(SyncMessage::Remove(key_string.clone(), tx.clone()))
        .unwrap();
    match rx.recv().unwrap() {
        SyncMessage::RemoveResponse(success) => Ok(success),
        _ => panic!("Received response we shouldn't have for remove_value(\"{key_string}\")"),
    }
}

async fn key_exists(app_data: web::Data<Sender<SyncMessage>>, key: impl Into<String>) -> bool {
    let key_string = key.into();
    let (tx, rx) = unbounded::<SyncMessage>();
    app_data
        .send(SyncMessage::KeyExists(key_string.clone(), tx.clone()))
        .unwrap();
    match rx.recv().unwrap() {
        SyncMessage::KeyExistsResponse(success) => success,
        _ => panic!("Received response we shouldn't have for remove_value(\"{key_string}\")"),
    }
}
/*
fn form_de<V: serde::de::DeserializeOwned>(chunk: Vec<u8>) -> V {
    V::deserialize(&chunk).unwrap()
}
*/
fn read_value<V: serde::de::DeserializeOwned>(
    app_data: web::Data<Sender<SyncMessage>>,
    key: &str,
) -> Result<V, acid_store::Error> {
    //GLOBAL_DATA.read().await.get::<str, V>(key)i
    let (tx, rx) = unbounded::<SyncMessage>();
    app_data
        .send(SyncMessage::Get(key.to_string(), tx.clone()))
        .unwrap();
    match rx.recv().unwrap() {
        SyncMessage::GetSuccess(val) => Ok(rmp_serde::from_slice(val.as_slice()).unwrap()),
        SyncMessage::GetError(e) => Err(e),
        _ => panic!("Received response we shouldn't have for read_value(\"{key}\")"),
    }
}

async fn write_value_tx(
    db_tx: Sender<SyncMessage>,
    key: impl Into<String>,
    val: impl Serialize,
) -> Result<(), acid_store::Error> {
    let key_string = key.into();
    let (tx, rx) = unbounded::<SyncMessage>();
    db_tx
        .send(SyncMessage::Insert(
            key_string.clone(),
            rmp_serde::to_vec(&val).unwrap(),
            tx.clone(),
        ))
        .unwrap();
    match rx.recv().unwrap() {
        SyncMessage::InsertSuccess => Ok(()),
        SyncMessage::InsertError(e) => Err(e),
        _ => panic!("Received response we shouldn't have for write_value(\"{key_string}\")"),
    }
}

async fn remove_value_tx(
    db_tx: Sender<SyncMessage>,
    key: impl Into<String>,
) -> anyhow::Result<bool> {
    let key_string = key.into();
    let (tx, rx) = unbounded::<SyncMessage>();
    db_tx
        .send(SyncMessage::Remove(key_string.clone(), tx.clone()))
        .unwrap();
    match rx.recv().unwrap() {
        SyncMessage::RemoveResponse(success) => Ok(success),
        _ => panic!("Received response we shouldn't have for remove_value(\"{key_string}\")"),
    }
}

async fn key_exists_tx(db_tx: Sender<SyncMessage>, key: impl Into<String>) -> bool {
    let key_string = key.into();
    let (tx, rx) = unbounded::<SyncMessage>();
    db_tx
        .send(SyncMessage::KeyExists(key_string.clone(), tx.clone()))
        .unwrap();
    match rx.recv().unwrap() {
        SyncMessage::KeyExistsResponse(success) => success,
        _ => panic!("Received response we shouldn't have for remove_value(\"{key_string}\")"),
    }
}

fn read_value_tx<V: serde::de::DeserializeOwned>(
    db_tx: Sender<SyncMessage>,
    key: &str,
) -> Result<V, acid_store::Error> {
    //GLOBAL_DATA.read().await.get::<str, V>(key)i
    let (tx, rx) = unbounded::<SyncMessage>();
    db_tx
        .send(SyncMessage::Get(key.to_string(), tx.clone()))
        .unwrap();
    match rx.recv().unwrap() {
        SyncMessage::GetSuccess(val) => Ok(rmp_serde::from_slice(val.as_slice()).unwrap()),
        SyncMessage::GetError(e) => Err(e),
        _ => panic!("Received response we shouldn't have for read_value(\"{key}\")"),
    }
}
#[derive(Template)]
#[template(path = "setup.html")]
struct SetupPageTemplate {
    offices: Vec<String>,
}

#[derive(Template)]
#[template(path = "client_intake.html")]
struct ClientIntakePageTemplate {
    success: Option<bool>,
}

#[derive(Template)]
#[template(path = "jc_tracker_report_export.html")]
struct JCTrackerReportExportPageTemplate {
    navigation_bar: NavBarTemplate,
}

#[derive(Template)]
#[template(path = "choose_caseload.html")]
struct ChooseCaseLoadPageTemplate {
    caseloads: Vec<caseload::CaseLoad>,
}

#[derive(Template)]
#[template(path = "cie_job_placement.html")]
struct CIEJobPlacementPageTemplate {
    navigation_bar: NavBarTemplate,
    success: Option<bool>,
}

#[get("/jc_tracker_report_export")]
async fn jc_tracker_report_export_page(req: HttpRequest) -> HttpResponse {
    check_cookies_and_username(
        req.clone(),
        jc_tracker_report_export_page_internal,
        "report",
    )
    .await
}

async fn jc_tracker_report_export_page_internal(
    req: HttpRequest,
    username: String,
) -> HttpResponse {
    utils::respond_html(
        JCTrackerReportExportPageTemplate {
            navigation_bar: NavBarTemplate::new(req.clone(), username),
        }
        .render()
        .unwrap(),
    )
}

#[derive(Debug, Clone, Serialize, Deserialize)]
struct CIEJobPlacementPost {
    employer: String,
    employer_address: String,
    job_title: String,
    start_date: NaiveDate,
    wage: f32,
    hours_per_week: String,
    benefits: String,
    supervisor_name: String,
    supervisor_contact: String,
    q1: Option<String>,
    q2: Option<String>,
    q3: Option<String>,
    q3_exp: String,
    q4: Option<String>,
    q4_exp: String,
    q5: Option<String>,
    q6: Option<String>,
    q7: Option<String>,
    q7_exp: String,
    q8: Option<String>,
    q8_exp: String,
    q9: Option<String>,
    q9_exp: String,
    crp_signature: String,
    start_hour: usize,
    start_minute: usize,
    start_am: String,
    end_hour: usize,
    end_minute: usize,
    end_am: String,
}

#[derive(Debug, Clone, Serialize, Deserialize)]
struct ChangeCaseLoadPost {
    caseload: String,
}

#[derive(Debug, Deserialize /*, MultipartForm*/)]
struct ClientIntakePost {
    ase_fname: String,
    ase_lname: String,
    client_fname: String,
    client_lname: String,
    best_contact_pn: String,
    best_pn_belongs: String,
    best_pn_fname: String,
    best_pn_lname: String,
    secondary_pn: String,
    email: String,
    street_address: String,
    address_line_2: String,
    city: String,
    state: String,
    zip: String,
    country: String,
    dob: NaiveDate,
    ssn: String,
    //ss_card: Option<File>,
    //state_license: Option<File>,
    ss_card: Option<String>,
    state_license: Option<String>,
    ssi: Option<String>,
    ssdi: Option<String>,
    cdb: Option<String>,
    dwb: Option<String>,
    medicare: Option<String>,
    medicaid: Option<String>,
    sfb_other: Option<String>,
    other_benefits: String,
    dspd: String,
    sc_fname: String,
    sc_lname: String,
    dspd_wait: String,
    ddeposit: String,
    criminalb: String,
    crim_back_details: String,
    mil_serve: String,
    mil_serve_details: String,
    highest_edu: String,
    driv_license: String,
    reli_vehicle: String,
    transp_assist: String,
    state_id: String,
    comf_diagn: String,
    diags: String,
    behave_plan: String,
    behave_fname: String,
    behave_lname: String,
    behave_email: String,
    behave_pn: String,
    /*
    resume: Option<File>,
    cover_letter: Option<File>,
    letter_rec: Option<File>,
    iep: Option<File>,
    behave_sup_plan: Option<File>,
    psy_eval: Option<File>,
    */
    resume: Option<String>,
    cover_letter: Option<String>,
    letter_rec: Option<String>,
    iep: Option<String>,
    behave_sup_plan: Option<String>,
    psy_eval: Option<String>,
}

fn string_or_none(yes_no: bool, val: String) -> Option<String> {
    if yes_no {
        //== String::from("yes") {
        Some(val.clone())
    } else {
        None
    }
}

#[post("/client_intake.rs")]
async fn client_intake_post(
    req: HttpRequest,
    form: web::Form<ClientIntakePost>, /*payload: Multipart*/
) -> HttpResponse {
    //let z = String::deserialize(form);
    /*
    while let Some(item) = payload.next().await {
        let mut field = item.unwrap();

        // Field in turn is stream of *Bytes* object
        while let Some(chunk) = field.next().await {
            //println!("-- CHUNK: \n{:?}", std::str::from_utf8(&chunk?));
        }
    }
    */
    let best_contact_pn_option = client::BestContact::from(form.best_pn_belongs.clone());
    if best_contact_pn_option.is_none() {
        return error_page(
            "Client Intake Systems",
            "Invalid or no selection for best contact relation",
        )
        .await;
    }
    let highest_edu_option = client::HighestEducation::from(form.highest_edu.clone());
    if highest_edu_option.is_none() {
        return error_page(
            "Client Intake Systems",
            "Invalid or no selection for highest education level",
        )
        .await;
    }
    let mut benefits: Vec<client::Benefits> = Vec::new();
    if form.medicare.is_some() {
        benefits.push(client::Benefits::Medicare);
    }
    if form.medicaid.is_some() {
        benefits.push(client::Benefits::Medicaid);
    }
    if form.ssi.is_some() {
        benefits.push(client::Benefits::SSI);
    }
    if form.ssdi.is_some() {
        benefits.push(client::Benefits::SSDI);
    }
    if form.cdb.is_some() {
        benefits.push(client::Benefits::CDB);
    }
    if form.dwb.is_some() {
        benefits.push(client::Benefits::DWB);
    }
    let numbers = ['0', '1', '2', '3', '4', '5', '6', '7', '8', '9'];
    let data = client::IntakeData {
        ase_fname: form.ase_fname.clone(),
        ase_lname: form.ase_lname.clone(),
        client_fname: form.client_fname.clone(),
        client_lname: form.client_lname.clone(),
        best_contact_pn: form
            .best_contact_pn
            .chars()
            .filter(|x| numbers.contains(x))
            .collect::<String>(),
        best_pn_belongs: client::BestContact::from(form.best_pn_belongs.clone()).unwrap(),
        best_pn_fname: form.best_pn_fname.clone(),
        best_pn_lname: form.best_pn_lname.clone(),
        secondary_pn: form
            .secondary_pn
            .chars()
            .filter(|x| numbers.contains(x))
            .collect::<String>(),
        email: form.email.clone(), // TODO: Check if valid, and if not, give error
        street_address: form.street_address.clone(),
        address_line_2: form.address_line_2.clone(),
        city: form.city.clone(),
        zip: form.zip.clone(),
        state: form.state.clone(),
        country: form.country.clone(),
        dob: form.dob,
        ssn: form
            .ssn
            .chars()
            .filter(|x| numbers.contains(x))
            .collect::<String>(),
        ss_card: match &form.ss_card {
            Some(x) => Some(Vec::new()),
            None => None,
        },
        state_license: match &form.state_license {
            Some(x) => Some(Vec::new()),
            None => None,
        },
        //ssb: Vec::new(), //client::SSBenefits::from(form.ssb.clone()),
        //medicare: form.medicare.is_some(),
        //medicaid: form.medicaid.is_some(),
        benefits: benefits,
        sfb_other: form.sfb_other.is_some(),
        other_benefits: string_or_none(form.sfb_other.is_some(), form.other_benefits.clone()),
        dspd: form.dspd == "yes",
        sc_fname: string_or_none(form.dspd == "yes", form.sc_fname.clone()),
        sc_lname: string_or_none(form.dspd == "yes", form.sc_lname.clone()),
        dspd_wait: form.dspd_wait == "yes",
        ddeposit: form.ddeposit == "yes",
        criminalb: form.criminalb == "yes",
        crim_back_details: string_or_none(form.criminalb == "yes", form.crim_back_details.clone()),
        mil_serve: form.mil_serve == "yes",
        mil_serve_details: string_or_none(form.mil_serve == "yes", form.mil_serve_details.clone()),
        highest_edu: client::HighestEducation::from(form.highest_edu.clone()).unwrap(),
        driv_license: form.driv_license == "yes",
        reli_vehicle: form.reli_vehicle == "yes",
        transp_assist: form.transp_assist == "yes",
        state_id: form.state_id == "yes",
        comf_diagn: form.comf_diagn == "yes",
        diags: string_or_none(form.comf_diagn == "yes", form.diags.clone()),
        behave_plan: form.behave_plan == "yes",
        behave_fname: string_or_none(form.behave_plan == "yes", form.behave_fname.clone()),
        behave_lname: string_or_none(form.behave_plan == "yes", form.behave_lname.clone()),
        behave_email: string_or_none(form.behave_plan == "yes", form.behave_email.clone()),
        behave_pn: string_or_none(
            form.behave_plan == "yes",
            form.behave_pn
                .clone()
                .chars()
                .filter(|x| numbers.contains(x))
                .collect::<String>(),
        ),
        resume: match &form.resume {
            Some(x) => Some(Vec::new()),
            //Some(x) => Some(x.bytes().clone()),
            None => None,
        },
        cover_letter: match &form.cover_letter {
            //Some(x) => Some(x.bytes().clone()),
            Some(x) => Some(Vec::new()),
            None => None,
        },
        letter_rec: match &form.letter_rec {
            //Some(x) => Some(x.bytes().clone()),
            Some(x) => Some(Vec::new()),
            None => None,
        },
        iep: match &form.iep {
            //Some(x) => Some(x.bytes().clone()),
            Some(x) => Some(Vec::new()),
            None => None,
        },
        behave_sup_plan: match &form.behave_sup_plan {
            //Some(x) => Some(x.bytes().clone()),
            Some(x) => Some(Vec::new()),
            None => None,
        },
        psy_eval: match &form.psy_eval {
            //Some(x) => Some(x.bytes().clone()),
            Some(x) => Some(Vec::new()),
            None => None,
        },
    };
    let data_clone = data.clone();
    if vec![
        data_clone.street_address,
        data_clone.city,
        data_clone.zip,
        data_clone.state,
        data_clone.country,
    ]
    .contains(&String::new())
    {
        return error_page("Client Intake Systems", "You must fill out your address.").await;
    }
    if data_clone.sfb_other && data_clone.other_benefits == Some(String::new()) {
        return error_page("Client Intake Systems", "Other benefits text box is empty").await;
    }
    if data_clone.best_contact_pn != String::new() {
        if data_clone.best_pn_belongs != client::BestContact::Client {
            if vec![data_clone.best_pn_fname, data_clone.best_pn_lname].contains(&String::new()) {
                return error_page("Client Intake Systems", "Need name of best contact person")
                    .await;
            }
        }
    }
    if data_clone.crim_back_details == Some(String::new()) {
        return error_page(
            "Client Intake Systems",
            "Missing criminal background details",
        )
        .await;
    }
    if data_clone.mil_serve_details == Some(String::new()) {
        return error_page("Client Intake Systems", "Missing military record details").await;
    }
    if vec![data_clone.sc_fname, data_clone.sc_lname].contains(&Some(String::new())) {
        return error_page(
            "Client Intake Systems",
            "Missing name of support coordinator",
        )
        .await;
    }
    if data_clone.diags == Some(String::new()) {
        return error_page("Client Intake Systems", "Missing diagnosises").await;
    }
    if vec![
        data_clone.behave_fname,
        data_clone.behave_lname,
        data_clone.behave_email,
        data_clone.behave_pn,
    ]
    .contains(&Some(String::new()))
    {
        return error_page(
            "Client Intake Systems",
            "Missing information on Behavior Analyst",
        )
        .await;
    }
    let wait_list_client = client::WaitingListClient::new(data);
    let wclient_uuid = wait_list_client.uuid.clone();
    let mut waiting_list: Vec<String> = get_value!(req.clone(), "/waiting_clients_list").unwrap();
    waiting_list.push(wclient_uuid.to_string());
    let worked = batch_write_value!(
        get_app_data(req.clone()),
        ("/waiting_clients_list", waiting_list),
        (
            "/waiting_clients_list/{wclient_uuid}/record",
            wait_list_client
        )
    )
    .is_ok();
    utils::respond_html(
        ClientIntakePageTemplate {
            success: Some(worked),
        }
        .render()
        .unwrap(),
    )
}

#[post("/change_caseload.rs")]
async fn change_caseload_post(
    req: HttpRequest,
    form: web::Form<ChangeCaseLoadPost>,
) -> HttpResponse {
    check_cookies_and_username_req_form(req.clone(), form, change_caseload_post_internal, "none")
        .await
}

async fn change_caseload_post_internal(
    username: String,
    req: HttpRequest,
    form: web::Form<ChangeCaseLoadPost>,
) -> HttpResponse {
    let caseload_uuid_list: Vec<String> =
        get_value!(req.clone(), "/employee_list/{username}/caseload_list").unwrap();
    if caseload_uuid_list.contains(&form.caseload) || form.caseload.clone() == "none" {
        write_value(
            get_app_data(req.clone()),
            format!("/employee_list/{username}/caseload"),
            form.caseload.clone(),
        )
        .await
        .unwrap();
        redirect("/")
    } else {
        error_page(
            "Caseload Management Systems",
            "You have not been assigned to this caseload",
        )
        .await
    }
}

#[get("/reports/jd/cie_job_placement")]
async fn cie_job_placement_page(req: HttpRequest) -> HttpResponse {
    check_cookies_and_username(req.clone(), cie_job_placement_page_internal, "none").await
}

async fn cie_job_placement_page_internal(req: HttpRequest, username: String) -> HttpResponse {
    utils::respond_html(
        CIEJobPlacementPageTemplate {
            navigation_bar: NavBarTemplate::new(req.clone(), username),
            success: None,
        }
        .render()
        .unwrap(),
    )
}

#[post("/reports/jd/cie_jp.rs")]
async fn cie_job_placement_post(
    req: HttpRequest,
    form: web::Form<CIEJobPlacementPost>,
) -> HttpResponse {
    check_cookies_and_username_req_form(req.clone(), form, cie_job_placement_post_internal, "none")
        .await
}

async fn cie_job_placement_post_internal(
    username: String,
    req: HttpRequest,
    form: web::Form<CIEJobPlacementPost>,
) -> HttpResponse {
    let now = Utc::now();
    if form.q1.is_none()
        || form.q2.is_none()
        || form.q3.is_none()
        || form.q4.is_none()
        || form.q5.is_none()
        || form.q6.is_none()
        || form.q7.is_none()
        || form.q8.is_none()
        || form.q9.is_none()
    {
        return error_page(
            "Job Placement Systems",
            "All checkboxes must be checked to be valid",
        )
        .await;
    }
    let local_tz: Tz = get_value!(req.clone(), "/employee_list/{username}/timezone").unwrap();
    let cie_jp = reports::jd::cie_jp::CIEJobPlacement {
        employer: form.employer.clone(),
        employer_address: form.employer_address.clone(),
        job_title: form.job_title.clone(),
        employment_start_date: local_tz
            .ymd(
                form.start_date.year(),
                form.start_date.month(),
                form.start_date.day(),
            )
            .and_hms(0, 0, 0)
            .with_timezone(&Utc),
        wage: form.wage,
        hours_per_week: form.hours_per_week.clone(),
        benefits: form.benefits.clone(),
        supervisor_name: form.supervisor_name.clone(),
        supervisor_contact: form.supervisor_contact.clone(),
        q_client_interact_other_exp: Some(form.q3_exp.clone()),
        q_coworker_prev_exp: Some(form.q4_exp.clone()),
        q_wage_ge_customary_exp: Some(form.q7_exp.clone()),
        q_client_same_opp_adv_exp: Some(form.q8_exp.clone()),
        q_access_same_non_exp: Some(form.q9_exp.clone()),
        crp_signature: form.crp_signature.clone(),
        sig_date: Utc.ymd(now.year(), now.month(), now.day()).and_hms(0, 0, 0),
    };
    let mut current_client = get_current_client(req.clone(), username.clone())
        .await
        .unwrap();
    current_client.employer = Some(form.employer.clone());
    current_client.supervisor = Some(form.supervisor_name.clone());
    let client_uuid = current_client.uuid;
    let success = batch_write_value!(
        get_app_data(req.clone()),
        ("/client_list/{client_uuid}/record", current_client),
        ("/client_list/{client_uuid}/report_data/jd/cie_jp", cie_jp)
    )
    .is_ok();
    /*
    let success = write_value(
        get_app_data(req.clone()),
        format!("/client_list/{client_uuid}/report_datajd/cie_jp").as_str(),
        cie_jp,
    )
    .await
    .is_ok();
    */
    //if let Some(current_client) = get_current_client(req.clone(), username.clone()).await {
    //let client_uuid = current_client.uuid;
    let mut data_vec: Vec<reports::jc::tracker::ClientScheduleRow>;
    if !key_exists!(
        req.clone(),
        "/client_list/{client_uuid}/report_data/jc/client_schedule"
    )
    .await
    {
        data_vec = Vec::new();
    } else {
        data_vec = get_value!(
            req.clone(),
            "/client_list/{client_uuid}/report_data/jc/client_schedule"
        )
        .unwrap();
    }
    let start_am: bool;
    if form.start_am == String::from("true") {
        start_am = true;
    } else if form.start_am == String::from("false") {
        start_am = false;
    } else {
        return error_page(
            "Client Scheduling Systems",
            "start_am must be true or false",
        )
        .await;
    }
    let end_am: bool;
    if form.end_am == String::from("true") {
        end_am = true;
    } else if form.end_am == String::from("false") {
        end_am = false;
    } else {
        return error_page("Client Scheduling Systems", "end_am must be true or false").await;
    }
    let valid_minutes = vec![0, 15, 30, 45];
    if form.start_hour < 1
        || form.start_hour > 12
        || form.end_hour < 1
        || form.end_hour > 12
        || !valid_minutes.contains(&form.start_minute)
        || !valid_minutes.contains(&form.end_minute)
    {
        return error_page("Client Scheduling Systems", "Invalid time").await;
    }
    data_vec.push(reports::jc::tracker::ClientScheduleRow {
        start_hour: form.start_hour,
        start_minute: form.start_minute,
        start_am: start_am,
        end_hour: form.end_hour,
        end_minute: form.end_minute,
        end_am: end_am,
    });
    let success2 = write_value(
        get_app_data(req.clone()),
        format!("/client_list/{client_uuid}/report_data/jc/client_schedule").as_str(),
        data_vec,
    )
    .await
    .is_ok();
    //}
    if success {
        let transport = get_smtp_transport();
        email::send_email(
            &transport,
            //"Hayden Johnson",
            vec!["Westly Ward <sonicrules1234@gmail.com>".parse().unwrap()],
            /*
            vec!["Hayden Johnson <haydenj@alliessupportedemployment.com>"
                .parse()
                .unwrap()],
            */
            None,
            //"haydenj@alliessupportedemployment.com",
            format!("Form 60 Ready for Approval"),
            format!(
                "A form 60 for {} {} is ready for approval.",
                current_client.first_name, current_client.last_name
            ),
        );
    }
    utils::respond_html(
        CIEJobPlacementPageTemplate {
            navigation_bar: NavBarTemplate::new(req.clone(), username),
            success: Some(success && success2),
        }
        .render()
        .unwrap(),
    )
}

#[derive(Template)]
#[template(path = "manage_caseloads.html")]
struct ManageCaseLoadPageTemplate {
    navigation_bar: NavBarTemplate,
    employees_checked: Vec<(employee::Employee, String)>,
    clients_checked: Vec<(client::Client, String)>,
    editing: bool,
    caseloads: Vec<caseload::CaseLoad>,
    editing_caseload_name: String,
    employees: Vec<employee::Employee>,
    clients: Vec<client::Client>,
    caseload_uuid: String,
}

#[get("/manage_caseloads")]
async fn manage_caseloads_page(req: HttpRequest) -> HttpResponse {
    check_cookies_and_username(req.clone(), manage_caseloads_page_internal, "none").await
}

async fn manage_caseloads_page_internal(req: HttpRequest, username: String) -> HttpResponse {
    let mut client_uuid_list: Vec<String> = get_value!(req.clone(), "/client_list").unwrap();
    client_uuid_list.retain(|x| x != "none");
    let employee_username_list: Vec<String> = get_value!(req.clone(), "/employee_list").unwrap();
    let caseload_uuid_list: Vec<String> = get_value!(req.clone(), "/caseload_list").unwrap();
    utils::respond_html(
        ManageCaseLoadPageTemplate {
            navigation_bar: NavBarTemplate::new(req.clone(), username.clone()),
            employees: employee_username_list
                .into_iter()
                .filter_map(|x| get_employee_record(req.clone(), x).unwrap())
                .collect(),
            clients: client_uuid_list
                .into_iter()
                .map(|x| {
                    //println!("{x}");
                    get_value!(req.clone(), "/client_list/{x}/record").unwrap()
                })
                .collect(),
            clients_checked: Vec::new(),
            employees_checked: Vec::new(),
            editing_caseload_name: String::new(),
            editing: false,
            caseloads: caseload_uuid_list
                .into_iter()
                .map(|x| get_value!(req.clone(), "/caseload_list/{x}/record").unwrap())
                .collect(),
            caseload_uuid: String::new(),
        }
        .render()
        .unwrap(),
    )
}

#[get("/choose_caseload")]
async fn choose_caseload_page(req: HttpRequest) -> HttpResponse {
    check_cookies_and_username(req.clone(), choose_caseload_page_internal, "none").await
}

async fn choose_caseload_page_internal(req: HttpRequest, username: String) -> HttpResponse {
    let caseload_uuid_list: Vec<String> =
        get_value!(req.clone(), "/employee_list/{username}/caseload_list").unwrap();
    utils::respond_html(
        ChooseCaseLoadPageTemplate {
            caseloads: caseload_uuid_list
                .iter()
                .map(|x| get_value!(req.clone(), "/caseload_list/{x}/record").unwrap())
                .collect(),
        }
        .render()
        .unwrap(),
    )
}

#[get("/management_index")]
async fn management_index_page(req: HttpRequest) -> HttpResponse {
    check_cookies_and_username(req.clone(), management_index_page_internal, "none").await
}

#[derive(Template)]
#[template(path = "management_index.html")]
struct ManagementIndexTemplate {
    navigation_bar: NavBarTemplate,
    report_allowed: bool,
    add_authorization_allowed: bool,
    //add_vr_counselor_support_coordinator_allowed: bool,
    add_employee_allowed: bool,
    add_service_area_allowed: bool,
    add_billing_office_allowed: bool,
    manage_caseloads_allowed: bool,
}

impl ManagementIndexTemplate {
    pub fn new(req: HttpRequest, username: String) -> Self {
        let employee = get_employee_record(req.clone(), username.clone())
            .unwrap()
            .unwrap();
        Self {
            navigation_bar: NavBarTemplate::new(req.clone(), username.clone()),
            report_allowed: employee.is_allowed("report"),
            add_authorization_allowed: employee.is_allowed("add_authorization"),
            //add_vr_counselor_support_coordinator_allowed: employee
            //    .is_allowed("add_vr_counselor_support_coordinator"),
            add_employee_allowed: employee.is_allowed("add_employee"),
            add_service_area_allowed: employee.is_allowed("add_service_area"),
            add_billing_office_allowed: employee.is_allowed("add_billing_office"),
            manage_caseloads_allowed: employee.is_allowed("manage_caseloads"),
        }
    }
}

async fn management_index_page_internal(req: HttpRequest, username: String) -> HttpResponse {
    utils::respond_html(
        ManagementIndexTemplate::new(req.clone(), username)
            .render()
            .unwrap(),
    )
}

#[derive(Template)]
#[template(path = "add_authorization.html")]
struct AddAuthorizationPageTemplate {
    is_authorization: bool,
    navigation_bar: NavBarTemplate,
    success: Option<bool>,
    service_areas: Vec<client::ServiceArea>,
    //vr_counselor_support_coordinators: Vec<String>,
    available_services: Vec<client::ClientService>,
    billing_offices: Vec<reports::BillingOffice>,
    waiting_clients_list: Vec<WaitingListClient>,
    //offices: Vec<String>,
}

#[derive(Template)]
#[template(path = "billing_tracking_vr_jd.html")]
struct BillingTrackingVRJDPageTemplate {
    navigation_bar: NavBarTemplate,
    success: Option<bool>,
    client_name: String,
    office_location: String,
    auth_number: String,
    employee_name: String,
    auth_type: String,
    //calculated_hours: Vec<String>,
    total_jd: String,
    vr_counselor: String,
    billing_location: reports::BillingOffice,
    //cur_job_seeking: String,
    //choices: Vec<String>,
    service_type: reports::billing_tracking_vr_jd::Service,
    comments: String,
    cur_job_tracking_yn: String,
}

#[derive(Template)]
#[template(path = "billing_tracking_vr_jd_report.html")]
struct BillingTrackingVRJDReportPageTemplate {
    navigation_bar: NavBarTemplate,
    data_rows: Vec<(
        String,
        String,
        String,
        Vec<employee::Employee>,
        String,
        String,
        String,
        String,
        String,
        String,
        String,
        String,
    )>,
}

#[get("/reports/billing_tracking_vr_jd_report")]
async fn billing_tracking_vr_jd_report_page(req: HttpRequest) -> HttpResponse {
    check_cookies_and_username(
        req.clone(),
        billing_tracking_vr_jd_report_page_internal,
        "report",
    )
    .await
}

async fn billing_tracking_vr_jd_report_page_internal(
    req: HttpRequest,
    username: String,
) -> HttpResponse {
    let client_list: Vec<String> = get_value!(req.clone(), "/client_list").unwrap();
    let data_rows = client_list
        .iter()
        .filter_map(|client_uuid| {
            if client_uuid.to_lowercase() == "none" {
                return None;
            }
            let this_client: client::Client =
                get_value!(req.clone(), "/client_list/{client_uuid}/record").unwrap();
            if this_client.service_provided == client::ClientService::JobDevelopment {
                let data: reports::billing_tracking_vr_jd::BillingTrackingVRJDRow = get_value!(
                    req.clone(),
                    "/client_list/{client_uuid}/report_data/billing_tracking_vr_jd"
                )
                .unwrap();
                let comments: String;
                let mut total_duration = chrono::Duration::seconds(0);
                let mut office_location = String::new();
                //let mut employee_name = String::new();
                comments = data.comments.clone();
                let mut employee_list: Vec<employee::Employee> = Vec::new();
                if let Some(employee_username) = data.employee_username {
                    let this_employee = get_employee_record(req.clone(), employee_username)
                        .unwrap()
                        .unwrap();
                    let caseload_uuids: Vec<String> =
                        get_value!(req.clone(), "/caseload_list").unwrap();
                    employee_list.push(this_employee.clone());
                    for caseload_uuid in caseload_uuids {
                        let caseload: caseload::CaseLoad =
                            get_value!(req.clone(), "/caseload_list/{caseload_uuid}/record")
                                .unwrap();
                        for this_employee_user in caseload.employees {
                            let this_empl: employee::Employee =
                                get_employee_record(req.clone(), this_employee_user)
                                    .unwrap()
                                    .unwrap();
                            if !employee_list.contains(&this_empl) {
                                employee_list.push(this_empl);
                            }
                        }
                    }
                    //employee_name =
                    //    format!("{} {}", this_employee.first_name, this_employee.last_name);
                    office_location = this_employee.office.string_proper_name();
                    let now = Utc::now();
                    //let this_year = now.year();
                    total_duration = match data.start_tracking_datetime {
                        Some(start_datetime) => calculate_duration_dates_with_client(
                            req.clone(),
                            this_employee.username.clone(),
                            client_uuid.clone().to_string(),
                            start_datetime,
                            Utc.ymd(now.year(), now.month(), now.day()).and_hms(
                                now.hour(),
                                now.minute(),
                                0,
                            ),
                        ),
                        None => chrono::Duration::seconds(0),
                    };
                }
                let cur_job_seeking: String;
                if data.cur_job_seek {
                    cur_job_seeking = String::from("Y");
                } else {
                    cur_job_seeking = String::from("N");
                }
                Some((
                    format!("{} {}", this_client.first_name, this_client.last_name),
                    office_location,
                    this_client.auth_num,
                    employee_list,
                    this_client.service_provided.client_service_string(),
                    calculate_hour_quarters(total_duration),
                    this_client.vr_counselor_support_coordinator.unwrap(),
                    this_client.billing_office.to_string(),
                    cur_job_seeking,
                    this_client.service_type.unwrap().to_string(),
                    comments,
                    this_client.uuid.to_string(),
                ))
            } else {
                None
            }
        })
        .collect();
    utils::respond_html(
        BillingTrackingVRJDReportPageTemplate {
            navigation_bar: NavBarTemplate::new(req.clone(), username.clone()),
            data_rows: data_rows,
        }
        .render()
        .unwrap(),
    )
}

#[get("/reports/billing_tracking_vr_jd")]
async fn billing_tracking_vr_jd_page(req: HttpRequest) -> HttpResponse {
    check_cookies_and_username(req.clone(), billing_tracking_vr_jd_page_internal, "none").await
}

async fn billing_tracking_vr_jd_page_internal(req: HttpRequest, username: String) -> HttpResponse {
    let (client_result, this_employee) =
        get_current_client_employee(req.clone(), username.clone()).await;
    if client_result.is_none() {
        return error_page(
            "Billing Tracking Systems",
            "You are currently not clocked in for a client",
        )
        .await;
    }
    let current_client = client_result.unwrap();
    if current_client.service_provided != client::ClientService::JobDevelopment {
        return error_page(
            "Billing Tracking Systems",
            "You are not clocked for job development",
        )
        .await;
    }
    let mut comments = String::new();
    //let choices: Vec<String>;
    let client_uuid = current_client.clone().uuid;
    let mut total_duration = chrono::Duration::seconds(0);
    if key_exists!(
        req.clone(),
        "/client_list/{client_uuid}/report_data/billing_tracking_vr_jd"
    )
    .await
    {
        let data: reports::billing_tracking_vr_jd::BillingTrackingVRJDRow = get_value!(
            req.clone(),
            "/client_list/{client_uuid}/report_data/billing_tracking_vr_jd"
        )
        .unwrap();
        comments = data.comments.clone();
        let now = Utc::now();
        //let this_year = now.year();
        total_duration = match data.start_tracking_datetime {
            Some(start_datetime) => calculate_duration_dates_with_client(
                req.clone(),
                this_employee.username.clone(),
                client_uuid.clone().to_string(),
                start_datetime,
                Utc.ymd(now.year(), now.month(), now.day())
                    .and_hms(now.hour(), now.minute(), 0),
            ),
            None => chrono::Duration::seconds(0),
        };
    }
    utils::respond_html(
        BillingTrackingVRJDPageTemplate {
            navigation_bar: NavBarTemplate::new(req.clone(), username.clone()),
            success: None,
            client_name: format!("{} {}", current_client.first_name, current_client.last_name),
            office_location: this_employee.office.string_proper_name(),
            auth_number: current_client.auth_num,
            employee_name: format!("{} {}", this_employee.first_name, this_employee.last_name),
            auth_type: current_client.service_provided.client_service_string(),
            total_jd: calculate_hour_quarters(total_duration),
            vr_counselor: current_client
                .vr_counselor_support_coordinator
                .unwrap_or(String::new()),
            service_type: current_client.service_type.unwrap(),
            billing_location: current_client.billing_office,
            //calculated_hours: calculated_hours,
            //choices: choices,
            comments: comments,
            cur_job_tracking_yn: String::from("Y"),
        }
        .render()
        .unwrap(),
    )
}

#[derive(Template)]
#[template(path = "add_billing_office.html")]
struct AddBillingOfficePageTemplate {
    navigation_bar: NavBarTemplate,
    success: Option<bool>,
}

#[get("/add_billing_office")]
async fn add_billing_office_page(req: HttpRequest) -> HttpResponse {
    check_cookies_and_username(
        req.clone(),
        add_billing_office_page_internal,
        "add_billing_office",
    )
    .await
}

async fn add_billing_office_page_internal(req: HttpRequest, username: String) -> HttpResponse {
    utils::respond_html(
        AddBillingOfficePageTemplate {
            navigation_bar: NavBarTemplate::new(req.clone(), username),
            success: None,
        }
        .render()
        .unwrap(),
    )
}

#[derive(Template)]
#[template(path = "add_service_area.html")]
struct AddServiceAreaPageTemplate {
    navigation_bar: NavBarTemplate,
    success: Option<bool>,
}

#[get("/add_service_area")]
async fn add_service_area_page(req: HttpRequest) -> HttpResponse {
    check_cookies_and_username(
        req.clone(),
        add_service_area_page_internal,
        "add_service_area",
    )
    .await
}

async fn add_service_area_page_internal(req: HttpRequest, username: String) -> HttpResponse {
    utils::respond_html(
        AddServiceAreaPageTemplate {
            navigation_bar: NavBarTemplate::new(req.clone(), username),
            success: None,
        }
        .render()
        .unwrap(),
    )
}
/*
#[derive(Template)]
#[template(path = "add_vr_counselor_support_coordinator.html")]
struct AddVRCounselorSupportCoordinatorPageTemplate {
    navigation_bar: NavBarTemplate,
    success: Option<bool>,
}
*/

#[derive(Debug, Clone, Serialize, Deserialize)]
struct AddBillingOfficePost {
    billing_office_name: String,
}

#[derive(Debug, Clone, Serialize, Deserialize)]
struct AddServiceAreaPost {
    service_area_name: String,
}

#[derive(Debug, Clone, Serialize, Deserialize)]
struct BillingTrackingVRJDPost {
    comments: String,
}

#[derive(Debug, Clone, Serialize, Deserialize)]
struct BillingTrackingVRJDReportPost {
    employee_username: String,
    client_uuid: String,
}

#[post("/add_billing_office.rs")]
async fn add_billing_office_post(
    req: HttpRequest,
    form: web::Form<AddBillingOfficePost>,
) -> HttpResponse {
    check_cookies_and_username_req_form(
        req.clone(),
        form,
        add_billing_office_post_internal,
        "add_billing_office",
    )
    .await
}

#[post("/reports/save_billing_tracking_vr_jd_report_row.rs")]
async fn save_billing_tracking_vr_jd_report_post(
    req: HttpRequest,
    form: web::Form<BillingTrackingVRJDReportPost>,
) -> HttpResponse {
    check_cookies_and_username_req_form(
        req.clone(),
        form,
        save_billing_tracking_vr_jd_report_post_internal,
        "report",
    )
    .await
}

async fn save_billing_tracking_vr_jd_report_post_internal(
    username: String,
    req: HttpRequest,
    form: web::Form<BillingTrackingVRJDReportPost>,
) -> HttpResponse {
    //let now = Utc::now();
    let client_uuid = form.client_uuid.clone();
    let mut row: reports::billing_tracking_vr_jd::BillingTrackingVRJDRow = get_value!(
        req.clone(),
        "/client_list/{client_uuid}/report_data/billing_tracking_vr_jd"
    )
    .unwrap();
    row.employee_username = Some(form.employee_username.clone());
    let _worked = write_value(
        get_app_data(req.clone()),
        format!("/client_list/{client_uuid}/report_data/billing_tracking_vr_jd").to_string(),
        row.clone(),
    )
    .await
    .is_ok();
    let client_list: Vec<String> = get_value!(req.clone(), "/client_list").unwrap();
    let data_rows = client_list
        .iter()
        .filter_map(|client_uuid| {
            if client_uuid.to_lowercase() == "none" {
                return None;
            }
            let this_client: client::Client =
                get_value!(req.clone(), "/client_list/{client_uuid}/record").unwrap();
            if this_client.service_provided == client::ClientService::JobDevelopment {
                let data: reports::billing_tracking_vr_jd::BillingTrackingVRJDRow = get_value!(
                    req.clone(),
                    "/client_list/{client_uuid}/report_data/billing_tracking_vr_jd"
                )
                .unwrap();
                let comments: String;
                let mut total_duration = chrono::Duration::seconds(0);
                let mut office_location = String::new();
                //let mut employee_name = String::new();
                comments = data.comments.clone();
                let mut employee_list: Vec<employee::Employee> = Vec::new();
                if let Some(employee_username) = data.employee_username {
                    let this_employee = get_employee_record(req.clone(), employee_username)
                        .unwrap()
                        .unwrap();
                    let caseload_uuids: Vec<String> =
                        get_value!(req.clone(), "/caseload_list").unwrap();
                    employee_list.push(this_employee.clone());
                    for caseload_uuid in caseload_uuids {
                        let caseload: caseload::CaseLoad =
                            get_value!(req.clone(), "/caseload_list/{caseload_uuid}/record")
                                .unwrap();
                        for this_employee_user in caseload.employees {
                            let this_empl: employee::Employee =
                                get_employee_record(req.clone(), this_employee_user)
                                    .unwrap()
                                    .unwrap();
                            if !employee_list.contains(&this_empl) {
                                employee_list.push(this_empl);
                            }
                        }
                    }
                    //employee_name =
                    //    format!("{} {}", this_employee.first_name, this_employee.last_name);
                    office_location = this_employee.office.string_proper_name();
                    let now = Utc::now();
                    //let this_year = now.year();
                    total_duration = match data.start_tracking_datetime {
                        Some(start_datetime) => calculate_duration_dates_with_client(
                            req.clone(),
                            this_employee.username.clone(),
                            client_uuid.clone().to_string(),
                            start_datetime,
                            Utc.ymd(now.year(), now.month(), now.day()).and_hms(
                                now.hour(),
                                now.minute(),
                                0,
                            ),
                        ),
                        None => chrono::Duration::seconds(0),
                    };
                }
                let cur_job_seeking: String;
                if data.cur_job_seek {
                    cur_job_seeking = String::from("Y");
                } else {
                    cur_job_seeking = String::from("N");
                }
                Some((
                    format!("{} {}", this_client.first_name, this_client.last_name),
                    office_location,
                    this_client.auth_num,
                    employee_list,
                    this_client.service_provided.client_service_string(),
                    calculate_hour_quarters(total_duration),
                    this_client.vr_counselor_support_coordinator.unwrap(),
                    this_client.billing_office.to_string(),
                    cur_job_seeking,
                    this_client.service_type.unwrap().to_string(),
                    comments,
                    this_client.uuid.to_string(),
                ))
            } else {
                None
            }
        })
        .collect();
    utils::respond_html(
        BillingTrackingVRJDReportPageTemplate {
            navigation_bar: NavBarTemplate::new(req.clone(), username.clone()),
            data_rows: data_rows,
        }
        .render()
        .unwrap(),
    )
}

#[post("/save_billing_tracking_vr_jd_row.rs")]
async fn save_billing_tracking_vr_jd_post(
    req: HttpRequest,
    form: web::Form<BillingTrackingVRJDPost>,
) -> HttpResponse {
    check_cookies_and_username_req_form(
        req.clone(),
        form,
        save_billing_tracking_vr_jd_post_internal,
        "none",
    )
    .await
}

async fn save_billing_tracking_vr_jd_post_internal(
    username: String,
    req: HttpRequest,
    form: web::Form<BillingTrackingVRJDPost>,
) -> HttpResponse {
    let client_result = get_current_client(req.clone(), username.clone()).await;
    if client_result.is_none() {
        return error_page(
            "Billing Tracking Systems",
            "You are currently not clocked in for a client",
        )
        .await;
    }
    let current_client = client_result.unwrap();
    if current_client.service_provided != client::ClientService::JobDevelopment {
        return error_page(
            "Billing Tracking Systems",
            "You are not clocked for job development",
        )
        .await;
    }
    let client_uuid = current_client.uuid;
    let now = Utc::now();
    //let this_year = now.year();
    /*
    let billing_tracking_vr_jd_row = reports::billing_tracking_vr_jd::BillingTrackingVRJDRow {
        client_uuid: client_uuid,
        /*
        vr_counselor_support_coordinator: current_client
            .vr_counselor_support_coordinator
            .clone()
            .unwrap(),
        */
        employee_username: Some(username.clone()),
        cur_job_seek: cur_job_seek,
        comments: form.comments.clone(),
    };
    */
    let mut row: reports::billing_tracking_vr_jd::BillingTrackingVRJDRow = get_value!(
        req.clone(),
        "/client_list/{client_uuid}/report_data/billing_tracking_vr_jd"
    )
    .unwrap();
    row.comments = form.comments.clone();
    let worked = write_value(
        get_app_data(req.clone()),
        format!("/client_list/{client_uuid}/report_data/billing_tracking_vr_jd").to_string(),
        row.clone(),
    )
    .await
    .is_ok();
    //let choices: Vec<String>;
    let comments = form.comments.clone();
    /*
    if cur_job_seek {
        choices = vec![String::from("Y"), String::from("N")]
    } else {
        choices = vec![String::from("N"), String::from("Y")]
    }
    */
    let employee_name: String;
    let office_location: String;
    let total_jd: String;
    match row.employee_username {
        Some(user) => {
            let rel_employee = get_employee_record(req.clone(), user.clone())
                .unwrap()
                .unwrap();
            employee_name = format!("{} {}", rel_employee.first_name, rel_employee.last_name);
            office_location = rel_employee.office.string_proper_name();
            if row.cur_job_seek {
                total_jd = calculate_hour_quarters(calculate_duration_dates_with_client(
                    req.clone(),
                    user.clone(),
                    client_uuid.to_string(),
                    row.start_tracking_datetime.unwrap(),
                    Utc.ymd(now.year(), now.month(), now.day()).and_hms(
                        now.hour(),
                        now.minute(),
                        0,
                    ),
                ));
            } else {
                total_jd = String::from("0");
            }
        }
        None => {
            employee_name = String::new();
            office_location = String::new();
            total_jd = String::new();
        }
    }
    utils::respond_html(
        BillingTrackingVRJDPageTemplate {
            navigation_bar: NavBarTemplate::new(req.clone(), username.clone()),
            success: Some(worked),
            client_name: format!("{} {}", current_client.first_name, current_client.last_name),
            office_location: office_location, //this_employee.office.string_proper_name(),
            auth_number: current_client.auth_num,
            employee_name: employee_name, //format!("{} {}", this_employee.first_name, this_employee.last_name),
            auth_type: current_client.service_provided.client_service_string(),
            total_jd: total_jd, //format_duration_hm(total_duration),
            vr_counselor: current_client
                .vr_counselor_support_coordinator
                .unwrap_or(String::new()),
            service_type: current_client.service_type.unwrap(),
            billing_location: current_client.billing_office,
            //calculated_hours: calculated_hours,
            comments: comments,
            //choices: choices,
            cur_job_tracking_yn: String::from("Y"),
        }
        .render()
        .unwrap(),
    )
}

async fn add_billing_office_post_internal(
    username: String,
    req: HttpRequest,
    form: web::Form<AddBillingOfficePost>,
) -> HttpResponse {
    let mut billing_office_list = list_all_billing_offices(req.clone());
    billing_office_list.push(reports::BillingOffice::Custom(String::from(
        form.billing_office_name.clone(),
    )));
    let worked = write_value(
        get_app_data(req.clone()),
        "/billing_office_list",
        billing_office_list,
    )
    .await
    .is_ok();
    utils::respond_html(
        AddBillingOfficePageTemplate {
            navigation_bar: NavBarTemplate::new(req.clone(), username),
            success: Some(worked),
        }
        .render()
        .unwrap(),
    )
}

#[post("/add_service_area.rs")]
async fn add_service_area_post(
    req: HttpRequest,
    form: web::Form<AddServiceAreaPost>,
) -> HttpResponse {
    check_cookies_and_username_req_form(
        req.clone(),
        form,
        add_service_area_post_internal,
        "add_service_area",
    )
    .await
}

async fn add_service_area_post_internal(
    username: String,
    req: HttpRequest,
    form: web::Form<AddServiceAreaPost>,
) -> HttpResponse {
    let mut service_area_list = list_all_service_areas(req.clone());
    service_area_list.push(client::ServiceArea::Custom(String::from(
        form.service_area_name.clone(),
    )));
    let worked = write_value(
        get_app_data(req.clone()),
        "/service_area_list",
        service_area_list,
    )
    .await
    .is_ok();
    utils::respond_html(
        AddServiceAreaPageTemplate {
            navigation_bar: NavBarTemplate::new(req.clone(), username),
            success: Some(worked),
        }
        .render()
        .unwrap(),
    )
}

/*
#[derive(Debug, Clone, Serialize, Deserialize)]
struct AddVRCounselorSupportCoordinatorPost {
    fname: String,
    lname: String,
}
*/
async fn add_authorization(
    req: HttpRequest,
    local_tz: Tz,
    auth_num: String,
    client_uuid: String,
    first_name: String,
    last_name: String,
    service_area: String,
    /*
    employer: Option<String>,
    supervisor: Option<String>,
    */
    vr_counselor_support_coordinator: Option<String>,
    service_provided: client::ClientService,
    rate: f64,
    duration_authed: std::time::Duration,
    start_date: NaiveDate,
    end_date: NaiveDate,
    mmu: Option<usize>,
    billing_office: String,
    service_type: Option<reports::billing_tracking_vr_jd::Service>,
    assoc: BTreeMap<client::ClientService, String>,
    //office: String,
) -> Result<(), acid_store::Error> {
    //let local_tz: Tz = get_value!(req.clone(), "/employee_list/{username}/timezone").unwrap();
    //let assoc_client: client::Client = get_value!(req.clone(), "/client_list/")
    let start_date = local_tz
        .ymd(start_date.year(), start_date.month(), start_date.day())
        .and_hms(0, 0, 0)
        .with_timezone(&Utc);
    let end_date = local_tz
        .ymd(end_date.year(), end_date.month(), end_date.day())
        .and_hms(0, 0, 0)
        .with_timezone(&Utc);
    let fname: String;
    let lname: String;
    let c_uuid: Uuid;
    if client_uuid == "below" {
        fname = first_name;
        lname = last_name;
        c_uuid = Uuid::new_v4();
    } else {
        let waiting_client: client::WaitingListClient =
            get_value!(req.clone(), "/waiting_clients_list/{client_uuid}/record").unwrap();
        fname = waiting_client.intake_data.client_fname;
        lname = waiting_client.intake_data.client_lname;
        c_uuid = waiting_client.uuid;
    }
    let this_client = client::Client::new(
        c_uuid,
        auth_num,
        fname,
        lname,
        client::ServiceArea::from(service_area, list_all_service_areas(req.clone())).unwrap(),
        /*
        employer,
        supervisor,
        */
        None,
        None,
        vr_counselor_support_coordinator,
        service_provided.clone(),
        rate,
        duration_authed,
        start_date,
        end_date,
        mmu,
        reports::BillingOffice::from(billing_office, list_all_billing_offices(req.clone()))
            .unwrap(),
        service_type,
        assoc, //employee::OfficeLocation::from(office),
    );
    let mut client_uuid_list: Vec<String> = get_value!(req.clone(), "/client_list")?;
    client_uuid_list.push(this_client.uuid.to_string());
    let client_uuid = this_client.uuid.clone();
    batch_write_value!(
        get_app_data(req.clone()),
        ("/client_list", client_uuid_list),
        ("/client_list/{client_uuid}/record", this_client.clone()),
        (
            "/client_list/{client_uuid}/caseload_list",
            Vec::<String>::new()
        )
    )?;
    if service_provided == client::ClientService::JobDevelopment {
        let billing_tracking_vr_jd_row = reports::billing_tracking_vr_jd::BillingTrackingVRJDRow {
            client_uuid: client_uuid.clone(),
            start_tracking_datetime: None,
            /*
            vr_counselor_support_coordinator: current_client
                .vr_counselor_support_coordinator
                .clone()
                .unwrap(),
            */
            employee_username: None,
            cur_job_seek: false,
            comments: String::new(),
        };
        write_value(
            get_app_data(req.clone()),
            format!("/client_list/{client_uuid}/report_data/billing_tracking_vr_jd").as_str(),
            billing_tracking_vr_jd_row,
        )
        .await
        .unwrap();
    }
    let budget_auth: &str;
    if service_provided.is_authorization() {
        budget_auth = "Authorization";
    } else {
        budget_auth = "Budget Line";
    }
    let transport = get_smtp_transport();
    let local_start = this_client
        .start_date
        .with_timezone(&Tz::America__Denver)
        .date();
    email::send_email(
        &transport,
        //vec!["Hayden Johnson <haydenj@alliessupportedemployment.com>".parse().unwrap()],
        vec!["Westly Ward <sonicrules1234@gmail.com>".parse().unwrap()],
        None,
        //"Hayden Johnson",
        //"Westly Ward",
        //"sonicrules1234@gmail.com",
        //"haydenj@alliessupportedemployment.com",
        format!("New {budget_auth} from {}", this_client.billing_office),
        format!(
            "A new {budget_auth} has been created for {} {}.\nStart date: {}\nAuth Number: {}\nBilling Office: {}\nAuth type: {}\nHours: {}",
            this_client.first_name, this_client.last_name, local_start.format("%B %e %Y"), this_client.auth_num, this_client.billing_office, this_client.service_provided.client_service_string(), calculate_hour_quarters(chrono::Duration::from_std(this_client.duration_authed).unwrap())
        ),
    );
    if service_provided == client::ClientService::Discovery {
        append_to_schedule_req(req.clone(), scheduled_notifications::ScheduledNotification::from_datetime(this_client.start_date.checked_add_signed(chrono::Duration::days(30)).unwrap(), Some(chrono::Duration::days(30).to_std().unwrap()), scheduled_notifications::NotificationType::Discovery30(client_uuid, this_client.start_date.checked_add_signed(chrono::Duration::days(60)).unwrap()), Vec::new(), None, "Reminder: DSR Due", format!("Hello,\nthis is a reminder that you have a Discovery Assessment for {} {} due on {}.\nThanks", this_client.first_name, this_client.last_name, this_client.start_date.checked_add_signed(chrono::Duration::days(60)).unwrap()))).await.unwrap();
    } else if vec![
        client::ClientService::WsaTier1,
        client::ClientService::WsaTier2,
    ]
    .contains(&service_provided)
    {
        append_to_schedule_req(req.clone(), scheduled_notifications::ScheduledNotification::from_datetime(this_client.start_date.checked_add_signed(chrono::Duration::weeks(2)).unwrap(), Some(chrono::Duration::days(7).to_std().unwrap()), scheduled_notifications::NotificationType::WSA14_7(client_uuid, this_client.start_date.checked_add_signed(chrono::Duration::weeks(3)).unwrap()), Vec::new(), None, "Reminder: Work Strategy Assessment Due", format!("Hello,\nthis is a reminder that you have a Work Strategy Assessment for {} {} due on {}.\nThanks", this_client.first_name, this_client.last_name, this_client.start_date.checked_add_signed(chrono::Duration::weeks(3)).unwrap()))).await.unwrap();
    }
    Ok(())
}
/*
async fn add_vr_counselor_support_coordinator(
    req: HttpRequest,
    first_name: String,
    last_name: String,
) -> Result<(), acid_store::Error> {
    let vr_counselor_support_coordinator =
        String::new(first_name, last_name);
    let mut vr_counselor_support_coordinator_uuid_list: Vec<String> =
        get_value!(req.clone(), "/vr_counselor_support_coordinator_list")?;
    vr_counselor_support_coordinator_uuid_list
        .push(vr_counselor_support_coordinator.uuid.to_string());
    let vr_counselor_uuid = vr_counselor_support_coordinator.uuid.to_string();
    batch_write_value!(
        get_app_data(req.clone()),
        (
            "/vr_counselor_support_coordinator_list",
            vr_counselor_support_coordinator_uuid_list
        ),
        (
            "/vr_counselor_support_coordinator_list/{vr_counselor_uuid}/record",
            vr_counselor_support_coordinator.clone()
        )
    )?;
    Ok(())
}
*/
async fn get_current_client_employee(
    req: HttpRequest,
    username: String,
) -> (Option<client::Client>, Employee) {
    let employee = get_employee_record(req.clone(), username.clone())
        .unwrap()
        .unwrap();
    if employee.is_clocked_in().await {
        if let Direction::In(_, client_uuid) = employee
            .clocked_times
            .iter()
            .last()
            .unwrap()
            .extract_direction()
        {
            let client: client::Client =
                get_value!(req.clone(), "/client_list/{client_uuid}/record").unwrap();
            return (Some(client), employee);
        }
    }
    (None, employee)
}
async fn get_current_client(req: HttpRequest, username: String) -> Option<client::Client> {
    let employee = get_employee_record(req.clone(), username.clone())
        .unwrap()
        .unwrap();
    if employee.is_clocked_in().await {
        if let Direction::In(_, client_uuid) = employee
            .clocked_times
            .iter()
            .last()
            .unwrap()
            .extract_direction()
        {
            let client: client::Client =
                get_value!(req.clone(), "/client_list/{client_uuid}/record").unwrap();
            return Some(client);
        }
    }
    None
}

#[derive(Template)]
#[template(path = "monthly_jc_report_export.html")]
struct MonthlyJCReportExport {
    observation_date_1: String,
    observation_date_2: String,
    client_name: String,
    vr_counselor: String,
    crp: String,
    report_date: String,
    employer: String,
    supervisor: String,
    observations: [reports::ongoing_supports::monthly_jc_report::Observation; 2],
    crp_signature_date: String,
    crp_signature: String,
    report: reports::ongoing_supports::monthly_jc_report::Report,
}

impl MonthlyJCReportExport {
    pub fn new(
        report: reports::ongoing_supports::monthly_jc_report::Report,
        client_name: String,
    ) -> Self {
        let report_clone = report.clone();
        let fo = report.observations[0].clone();
        let so = report.observations[1].clone();
        Self {
            observation_date_1: format!("{}/{}/{}", fo.date.month(), fo.date.day(), fo.date.year()),
            observation_date_2: format!("{}/{}/{}", so.date.month(), so.date.day(), so.date.year()),
            client_name: client_name,
            /*
            vr_counselor: format!(
                "{} {}",
                report.vr_counselor_support_coordinator.first_name,
                report.vr_counselor_support_coordinator.last_name
            ),
            */
            vr_counselor: report.vr_counselor_support_coordinator.clone(),
            crp: report.crp,
            report_date: format!(
                "{}/{}/{}",
                report.date.month(),
                report.date.day(),
                report.date.year()
            ),
            employer: report.employer,
            supervisor: report.supervisor,
            observations: report.observations,
            crp_signature: report.crp_signature,
            crp_signature_date: format!(
                "{}/{}/{}",
                report.crp_signature_date.month(),
                report.crp_signature_date.day(),
                report.crp_signature_date.year()
            ),
            report: report_clone,
        }
    }
}

async fn monthly_jc_report_export_internal(req: HttpRequest, username: String) -> HttpResponse {
    if let Some(current_client) = get_current_client(req.clone(), username.clone()).await {
        if current_client.service_provided == client::ClientService::JobCoaching {
            let client_uuid = current_client.uuid.to_string();
            let report_vec: Vec<reports::ongoing_supports::monthly_jc_report::Report> = get_value!(
                req.clone(),
                "/client_list/{client_uuid}/report_data/ongoing_supports/jc_monthly"
            )
            .unwrap();
            let report = report_vec[0].clone();
            let html_data = MonthlyJCReportExport::new(
                report,
                format!("{} {}", current_client.first_name, current_client.last_name),
            )
            .render()
            .unwrap();
            let mut f =
                std::fs::File::create(format!("monthly_jc_report_export_{client_uuid}.html"))
                    .unwrap();
            f.write_all(html_data.as_bytes()).unwrap();
            drop(f);
            let file_path = std::path::Path::new(
                format!("monthly_jc_report_export_{client_uuid}.html").as_str(),
            )
            .canonicalize()
            .unwrap();
            let path_display = file_path.display();
            let browser = Browser::new(LaunchOptionsBuilder::default().build().unwrap()).unwrap();
            let tab = browser.wait_for_initial_tab().unwrap();
            tab.navigate_to(format!("file://{path_display}").as_str())
                .unwrap()
                .wait_until_navigated()
                .unwrap();
            let data = tab
                .print_to_pdf(Some(headless_chrome::protocol::page::PrintToPdfOptions {
                    landscape: Some(false),
                    display_header_footer: None,
                    print_background: Some(true),
                    scale: Some(0.9),
                    paper_width: None,
                    paper_height: None,
                    margin_top: None,
                    margin_bottom: None,
                    margin_left: None,
                    margin_right: None,
                    page_ranges: None,
                    ignore_invalid_page_ranges: None,
                    header_template: None,
                    footer_template: None,
                    prefer_css_page_size: None,
                }))
                .unwrap();
            let mut f =
                std::fs::File::create(format!("monthly_jc_report_export_{client_uuid}.pdf"))
                    .unwrap();
            f.write_all(data.as_slice()).unwrap();
            drop(f);
            return redirect(format!("/monthly_jc_report_export_{client_uuid}.pdf"));
        }
    }
    error_page("Report Exporting Systems", "You are not clocked in for a client or the client service you are clocked in for is not job coaching").await
}

async fn grab_jc_monthly_pdf_internal(
    username: String,
    req: HttpRequest,
    path: web::Path<String>,
) -> HttpResponse {
    let client_uuid = path;
    let current_client = get_current_client(req.clone(), username.clone())
        .await
        .unwrap();
    if current_client.uuid.to_string() == client_uuid.clone() {
        let mut fileobj =
            std::fs::File::open(format!("monthly_jc_report_export_{client_uuid}.pdf").as_str())
                .unwrap();
        let mut data: Vec<u8> = Vec::new();
        fileobj.read_to_end(&mut data).unwrap();
        drop(fileobj);
        HttpResponse::Ok()
            .content_type(actix_web::http::header::ContentType::octet_stream())
            .body(data)
    } else {
        HttpResponse::NotFound().finish()
    }
}

#[get("/monthly_jc_report_export_{client_uuid}.pdf")]
async fn grab_jc_monthly_pdf(req: HttpRequest, client_uuid: web::Path<String>) -> HttpResponse {
    check_cookies_and_username_path(
        req.clone(),
        client_uuid,
        grab_jc_monthly_pdf_internal,
        "report",
    )
    .await
}
/*
#[get("/oauth/{get_data}")]
async fn oauth_page(req: HttpRequest, get_data: web::Path<String>) -> HttpResponse {
    check_cookies_and_username_path(req.clone(), get_data, oauth_page_internal, "report").await
}

async fn oauth_page_internal(
    username: String,
    req: HttpRequest,
    path: web::Path<String>,
) -> HttpResponse {
    let query_string = path;
}
*/

#[get("/monthly_jc_report_export")]
async fn monthly_jc_report_export(req: HttpRequest) -> HttpResponse {
    check_cookies_and_username(req.clone(), monthly_jc_report_export_internal, "report").await
}

#[derive(Debug, Clone, Serialize, Deserialize)]
struct MonthlyJCObservationReportPost {
    crp: Option<String>,
    attendance: String,
    time_management: String,
    appearance: String,
    communication: String,
    jt_completion_rate: String,
    jt_quality: String,
    overall_appraisal: Option<String>,
    problems: Option<String>,
    interventions_used: Option<String>,
    recommended_interventions: Option<String>,
    req_counselor_contact: Option<bool>,
    crp_signature: Option<String>,
}

async fn monthly_jc_post_internal(
    username: String,
    req: HttpRequest,
    form: web::Form<MonthlyJCObservationReportPost>,
) -> HttpResponse {
    let current_client = get_current_client(req.clone(), username.clone())
        .await
        .unwrap();
    let mut employer: Option<String> = None;
    let mut supervisor: Option<String> = None;
    if current_client
        .assoc_service_uuids
        .contains_key(&client::ClientService::JobDevelopment)
    {
        let jd_client_uuid: String =
            current_client.assoc_service_uuids[&client::ClientService::JobDevelopment].clone();
        let assoc_jd_client: client::Client =
            get_value!(req.clone(), "/client_list/{jd_client_uuid}/record").unwrap();
        employer = assoc_jd_client.employer;
        supervisor = assoc_jd_client.supervisor;
    }
    let client_uuid = current_client.uuid;
    /*
    if let None = client.vr_counselor_support_coordinator {
        return error_page("Report Data Systems", "Client has no VR Counselor").await;
    }
    let vr_counselor_support_coordinator_uuid =
        client.vr_counselor_support_coordinator.unwrap().to_string();
    */
    /*
    if let None = client.supervisor {
        return error_page("Report Data Systems", "Client has no supervisor").await;
    }
    if let None = client.employer {
        return error_page("Report Data Systems", "Client has no employer").await;
    }
    */
    if !key_exists!(
        req.clone(),
        "/client_list/{client_uuid}/report_data/ongoing_supports/jc_monthly"
    )
    .await
    {
        write_value(
            get_app_data(req.clone()),
            format!("/client_list/{client_uuid}/report_data/ongoing_supports/jc_monthly").as_str(),
            Vec::<reports::ongoing_supports::monthly_jc_report::Report>::new(),
        )
        .await
        .unwrap();
    }
    if !key_exists!(
        req.clone(),
        "/client_list/{client_uuid}/report_data/ongoing_supports/jc_monthly_partial"
    )
    .await
    {
        write_value(
            get_app_data(req.clone()),
            format!("/client_list/{client_uuid}/report_data/ongoing_supports/jc_monthly_partial")
                .as_str(),
            reports::ongoing_supports::monthly_jc_report::ReportBuilder::new(
                employer.unwrap_or(String::new()),
                form.crp.clone().unwrap(),
                current_client
                    .vr_counselor_support_coordinator
                    .clone()
                    .unwrap(),
                supervisor.unwrap_or(String::new()),
                form.req_counselor_contact.clone().is_some(),
                get_value!(req.clone(), "/employee_list/{username}/timezone").unwrap(),
            ),
        )
        .await
        .unwrap();
    }
    /*
    let mut jc_monthly_reports: Vec<reports::ongoing_supports::monthly_jc_report::Report> = get_value!(
        req.clone(),
        "/client_list/{client_uuid}/ongoing_supports/jc_monthly"
    )
    .unwrap();
    let mut current_report = jc_monthly_reports.iter().last().unwrap();
    */
    let mut current_report_builder: reports::ongoing_supports::monthly_jc_report::ReportBuilder =
        get_value!(
            req.clone(),
            "/client_list/{client_uuid}/report_data/ongoing_supports/jc_monthly_partial"
        )
        .unwrap();
    //println!("{:#?}", current_report_builder);
    if let Ok(_) = current_report_builder.add_observation(
        reports::ongoing_supports::monthly_jc_report::Observation {
            date: Utc::today().and_hms(0, 0, 0),
            attendance: reports::Rating::from(form.attendance.clone()).unwrap(),
            time_management: reports::Rating::from(form.time_management.clone()).unwrap(),
            appearance: reports::Rating::from(form.appearance.clone()).unwrap(),
            communication: reports::Rating::from(form.communication.clone()).unwrap(),
            jt_completion_rate: reports::Rating::from(form.jt_completion_rate.clone()).unwrap(),
            jt_quality: reports::Rating::from(form.jt_quality.clone()).unwrap(),
        },
    ) {
        write_value(
            get_app_data(req.clone()),
            format!("/client_list/{client_uuid}/report_data/ongoing_supports/jc_monthly_partial")
                .as_str(),
            current_report_builder.clone(),
        )
        .await
        .unwrap();
        if current_report_builder.observations.len() == 2 {
            let complete_report: reports::ongoing_supports::monthly_jc_report::Report =
                current_report_builder
                    .build(
                        form.overall_appraisal.as_ref().unwrap().clone(),
                        form.problems.as_ref().unwrap().clone(),
                        form.interventions_used.as_ref().unwrap().clone(),
                        form.recommended_interventions.as_ref().unwrap().clone(),
                        form.req_counselor_contact.is_some(),
                        form.crp_signature.clone().unwrap(),
                    )
                    .unwrap();
            write_value(
                get_app_data(req.clone()),
                format!("/client_list/{client_uuid}/report_data/ongoing_supports/jc_monthly")
                    .as_str(),
                vec![complete_report],
            )
            .await
            .unwrap();
        }
        redirect("/")
    } else {
        error_page("Report Data Systems", "Error adding observation").await
    }
}

#[post("/reports/ongoing_supports/monthly_jc.rs")]
async fn monthly_jc_report_post(
    req: HttpRequest,
    form: web::Form<MonthlyJCObservationReportPost>,
) -> HttpResponse {
    check_cookies_and_username_req_form(req.clone(), form, monthly_jc_post_internal, "none").await
}

#[derive(Template)]
#[template(path = "report_index.html")]
struct ReportIndexPageTemplate {
    navigation_bar: NavBarTemplate,
    report_urls: Vec<(String, String)>,
}

async fn report_index_page_internal(req: HttpRequest, username: String) -> HttpResponse {
    utils::respond_html(
        ReportIndexPageTemplate {
            navigation_bar: NavBarTemplate::new(req.clone(), username),
            report_urls: get_value!(req.clone(), "/report_index_list").unwrap(),
        }
        .render()
        .unwrap(),
    )
}

#[get("/report_index")]
async fn report_index_page(req: HttpRequest) -> HttpResponse {
    check_cookies_and_username(req.clone(), report_index_page_internal, "none").await
}

#[derive(Template)]
#[template(path = "monthly_jc_report.html")]
struct MonthlyJCReport {
    report_state: reports::ongoing_supports::monthly_jc_report::ReportState,
    navigation_bar: NavBarTemplate,
    is_job_coaching: bool,
}

fn is_jc_monthly_done(req: HttpRequest, username: String, client_uuid: String) -> bool {
    let time_zone: Tz = get_value!(req.clone(), "/employee_list/{username}/timezone").unwrap();
    let report_list_result: Result<
        Vec<reports::ongoing_supports::monthly_jc_report::Report>,
        acid_store::Error,
    > = get_value!(
        req.clone(),
        "/client_list/{client_uuid}/report_data/ongoing_supports/jc_monthly"
    );
    if let Ok(report_list) = report_list_result {
        if report_list.len() > 0 {
            let latest_report = report_list.last().unwrap();
            let date_time = latest_report.date.with_timezone(&time_zone);
            let now = Utc::now().with_timezone(&time_zone);
            now.month() == date_time.month() && now.year() == date_time.year()
        } else {
            false
        }
    } else {
        false
    }
}

async fn monthly_jc_report_page_internal(req: HttpRequest, username: String) -> HttpResponse {
    let mut is_job_coaching = false;
    let mut state = reports::ongoing_supports::monthly_jc_report::ReportState::First;
    let employee = get_employee_record(req.clone(), username.clone())
        .unwrap()
        .unwrap();
    if employee.is_clocked_in().await {
        if let Direction::In(_, client_uuid) = employee
            .clocked_times
            .iter()
            .last()
            .unwrap()
            .extract_direction()
        {
            update_monthly_jc_report(req.clone(), username.clone(), client_uuid.clone()).await;
            let current_client: client::Client =
                get_value!(req.clone(), "/client_list/{client_uuid}/record").unwrap();
            if current_client.service_provided == client::ClientService::JobCoaching {
                is_job_coaching = true;
            }
            if !current_client
                .assoc_service_uuids
                .contains_key(&client::ClientService::JobDevelopment)
            {
                return error_page("Job Coaching Systems", "This client must be associsted with a jd client of the same name and have a job before filling this form").await;
            }
            //is_first_observation = observations.len() == 0;
            let is_first_observation = !key_exists!(
                req.clone(),
                "/client_list/{client_uuid}/report_data/ongoing_supports/jc_monthly_partial"
            )
            .await;
            if !is_first_observation {
                let report_builder: reports::ongoing_supports::monthly_jc_report::ReportBuilder = get_value!(req.clone(), "/client_list/{client_uuid}/report_data/ongoing_supports/jc_monthly_partial").unwrap();
                //println!("{:#?}", report_builder);
                if report_builder.observations.len() == 2 {
                    state = reports::ongoing_supports::monthly_jc_report::ReportState::AlreadyTwo;
                } else {
                    state = reports::ongoing_supports::monthly_jc_report::ReportState::NotFirst;
                }
            } else if is_jc_monthly_done(req.clone(), username.clone(), client_uuid) {
                state = reports::ongoing_supports::monthly_jc_report::ReportState::Approved;
            }
        }
    }
    utils::respond_html(
        MonthlyJCReport {
            //already_two: already_two,
            //is_first_observation: is_first_observation,
            report_state: state,
            navigation_bar: NavBarTemplate::new(req.clone(), username),
            is_job_coaching: is_job_coaching,
        }
        .render()
        .unwrap(),
    )
}

#[get("/reports/ongoing_supports/monthly_jc")]
async fn monthly_jc_report_page(req: HttpRequest) -> HttpResponse {
    check_cookies_and_username(req.clone(), monthly_jc_report_page_internal, "none").await
}

fn list_all_service_areas(req: HttpRequest) -> Vec<client::ServiceArea> {
    get_value!(req.clone(), "/service_area_list").unwrap()
}

fn list_all_billing_offices(req: HttpRequest) -> Vec<reports::BillingOffice> {
    get_value!(req.clone(), "/billing_office_list").unwrap()
}

#[derive(Debug, Clone, Serialize, Deserialize)]
struct AddAuthorizationPost {
    auth_num: String,
    fname: String,
    lname: String,
    client_uuid: String,
    service_area: String,
    vr_counselor_support_coordinator: String,
    service_provided: String,
    rate: f64,
    start_date: NaiveDate,
    end_date: NaiveDate,
    hours_authed: Option<usize>,
    units_authed: Option<usize>,
    mmu: String,
    billing_office: String,
    service_type: Option<String>,
    assoc: Option<String>, //office: String,
}

async fn add_authorization_post_internal(
    username: String,
    req: HttpRequest,
    form: web::Form<AddAuthorizationPost>,
) -> HttpResponse {
    let service_type: Option<reports::billing_tracking_vr_jd::Service>;
    if let Some(service_typ) = &form.service_type {
        service_type = Some(reports::billing_tracking_vr_jd::Service::from(
            service_typ.clone(),
        ));
    } else {
        service_type = None;
    }
    let vr_counselor_support_coordinator: Option<String>;
    if form.vr_counselor_support_coordinator.clone() == String::new() {
        vr_counselor_support_coordinator = None;
    } else {
        vr_counselor_support_coordinator = Some(form.vr_counselor_support_coordinator.clone());
    }
    let assoc: BTreeMap<client::ClientService, String> = match form.assoc {
        Some(_) => {
            let mut ret_assoc = BTreeMap::new();
            if let Some(current_client) = get_current_client(req.clone(), username.clone()).await {
                ret_assoc.insert(
                    current_client.service_provided,
                    current_client.uuid.to_string(),
                );
            }
            ret_assoc
        }
        None => BTreeMap::new(),
    };
    match add_authorization(
        req.clone(),
        get_value!(req.clone(), "/employee_list/{username}/timezone").unwrap(),
        form.auth_num.clone(),
        form.client_uuid.clone(),
        form.fname.clone(),
        form.lname.clone(),
        form.service_area.clone(),
        /*
        employer,
        supervisor,
        */
        vr_counselor_support_coordinator,
        client::ClientService::from(form.service_provided.clone()),
        form.rate,
        std::time::Duration::from_secs((60 * 60 * form.hours_authed.unwrap()).try_into().unwrap()),
        //form.hours_authed.clone().unwrap(),
        form.start_date,
        form.end_date,
        None,
        form.billing_office.clone(),
        service_type,
        //form.office.clone(),
        assoc,
    )
    .await
    {
        Ok(_) => utils::respond_html(
            AddAuthorizationPageTemplate {
                navigation_bar: NavBarTemplate::new(req.clone(), username.clone()),
                success: Some(true),
                service_areas: list_all_service_areas(req.clone()),
                waiting_clients_list: get_waiting_clients(req.clone()).await,
                //vr_counselor_support_coordinator: vr_counselor_support_coordinator,
                available_services: client::ClientService::list_all_authorization_types(),
                is_authorization: true,
                billing_offices: list_all_billing_offices(req.clone()),
                //offices: get_value!(req.clone(), "/office_location_list").unwrap(),
            }
            .render()
            .unwrap(),
        ),
        Err(_) => utils::respond_html(
            AddAuthorizationPageTemplate {
                navigation_bar: NavBarTemplate::new(req.clone(), username.clone()),
                success: Some(false),
                service_areas: list_all_service_areas(req.clone()),
                waiting_clients_list: get_waiting_clients(req.clone()).await,
                //vr_counselor_support_coordinators: vr_counselor_support_coordinators,
                available_services: client::ClientService::list_all_authorization_types(),
                is_authorization: true,
                billing_offices: list_all_billing_offices(req.clone()),
                //offices: get_value!(req.clone(), "/office_location_list").unwrap(),
            }
            .render()
            .unwrap(),
        ),
    }
}

async fn get_waiting_clients(req: HttpRequest) -> Vec<WaitingListClient> {
    let client_uuid_list: Vec<String> = get_value!(req.clone(), "/client_list").unwrap();
    let all_clients: Vec<String> = get_value!(req.clone(), "/waiting_clients_list").unwrap();
    all_clients
        .into_iter()
        .filter_map(|x| {
            if !client_uuid_list.contains(&x) {
                let waiting_client: WaitingListClient =
                    get_value!(req.clone(), "/waiting_clients_list/{x}/record").unwrap();
                Some(waiting_client)
            } else {
                None
            }
        })
        .collect()
}

async fn add_budget_line_post_internal(
    username: String,
    req: HttpRequest,
    form: web::Form<AddAuthorizationPost>,
) -> HttpResponse {
    let service_type: Option<reports::billing_tracking_vr_jd::Service>;
    if let Some(service_typ) = &form.service_type {
        service_type = Some(reports::billing_tracking_vr_jd::Service::from(
            service_typ.clone(),
        ));
    } else {
        service_type = None;
    }
    let vr_counselor_support_coordinator: Option<String>;
    if form.vr_counselor_support_coordinator.clone() == String::new() {
        vr_counselor_support_coordinator = None;
    } else {
        vr_counselor_support_coordinator = Some(form.vr_counselor_support_coordinator.clone());
    }
    let mmu: Option<usize>;
    if form.mmu.clone() == String::new() {
        mmu = None;
    } else {
        mmu = Some(form.mmu.clone().parse().unwrap());
    }
    let assoc: BTreeMap<client::ClientService, String> = match form.assoc {
        Some(_) => {
            let mut ret_assoc = BTreeMap::new();
            if let Some(current_client) = get_current_client(req.clone(), username.clone()).await {
                ret_assoc.insert(
                    current_client.service_provided,
                    current_client.uuid.to_string(),
                );
            }
            ret_assoc
        }
        None => BTreeMap::new(),
    };
    match add_authorization(
        req.clone(),
        get_value!(req.clone(), "/employee_list/{username}/timezone").unwrap(),
        form.auth_num.clone(),
        form.client_uuid.clone(),
        form.fname.clone(),
        form.lname.clone(),
        form.service_area.clone(),
        /*
        employer,
        supervisor,
        */
        vr_counselor_support_coordinator,
        client::ClientService::from(form.service_provided.clone()),
        form.rate,
        std::time::Duration::from_secs((60 * 15 * form.units_authed.unwrap()).try_into().unwrap()),
        form.start_date,
        form.end_date,
        mmu,
        form.billing_office.clone(),
        service_type,
        assoc, //form.office.clone(),
    )
    .await
    {
        Ok(_) => utils::respond_html(
            AddAuthorizationPageTemplate {
                navigation_bar: NavBarTemplate::new(req.clone(), username.clone()),
                success: Some(true),
                waiting_clients_list: get_waiting_clients(req.clone()).await,
                service_areas: list_all_service_areas(req.clone()),
                //vr_counselor_support_coordinator: vr_counselor_support_coordinator,
                available_services: client::ClientService::list_all_budget_line_types(),
                is_authorization: false,
                billing_offices: list_all_billing_offices(req.clone()),
                //offices: get_value!(req.clone(), "/office_location_list").unwrap(),
            }
            .render()
            .unwrap(),
        ),
        Err(_) => utils::respond_html(
            AddAuthorizationPageTemplate {
                navigation_bar: NavBarTemplate::new(req.clone(), username.clone()),
                success: Some(false),
                service_areas: list_all_service_areas(req.clone()),
                waiting_clients_list: get_waiting_clients(req.clone()).await,
                //vr_counselor_support_coordinators: vr_counselor_support_coordinators,
                available_services: client::ClientService::list_all_budget_line_types(),
                is_authorization: false,
                billing_offices: list_all_billing_offices(req.clone()),
                //offices: get_value!(req.clone(), "/office_location_list").unwrap(),
            }
            .render()
            .unwrap(),
        ),
    }
}
/*
async fn add_vr_counselor_support_coordinator_post_internal(
    username: String,
    req: HttpRequest,
    form: web::Form<AddVRCounselorSupportCoordinatorPost>,
) -> HttpResponse {
    match add_vr_counselor_support_coordinator(req.clone(), form.fname.clone(), form.lname.clone())
        .await
    {
        Ok(_) => utils::respond_html(
            AddVRCounselorSupportCoordinatorPageTemplate {
                navigation_bar: NavBarTemplate::new(req.clone(), username.clone()),
                success: Some(true),
            }
            .render()
            .unwrap(),
        ),
        Err(_) => utils::respond_html(
            AddVRCounselorSupportCoordinatorPageTemplate {
                navigation_bar: NavBarTemplate::new(req.clone(), username.clone()),
                success: Some(false),
            }
            .render()
            .unwrap(),
        ),
    }
}
*/
#[post("/add_authorization.rs")]
async fn add_authorization_post(
    req: HttpRequest,
    form: web::Form<AddAuthorizationPost>,
) -> HttpResponse {
    check_cookies_and_username_req_form(
        req,
        form,
        add_authorization_post_internal,
        "add_authorization",
    )
    .await
}

#[post("/add_budget_line.rs")]
async fn add_budget_line_post(
    req: HttpRequest,
    form: web::Form<AddAuthorizationPost>,
) -> HttpResponse {
    check_cookies_and_username_req_form(
        req,
        form,
        add_budget_line_post_internal,
        "add_authorization",
    )
    .await
}
/*
#[post("/add_vr_counselor_support_coordinator.rs")]
async fn add_vr_counselor_support_coordinator_post(
    req: HttpRequest,
    form: web::Form<AddVRCounselorSupportCoordinatorPost>,
) -> HttpResponse {
    check_cookies_and_username_req_form(
        req,
        form,
        add_vr_counselor_support_coordinator_post_internal,
        "add_vr_counselor_support_coordinator",
    )
    .await
}
*/
async fn add_authorization_internal(req: HttpRequest, username: String) -> HttpResponse {
    utils::respond_html(
        AddAuthorizationPageTemplate {
            navigation_bar: NavBarTemplate::new(req.clone(), username),
            success: None,
            service_areas: list_all_service_areas(req.clone()),
            waiting_clients_list: get_waiting_clients(req.clone()).await,
            //vr_counselor_support_coordinators: vr_counselor_support_coordinators,
            available_services: client::ClientService::list_all_authorization_types(),
            is_authorization: true,
            billing_offices: list_all_billing_offices(req.clone()),
            //offices: get_value!(req.clone(), "/office_location_list").unwrap(),
        }
        .render()
        .unwrap(),
    )
}
async fn add_budget_line_internal(req: HttpRequest, username: String) -> HttpResponse {
    utils::respond_html(
        AddAuthorizationPageTemplate {
            navigation_bar: NavBarTemplate::new(req.clone(), username),
            success: None,
            service_areas: list_all_service_areas(req.clone()),
            //vr_counselor_support_coordinators: vr_counselor_support_coordinators,
            waiting_clients_list: get_waiting_clients(req.clone()).await,
            available_services: client::ClientService::list_all_budget_line_types(),
            is_authorization: false,
            billing_offices: list_all_billing_offices(req.clone()),
            //offices: get_value!(req.clone(), "/office_location_list").unwrap(),
        }
        .render()
        .unwrap(),
    )
}
/*
async fn add_vr_counselor_support_coordinator_internal(
    req: HttpRequest,
    username: String,
) -> HttpResponse {
    utils::respond_html(
        AddVRCounselorSupportCoordinatorPageTemplate {
            navigation_bar: NavBarTemplate::new(req, username),
            success: None,
        }
        .render()
        .unwrap(),
    )
}
*/
#[get("/add_authorization")]
async fn add_authorization_page(req: HttpRequest) -> HttpResponse {
    check_cookies_and_username(req.clone(), add_authorization_internal, "add_authorization").await
}

#[get("/add_budget_line")]
async fn add_budget_line_page(req: HttpRequest) -> HttpResponse {
    check_cookies_and_username(req.clone(), add_budget_line_internal, "add_authorization").await
}
/*
#[get("/add_vr_counselor_support_coordinator")]
async fn add_vr_counselor_support_coordinator_page(req: HttpRequest) -> HttpResponse {
    check_cookies_and_username(
        req.clone(),
        add_vr_counselor_support_coordinator_internal,
        "add_authorization",
    )
    .await
}
*/

#[get("/client_intake")]
async fn client_intake_page(req: HttpRequest) -> HttpResponse {
    let template = ClientIntakePageTemplate { success: None };
    tms::utils::respond_html(template.render().unwrap())
}

#[get("/setup")]
async fn setup_page(req: HttpRequest) -> HttpResponse {
    let is_setup: bool = read_value(
        req.app_data::<web::Data<Sender<SyncMessage>>>()
            .unwrap()
            .clone(),
        "/setup_done",
    )
    .unwrap();
    if !is_setup {
        let offices: Vec<String> =
            read_value(get_app_data(req.clone()), "/office_location_list").unwrap();
        let template = SetupPageTemplate { offices: offices };
        tms::utils::respond_html(template.render().unwrap())
    } else {
        error_page("Setup", "Setup has already been completed.").await
    }
}

#[derive(Serialize, Deserialize, Debug, Clone)]
struct SetupPost {
    user: String,
    email: String,
    fname: String,
    lname: String,
    passwd: String,
    cpasswd: String,
    dept: String,
    jobd: String,
    office: String,
}

#[derive(Debug, Clone)]
enum EmployeeError {
    UsernameAlreadyExists,
    InvalidUsername,
}

enum SessionError {
    NotRegistered,
    SessionIDMismatch,
    NotLoggedIn,
    NoCookies,
}

#[derive(Serialize, Deserialize, Debug, Clone)]
struct ReportPost {
    employee: String,
    start_date: NaiveDate,
    end_date: NaiveDate,
}

fn get_employee_record(
    req: HttpRequest,
    username: String,
) -> Result<Option<Employee>, acid_store::Error> {
    match get_value!(req.clone(), "/employee_list/{username}/record")? {
        EmployeeRecord::Registered(employee) => Ok(Some(employee)),
        EmployeeRecord::NotRegistered(_) => Ok(None),
    }
}

fn calculate_duration_dates_with_client(
    req: HttpRequest,
    username: String,
    client_uuid: String,
    _start_date: DateTime<Utc>,
    _end_date: DateTime<Utc>,
) -> chrono::Duration {
    let mut should_count = false;
    //let utc_start_date = start_date.clone();
    //let utc_end_date = end_date.clone();
    let now = Utc::now();
    let now_time = now.time();
    let date_time =
        Utc.ymd(now.year(), now.month(), now.day())
            .and_hms(now_time.hour(), now_time.minute(), 0);

    let this_employee = get_employee_record(req.clone(), username.clone())
        .unwrap()
        .unwrap();
    //let mut last_clock_was_in = false;
    let mut clocked_in_duration = chrono::Duration::seconds(0);
    let mut clocked_times = this_employee
        .clocked_times
        .iter()
        .filter_map(|x| {
            //if x.extract_datetime() >= utc_start_date && x.extract_datetime() < utc_end_date {
            if let employee::ClockedTime::ClientService(_) = x {
                Some((x.extract_datetime(), x.extract_client_uuid()))
            } else {
                None
            }
            //} else {
            //    None
            //}
        })
        .collect::<Vec<(DateTime<Utc>, Option<String>)>>();

    if clocked_times.len() == 0 {
        //println!("test");
        return clocked_in_duration;
    }
    let position = this_employee
        .clocked_times
        .iter()
        .position(|x| x.extract_datetime() == clocked_times[0].0)
        .unwrap();
    if position != 0 {
        if this_employee.clocked_times[position - 1]
            .clone()
            .is_clocking_in()
        {
            let prev_clocked_time = this_employee.clocked_times[position - 1].clone();
            //prev_clocked_time = prev_clocked_time.with_modified_datetime(utc_start_date);
            clocked_times.insert(
                0,
                (
                    prev_clocked_time.extract_datetime(),
                    prev_clocked_time.extract_client_uuid(),
                ),
            );
        }
    }
    /*
    if clocked_times.len() < 1 {
        return error_page(
            "Reports",
            format!(
                "{} {} did not clock in during this period",
                employee.first_name, employee.last_name
            ),
        )
        .await;
    }
    */
    let final_num = clocked_times.len() - 1;
    for (num, clocked_time) in clocked_times.iter().enumerate() {
        if final_num == num {
            if num % 2 == 0 {
                if num == 0 {
                    should_count = true;
                }
                let final_comparison: DateTime<Utc>;
                //if utc_end_date >= date_time {
                final_comparison = date_time.clone();
                //} else {
                //    final_comparison = utc_end_date.clone();
                //}
                if should_count {
                    clocked_in_duration = clocked_in_duration
                        .checked_add(&final_comparison.signed_duration_since(clocked_times[num].0))
                        .unwrap();
                }
            } else {
                if should_count {
                    clocked_in_duration = clocked_in_duration
                        .checked_add(
                            &clocked_times[num]
                                .0
                                .signed_duration_since(clocked_times[num - 1].clone().0),
                        )
                        .unwrap();
                }
            }
        } else if num == 0 {
            if clocked_times[0].1.clone().unwrap() == client_uuid {
                should_count = true;
            } else {
                should_count = false;
            }
        } else if num % 2 == 1 {
            if should_count {
                clocked_in_duration = clocked_in_duration
                    .checked_add(
                        &clocked_time
                            .0
                            .signed_duration_since(clocked_times[num - 1].0.clone()),
                    )
                    .unwrap();
            }
        } else {
            if clocked_times[num].clone().1.unwrap() == client_uuid {
                should_count = true;
            } else {
                should_count = false;
            }
        }
    }
    clocked_in_duration
}

fn calculate_duration_dates(
    req: HttpRequest,
    username: String,
    start_date: DateTime<Utc>,
    end_date: DateTime<Utc>,
) -> chrono::Duration {
    let utc_start_date = start_date.clone();
    let utc_end_date = end_date.clone();
    let now = Utc::now();
    let now_time = now.time();
    let date_time =
        Utc.ymd(now.year(), now.month(), now.day())
            .and_hms(now_time.hour(), now_time.minute(), 0);

    let this_employee = get_employee_record(req.clone(), username.clone())
        .unwrap()
        .unwrap();
    //let mut last_clock_was_in = false;
    let mut clocked_in_duration = chrono::Duration::seconds(0);
    //println!("{:#?} - {:#?}", utc_start_date, utc_end_date);
    let mut clocked_times = this_employee
        .clocked_times
        .iter()
        .filter_map(|x| {
            if x.extract_datetime() >= utc_start_date && x.extract_datetime() < utc_end_date {
                Some(x.clone())
            } else {
                None
            }
            //} else {
            //    None
            //}
        })
        .collect::<Vec<ClockedTime>>();

    if this_employee.clocked_times.len() == 0 {
        //println!("test");
        return clocked_in_duration;
    }
    /*
    let position: usize;
    if clocked_times.len() == 0 {
        position = this_employee.clocked_times.last()
    }
    if position != 0 {
        */
    //if clocked_times.len() % 2 == 1 {
    let mut prev_clocked_time: Option<employee::ClockedTime> = None;
    if clocked_times.len() == 0 {
        prev_clocked_time = Some(this_employee.clocked_times.last().unwrap().clone());
        //clocked_times.push(prev_clocked_time.with_modified_datetime(start_date));
    } else {
        let position = this_employee
            .clocked_times
            .iter()
            .position(|x| x == &clocked_times[0])
            .unwrap();
        /*
        println!(
            "{:#?} {:#?}",
            this_employee.clocked_times.clone(),
            clocked_times.clone()
        );
        */
        if clocked_times[0] != this_employee.clocked_times[0] {
            prev_clocked_time = Some(this_employee.clocked_times[position - 1].clone());
        }
    }
    if let Some(prev_clocked) = prev_clocked_time {
        //let mut prev_clocked_time = this_employee.clocked_times.last().unwrap().clone();
        if prev_clocked.is_clocking_in() {
            clocked_times.insert(0, prev_clocked.with_modified_datetime(start_date));
        }
    }
    //}

    let final_num = clocked_times.len() - 1;
    for (num, clocked_time) in clocked_times.iter().enumerate() {
        if final_num == num {
            if num % 2 == 0 {
                /*
                if num == 0 {
                    should_count = true;
                }
                */
                let final_comparison: DateTime<Utc>;
                //if utc_end_date >= date_time {
                final_comparison = date_time.clone();
                //} else {
                //    final_comparison = utc_end_date.clone();
                //}
                clocked_in_duration = clocked_in_duration
                    .checked_add(
                        &final_comparison
                            .signed_duration_since(clocked_times[num].extract_datetime()),
                    )
                    .unwrap();
            } else {
                clocked_in_duration = clocked_in_duration
                    .checked_add(
                        &clocked_times[num].extract_datetime().signed_duration_since(
                            clocked_times[num - 1].clone().extract_datetime(),
                        ),
                    )
                    .unwrap();
            }
        } else if num % 2 == 1 {
            clocked_in_duration = clocked_in_duration
                .checked_add(
                    &clocked_time
                        .extract_datetime()
                        .signed_duration_since(clocked_times[num - 1].extract_datetime().clone()),
                )
                .unwrap();
        } /*else {
              if clocked_times[num].clone().1.unwrap() == client_uuid {
                  should_count = true;
              } else {
                  should_count = false;
              }
          }*/
    }
    clocked_in_duration
}

async fn report_post_internal(
    username: String,
    req: HttpRequest,
    form: web::Form<ReportPost>,
) -> HttpResponse {
    let now = Utc::now();
    let now_time = now.time();
    let date_time =
        Utc.ymd(now.year(), now.month(), now.day())
            .and_hms(now_time.hour(), now_time.minute(), 0);

    let username_list: Vec<String> =
        read_value(get_app_data(req.clone()), "/employee_list").unwrap();
    let employee_list: Vec<Employee> = username_list
        .iter()
        .filter_map(|employee_record| {
            if let EmployeeRecord::Registered(employee) =
                get_value!(req.clone(), "/employee_list/{employee_record}/record").unwrap()
            {
                Some(employee)
            } else {
                None
            }
        })
        .collect();
    let local_tz: Tz = get_value!(req.clone(), "/employee_list/{username}/timezone").unwrap();
    let local_start_date: DateTime<Tz> = local_tz
        .ymd(
            form.start_date.year(),
            form.start_date.month(),
            form.start_date.day(),
        )
        .and_hms(0, 0, 0);
    let local_end_date: DateTime<Tz> = local_tz
        .ymd(
            form.end_date.year(),
            form.end_date.month(),
            form.end_date.day(),
        )
        .and_hms(0, 0, 0);
    let utc_start_date = local_start_date.with_timezone(&Utc);
    let utc_end_date = local_end_date.with_timezone(&Utc);
    let employee = get_employee_record(req.clone(), form.employee.clone())
        .unwrap()
        .unwrap();
    //let mut last_clock_was_in = false;
    let mut clocked_in_duration = chrono::Duration::seconds(0);
    let mut clocked_times = employee
        .clocked_times
        .iter()
        .filter_map(|x| {
            if x.extract_datetime() >= utc_start_date && x.extract_datetime() < utc_end_date {
                Some(x.extract_datetime())
            } else {
                None
            }
        })
        .collect::<Vec<DateTime<Utc>>>();
    let position = employee
        .clocked_times
        .iter()
        .position(|x| x.extract_datetime() == clocked_times[0])
        .unwrap();
    if position != 0 {
        if employee.clocked_times[position - 1]
            .clone()
            .is_clocking_in()
        {
            let mut prev_clocked_time = employee.clocked_times[position - 1].clone();
            prev_clocked_time =
                prev_clocked_time.with_modified_datetime(local_start_date.with_timezone(&Utc));
            clocked_times.insert(0, prev_clocked_time.extract_datetime());
        }
    }
    if clocked_times.len() < 1 {
        return error_page(
            "Reports",
            format!(
                "{} {} did not clock in during this period",
                employee.first_name, employee.last_name
            ),
        )
        .await;
    }
    let final_num = clocked_times.len() - 1;
    for (num, clocked_time) in clocked_times.iter().enumerate() {
        if final_num == num {
            if num % 2 == 0 {
                let final_comparison: DateTime<Utc>;
                if utc_end_date >= date_time {
                    final_comparison = date_time.clone();
                } else {
                    final_comparison = utc_end_date.clone();
                }
                clocked_in_duration = clocked_in_duration
                    .checked_add(&final_comparison.signed_duration_since(clocked_times[num]))
                    .unwrap();
            } else {
                clocked_in_duration = clocked_in_duration
                    .checked_add(
                        &clocked_times[num].signed_duration_since(clocked_times[num - 1].clone()),
                    )
                    .unwrap();
            }
        } else if num == 0 {
            continue;
        } else if num % 2 == 1 {
            clocked_in_duration = clocked_in_duration
                .checked_add(&clocked_time.signed_duration_since(clocked_times[num - 1].clone()))
                .unwrap();
        }
    }
    utils::respond_html(
        ReportPageTemplate {
            navigation_bar: NavBarTemplate::new(req.clone(), username.clone()),
            employees: employee_list,
            output_report: Some(format!(
                "{} {} was clocked in for {} during the period of {} to {}.",
                employee.first_name,
                employee.last_name,
                format_duration_hm(clocked_in_duration),
                local_start_date,
                local_end_date
            )),
        }
        .render()
        .unwrap(),
    )
}

#[post("/report.rs")]
async fn report_post(req: HttpRequest, form: web::Form<ReportPost>) -> HttpResponse {
    check_cookies_and_username_req_form(req, form, report_post_internal, "report").await
}

#[derive(Serialize, Deserialize, Debug, Clone)]
struct SettingsPost {
    timezone: Option<Tz>,
}

async fn settings_post_internal(
    username: String,
    req: HttpRequest,
    form: web::Form<SettingsPost>,
) -> HttpResponse {
    let success = Some(
        write_value(
            get_app_data(req.clone()),
            format!("/employee_list/{username}/timezone").as_str(),
            form.timezone,
        )
        .await
        .is_ok(),
    );
    let current_timezone = read_value::<Tz>(
        get_app_data(req.clone()),
        format!("/employee_list/{username}/timezone").as_str(),
    )
    .unwrap();
    let mut timezones: Vec<Tz> = Vec::new();
    timezones.push(current_timezone.clone());
    let mut timezone_vec = TZ_VARIANTS.to_vec();
    timezone_vec.retain(|x| x != &current_timezone);
    timezones.append(&mut timezone_vec);
    utils::respond_html(
        SettingsPageTemplate {
            success: success,
            navigation_bar: NavBarTemplate::new(req.clone(), username),
            timezones: timezones,
        }
        .render()
        .unwrap(),
    )
}

#[post("/settings.rs")]
async fn settings_post(req: HttpRequest, form: web::Form<SettingsPost>) -> HttpResponse {
    check_cookies_and_username_req_form(req, form, settings_post_internal, "none").await
}

#[derive(Template)]
#[template(path = "settings.html")]
struct SettingsPageTemplate {
    navigation_bar: NavBarTemplate,
    //current_timezone: Tz,
    success: Option<bool>,
    timezones: Vec<Tz>, //[Tz; 594],
}

async fn settings_page_internal(req: HttpRequest, username: String) -> HttpResponse {
    let current_timezone = read_value::<Tz>(
        get_app_data(req.clone()),
        format!("/employee_list/{username}/timezone").as_str(),
    )
    .unwrap();
    let mut timezones: Vec<Tz> = Vec::new();
    timezones.push(current_timezone.clone());
    let mut timezone_vec = TZ_VARIANTS.to_vec();
    timezone_vec.retain(|x| x != &current_timezone);
    timezones.append(&mut timezone_vec);
    utils::respond_html(
        SettingsPageTemplate {
            success: None,
            navigation_bar: NavBarTemplate::new(req, username),
            timezones: timezones,
        }
        .render()
        .unwrap(),
    )
}

#[get("/settings")]
async fn settings_page(req: HttpRequest) -> HttpResponse {
    check_cookies_and_username(req, settings_page_internal, "none").await
}

async fn add_employee(
    req: HttpRequest,
    employee: Employee,
    insert: bool,
) -> Result<(), EmployeeError> {
    let username = employee.clone().username;
    if insert {
        //write_value(get_app_data(req.clone()), format!("/employee_list/{username}/record").as_str(), EmployeeRecord::Registered(employee.clone())).await.unwrap();
        batch_write_value!(
            req.clone()
                .app_data::<web::Data<Sender<SyncMessage>>>()
                .unwrap()
                .clone(),
            (
                "/employee_list/{username}/record",
                EmployeeRecord::Registered(employee.clone())
            ),
            (
                "/employee_list/{username}/timezone",
                chrono_tz::America::Denver
            ),
            ("/employee_list/{username}/caseload", "none"),
            (
                "/employee_list/{username}/caseload_list",
                Vec::<String>::new()
            )
        )
        .unwrap();
        return Ok(());
    }
    if username.contains("/") || username.is_empty() {
        return Err(EmployeeError::InvalidUsername);
    }
    let mut employee_list: Vec<String> = read_value(
        req.app_data::<web::Data<Sender<SyncMessage>>>()
            .unwrap()
            .clone(),
        "/employee_list",
    )
    .unwrap();
    if !employee_list.contains(&username.clone()) {
        //drop(employee_list);
        let employee_record = EmployeeRecord::Registered(employee.clone());
        //employee_list.insert(username, employee_record
        employee_list.push(employee.username);
        batch_write_value!(
            req.clone()
                .app_data::<web::Data<Sender<SyncMessage>>>()
                .unwrap()
                .clone(),
            ("/employee_list/{username}/record", employee_record),
            (
                "/employee_list/{username}/timezone",
                chrono_tz::America::Denver
            ),
            ("/employee_list", employee_list),
            ("/employee_list/{username}/caseload", "none"),
            (
                "/employee_list/{username}/caseload_list",
                Vec::<String>::new()
            )
        )
        .unwrap();
        Ok(())
    } else {
        Err(EmployeeError::UsernameAlreadyExists)
    }
}

#[derive(Template)]
#[template(path = "navigation_bar.html")]
struct NavBarTemplate {
    report_allowed: bool,
    add_authorization_allowed: bool,
    add_employee_allowed: bool,
    add_service_area_allowed: bool,
    add_billing_office_allowed: bool,
    manage_caseloads_allowed: bool,
}

impl NavBarTemplate {
    pub fn new(req: HttpRequest, username: String) -> Self {
        let employee = get_employee_record(req, username).unwrap().unwrap();
        Self {
            report_allowed: employee.is_allowed("report"),
            add_authorization_allowed: employee.is_allowed("add_authorization"),
            add_employee_allowed: employee.is_allowed("add_employee"),
            add_service_area_allowed: employee.is_allowed("add_service_area"),
            add_billing_office_allowed: employee.is_allowed("add_billing_office"),
            manage_caseloads_allowed: employee.is_allowed("manage_caseloads"),
        }
    }
}

#[derive(Template)]
#[template(path = "report.html")]
struct ReportPageTemplate {
    navigation_bar: NavBarTemplate,
    employees: Vec<Employee>,
    output_report: Option<String>,
}

#[get("/report")]
async fn report_page(req: HttpRequest) -> HttpResponse {
    check_cookies_and_username(req, report_page_internal, "report").await
}

fn get_app_data(req: HttpRequest) -> web::Data<Sender<SyncMessage>> {
    req.app_data::<web::Data<Sender<SyncMessage>>>()
        .unwrap()
        .clone()
}
/*
fn username_allowed(username: String, req: HttpRequest, required_access: String) -> bool {
    if let EmployeeRecord::Registered(employee) = read_value(
        req.clone()
            .app_data::<web::Data<Sender<SyncMessage>>>()
            .unwrap()
            .clone(),
        format!("/employee_list/{username}/record").as_str(),
    )
    .unwrap()
    {
        employee.is_allowed(required_access)
    } else {
        false
    }
}
*/
//async fn employee_record_filter_map(req: HttpRequest, username: String) -> Option<Employee> {}

async fn report_page_internal(req: HttpRequest, username: String) -> HttpResponse {
    let username_list: Vec<String> =
        read_value(get_app_data(req.clone()), "/employee_list").unwrap();
    let employee_list: Vec<Employee> = username_list
        .iter()
        .filter_map(|employee_record| {
            if let EmployeeRecord::Registered(employee) = read_value(
                get_app_data(req.clone()),
                format!("/employee_list/{employee_record}/record").as_str(),
            )
            .unwrap()
            {
                Some(employee)
            } else {
                None
            }
        })
        .collect();
    utils::respond_html(
        ReportPageTemplate {
            navigation_bar: NavBarTemplate::new(req.clone(), username),
            employees: employee_list,
            output_report: None,
        }
        .render()
        .unwrap(),
    )
}

#[derive(Template)]
#[template(path = "home.html")]
struct HomePageTemplate {
    navigation_bar: NavBarTemplate,
    valid_clock_types: Vec<String>,
    can_clock_in: bool,
    show_got_job: bool,
    clients: Vec<client::Client>,
    show_times: Option<(Vec<(String, String, String, String)>, String)>,
    dur_remain_day: String,
    dur_remain_week: String,
    dur_remain_pay_period: String,
}

fn get_current_pay_period(date: NaiveDate) -> (NaiveDate, NaiveDate) {
    let orig_pay_start = NaiveDate::from_ymd(2022, 9, 17);
    let diff = date - orig_pay_start;
    let num_weeks = diff.num_weeks();
    let iso_week = date.iso_week().week();
    let prev_week = date
        .checked_sub_signed(chrono::Duration::weeks(1))
        .unwrap()
        .year()
        < date.year();
    let next_week = date
        .checked_add_signed(chrono::Duration::weeks(1))
        .unwrap()
        .year()
        > date.year();
    if num_weeks % 2 == 0 {
        if date.weekday() == Weekday::Sat {
            (
                date,
                date.checked_add_signed(chrono::Duration::weeks(2)).unwrap(),
            )
        } else {
            let end_date: NaiveDate;
            if !next_week {
                end_date =
                    NaiveDate::from_isoywd(date.year(), date.iso_week().week(), Weekday::Sat);
            } else {
                end_date = NaiveDate::from_isoywd(date.year() + 1, 1, Weekday::Sat);
            }
            (
                end_date
                    .checked_sub_signed(chrono::Duration::weeks(2))
                    .unwrap(),
                end_date,
            )
        }
    } else {
        if date.weekday() == Weekday::Sat {
            let start_date = date.checked_sub_signed(chrono::Duration::weeks(1)).unwrap();
            (
                start_date,
                date.checked_add_signed(chrono::Duration::weeks(1)).unwrap(),
            )
        } else {
            if !prev_week {
                let start_date = NaiveDate::from_isoywd(date.year(), iso_week - 1, Weekday::Sat);
                (
                    start_date,
                    start_date
                        .checked_add_signed(chrono::Duration::weeks(2))
                        .unwrap(),
                )
            } else {
                let end_date = NaiveDate::from_isoywd(date.year(), iso_week + 1, Weekday::Sat);
                (
                    end_date
                        .checked_sub_signed(chrono::Duration::weeks(2))
                        .unwrap(),
                    end_date,
                )
            }
        }
    }
}

fn get_current_work_week(date: NaiveDate) -> (NaiveDate, NaiveDate) {
    let iso_week = date.iso_week().week();
    let prev_week = date
        .checked_sub_signed(chrono::Duration::weeks(1))
        .unwrap()
        .year()
        < date.year();
    let next_week = date
        .checked_add_signed(chrono::Duration::weeks(1))
        .unwrap()
        .year()
        > date.year();
    if date.weekday() == Weekday::Mon {
        (
            date,
            date.checked_add_signed(chrono::Duration::weeks(1)).unwrap(),
        )
    } else {
        if !prev_week {
            let start_date = match date.weekday() {
                Weekday::Sun => NaiveDate::from_isoywd(date.year(), iso_week - 1, Weekday::Mon),
                _ => NaiveDate::from_isoywd(date.year(), iso_week, Weekday::Mon),
            };
            (
                start_date,
                start_date
                    .checked_add_signed(chrono::Duration::weeks(1))
                    .unwrap(),
            )
        } else {
            let end_date = NaiveDate::from_isoywd(date.year(), iso_week + 1, Weekday::Mon);
            (
                end_date
                    .checked_sub_signed(chrono::Duration::weeks(1))
                    .unwrap(),
                end_date,
            )
        }
    }
}

impl HomePageTemplate {
    pub async fn new(req: HttpRequest, username: String) -> Result<Self, SessionError> {
        if let Ok(EmployeeRecord::Registered(this_employee)) = read_value(
            req.clone()
                .app_data::<web::Data<Sender<SyncMessage>>>()
                .unwrap()
                .clone(),
            format!("/employee_list/{username}/record").as_str(),
        ) {
            if this_employee.clocked_times.len() == 0 {
                let employee_username = this_employee.username.clone();
                let local_tz: Tz =
                    get_value!(req.clone(), "/employee_list/{employee_username}/timezone").unwrap();
                let local_now = get_now_minutes().with_timezone(&local_tz);
                let local_today = local_now.date();
                let today =
                    NaiveDate::from_ymd(local_today.year(), local_today.month(), local_today.day());
                let pay_period_range = get_current_pay_period(today);
                let work_week_range = get_current_work_week(today);
                let caseload_uuid: String =
                    get_value!(req.clone(), "/employee_list/{employee_username}/caseload").unwrap();
                let client_uuid_list: Vec<String>;
                if caseload_uuid == "none" {
                    client_uuid_list = Vec::new();
                } else {
                    let this_caseload: caseload::CaseLoad =
                        get_value!(req.clone(), "/caseload_list/{caseload_uuid}/record").unwrap();
                    client_uuid_list = this_caseload.client_uuids;
                    /*
                    let local_tz: Tz =
                        get_value!(req.clone(), "/employee_list/{employee_username}/timezone")
                            .unwrap();
                    //let local_now = get_now_minutes().with_timezone(&local_tz);
                    //let local_today = local_now.date();

                    let today = NaiveDate::from_ymd(
                        local_today.year(),
                        local_today.month(),
                        local_today.day(),
                    );
                    */
                    //let pay_period_range = get_current_pay_period(today);
                    //let work_week_range = get_current_work_week(today);
                }
                Ok(Self {
                    show_times: calculate_clocks_today(req.clone(), this_employee.clone()).await,
                    clients: client_uuid_list
                        .iter()
                        .filter_map(|x| {
                            if x != "none" {
                                let this_client: client::Client =
                                    get_value!(req.clone(), "/client_list/{x}/record").unwrap();
                                /*
                                let client_office = this_client.office.clone();
                                if client_office.matches(this_employee.clone().office) {
                                    Some(this_client.clone())
                                } else {
                                    None
                                }
                                */
                                Some(this_client)
                            } else {
                                None
                            }
                        })
                        .collect(),
                    navigation_bar: NavBarTemplate::new(req.clone(), username),
                    valid_clock_types: this_employee.valid_clock_types,
                    can_clock_in: true,
                    show_got_job: false,
                    /*
                    dur_remain_day: String::new(),
                    dur_remain_week: String::new(),
                    dur_remain_pay_period: String::new(),
                    */
                    dur_remain_pay_period: format_duration_hm(
                        chrono::Duration::hours(80)
                            .checked_sub(&calculate_duration_dates(
                                req.clone(),
                                employee_username.clone(),
                                local_tz
                                    .ymd(
                                        pay_period_range.0.year(),
                                        pay_period_range.0.month(),
                                        pay_period_range.0.day(),
                                    )
                                    .and_hms(0, 0, 0)
                                    .with_timezone(&Utc),
                                local_tz
                                    .ymd(
                                        pay_period_range.1.year(),
                                        pay_period_range.1.month(),
                                        pay_period_range.1.day(),
                                    )
                                    .and_hms(0, 0, 0)
                                    .with_timezone(&Utc),
                            ))
                            .unwrap(),
                    ),
                    dur_remain_week: format_duration_hm(
                        chrono::Duration::hours(40)
                            .checked_sub(&calculate_duration_dates(
                                req.clone(),
                                employee_username.clone(),
                                local_tz
                                    .ymd(
                                        work_week_range.0.year(),
                                        work_week_range.0.month(),
                                        work_week_range.0.day(),
                                    )
                                    .and_hms(0, 0, 0)
                                    .with_timezone(&Utc),
                                local_tz
                                    .ymd(
                                        work_week_range.1.year(),
                                        work_week_range.1.month(),
                                        work_week_range.1.day(),
                                    )
                                    .and_hms(0, 0, 0)
                                    .with_timezone(&Utc),
                            ))
                            .unwrap(),
                    ),
                    dur_remain_day: format_duration_hm(
                        chrono::Duration::hours(8)
                            .checked_sub(&calculate_duration_dates(
                                req.clone(),
                                employee_username.clone(),
                                local_today.and_hms(0, 0, 0).with_timezone(&Utc),
                                local_today
                                    .and_hms(0, 0, 0)
                                    .checked_add_signed(chrono::Duration::days(1))
                                    .unwrap()
                                    .with_timezone(&Utc),
                            ))
                            .unwrap(),
                    ),
                })
            } else if this_employee
                .clocked_times
                .iter()
                .last()
                .unwrap()
                .is_clocking_in()
            {
                let employee_username = this_employee.username.clone();
                let caseload_uuid: String =
                    get_value!(req.clone(), "/employee_list/{employee_username}/caseload").unwrap();
                let this_caseload: caseload::CaseLoad =
                    get_value!(req.clone(), "/caseload_list/{caseload_uuid}/record").unwrap();
                let client_uuid_list = this_caseload.client_uuids;
                let this_client = get_current_client(req.clone(), employee_username.clone())
                    .await
                    .unwrap();
                let client_uuid = this_client.uuid;
                let clocked_in_jd =
                    this_client.service_provided == client::ClientService::JobDevelopment;
                let local_tz: Tz =
                    get_value!(req.clone(), "/employee_list/{employee_username}/timezone").unwrap();
                let local_now = get_now_minutes().with_timezone(&local_tz);
                let local_today = local_now.date();
                let today =
                    NaiveDate::from_ymd(local_today.year(), local_today.month(), local_today.day());
                let pay_period_range = get_current_pay_period(today);
                let work_week_range = get_current_work_week(today);
                //println!("{:#?}", work_week_range);
                Ok(Self {
                    show_times: calculate_clocks_today(req.clone(), this_employee.clone()).await,
                    clients: client_uuid_list
                        .iter()
                        .filter_map(|x| {
                            if x != "none" {
                                let this_client: client::Client =
                                    get_value!(req.clone(), "/client_list/{x}/record").unwrap();
                                /*
                                let client_office = this_client.office.clone();
                                if client_office.matches(this_employee.clone().office) {
                                    Some(this_client.clone())
                                } else {
                                    None
                                }
                                */
                                Some(this_client)
                            } else {
                                None
                            }
                        })
                        .collect(),
                    navigation_bar: NavBarTemplate::new(req.clone(), username),
                    valid_clock_types: vec![],
                    can_clock_in: false,
                    show_got_job: clocked_in_jd
                        && !key_exists!(
                            req.clone(),
                            "/client_list/{client_uuid}/report_data/jd/cie_jp"
                        )
                        .await,
                    dur_remain_pay_period: format_duration_hm(
                        chrono::Duration::hours(80)
                            .checked_sub(&calculate_duration_dates(
                                req.clone(),
                                employee_username.clone(),
                                local_tz
                                    .ymd(
                                        pay_period_range.0.year(),
                                        pay_period_range.0.month(),
                                        pay_period_range.0.day(),
                                    )
                                    .and_hms(0, 0, 0)
                                    .with_timezone(&Utc),
                                local_tz
                                    .ymd(
                                        pay_period_range.1.year(),
                                        pay_period_range.1.month(),
                                        pay_period_range.1.day(),
                                    )
                                    .and_hms(0, 0, 0)
                                    .with_timezone(&Utc),
                            ))
                            .unwrap(),
                    ),
                    dur_remain_week: format_duration_hm(
                        chrono::Duration::hours(40)
                            .checked_sub(&calculate_duration_dates(
                                req.clone(),
                                employee_username.clone(),
                                local_tz
                                    .ymd(
                                        work_week_range.0.year(),
                                        work_week_range.0.month(),
                                        work_week_range.0.day(),
                                    )
                                    .and_hms(0, 0, 0)
                                    .with_timezone(&Utc),
                                local_tz
                                    .ymd(
                                        work_week_range.1.year(),
                                        work_week_range.1.month(),
                                        work_week_range.1.day(),
                                    )
                                    .and_hms(0, 0, 0)
                                    .with_timezone(&Utc),
                            ))
                            .unwrap(),
                    ),
                    dur_remain_day: format_duration_hm(
                        chrono::Duration::hours(8)
                            .checked_sub(&calculate_duration_dates(
                                req.clone(),
                                employee_username.clone(),
                                local_today.and_hms(0, 0, 0).with_timezone(&Utc),
                                local_today
                                    .and_hms(0, 0, 0)
                                    .checked_add_signed(chrono::Duration::days(1))
                                    .unwrap()
                                    .with_timezone(&Utc),
                            ))
                            .unwrap(),
                    ),
                })
            } else {
                let employee_username = this_employee.username.clone();
                let caseload_uuid: String =
                    get_value!(req.clone(), "/employee_list/{employee_username}/caseload").unwrap();
                let this_caseload: caseload::CaseLoad =
                    get_value!(req.clone(), "/caseload_list/{caseload_uuid}/record").unwrap();
                let client_uuid_list = this_caseload.client_uuids;
                let local_tz: Tz =
                    get_value!(req.clone(), "/employee_list/{employee_username}/timezone").unwrap();
                let local_now = get_now_minutes().with_timezone(&local_tz);
                let local_today = local_now.date();
                let today =
                    NaiveDate::from_ymd(local_today.year(), local_today.month(), local_today.day());
                let pay_period_range = get_current_pay_period(today);
                let work_week_range = get_current_work_week(today);
                Ok(Self {
                    show_times: calculate_clocks_today(req.clone(), this_employee.clone()).await,
                    clients: client_uuid_list
                        .iter()
                        .filter_map(|x| {
                            if x != "none" {
                                let this_client: client::Client =
                                    get_value!(req.clone(), "/client_list/{x}/record").unwrap();
                                /*
                                let client_office = this_client.office.clone();
                                if client_office.matches(this_employee.clone().office) {
                                    Some(this_client.clone())
                                } else {
                                    None
                                }
                                */
                                Some(this_client)
                            } else {
                                None
                            }
                        })
                        .collect(),
                    navigation_bar: NavBarTemplate::new(req.clone(), username),
                    valid_clock_types: this_employee.valid_clock_types,
                    can_clock_in: true,
                    show_got_job: false,
                    /*
                    dur_remain_day: String::new(),
                    dur_remain_week: String::new(),
                    dur_remain_pay_period: String::new(),
                    */
                    dur_remain_pay_period: format_duration_hm(
                        chrono::Duration::hours(80)
                            .checked_sub(&calculate_duration_dates(
                                req.clone(),
                                employee_username.clone(),
                                local_tz
                                    .ymd(
                                        pay_period_range.0.year(),
                                        pay_period_range.0.month(),
                                        pay_period_range.0.day(),
                                    )
                                    .and_hms(0, 0, 0)
                                    .with_timezone(&Utc),
                                local_tz
                                    .ymd(
                                        pay_period_range.1.year(),
                                        pay_period_range.1.month(),
                                        pay_period_range.1.day(),
                                    )
                                    .and_hms(0, 0, 0)
                                    .with_timezone(&Utc),
                            ))
                            .unwrap(),
                    ),
                    dur_remain_week: format_duration_hm(
                        chrono::Duration::hours(40)
                            .checked_sub(&calculate_duration_dates(
                                req.clone(),
                                employee_username.clone(),
                                local_tz
                                    .ymd(
                                        work_week_range.0.year(),
                                        work_week_range.0.month(),
                                        work_week_range.0.day(),
                                    )
                                    .and_hms(0, 0, 0)
                                    .with_timezone(&Utc),
                                local_tz
                                    .ymd(
                                        work_week_range.1.year(),
                                        work_week_range.1.month(),
                                        work_week_range.1.day(),
                                    )
                                    .and_hms(0, 0, 0)
                                    .with_timezone(&Utc),
                            ))
                            .unwrap(),
                    ),
                    dur_remain_day: format_duration_hm(
                        chrono::Duration::hours(8)
                            .checked_sub(&calculate_duration_dates(
                                req.clone(),
                                employee_username.clone(),
                                local_today.and_hms(0, 0, 0).with_timezone(&Utc),
                                local_today
                                    .and_hms(0, 0, 0)
                                    .checked_add_signed(chrono::Duration::days(1))
                                    .unwrap()
                                    .with_timezone(&Utc),
                            ))
                            .unwrap(),
                    ),
                })
            }
        } else {
            Err(SessionError::NotRegistered)
        }
    }
}

#[derive(Template)]
#[template(path = "login.html")]
struct LoginPageTemplate {
    invalid: bool,
}

#[get("/")]
async fn home_page(req: HttpRequest) -> HttpResponse {
    match check_cookies(req.clone()).await {
        Ok(username) => match HomePageTemplate::new(req.clone(), username).await {
            Ok(template) => utils::respond_html(template.render().unwrap()),
            Err(SessionError::NotRegistered) => {
                error_page("Session Processing", "Employee not registered").await
            }
            Err(_) => error_page("Impossible Error", "This error shouldn't happen").await,
        },
        Err(SessionError::NotRegistered) => {
            error_page("Employee Sessions", "No such employee").await
        }
        //Err(SessionError::SessionIDMismatch) => HttpResponse::SeeOther()
        Err(SessionError::SessionIDMismatch) => {
            utils::respond_html(LoginPageTemplate { invalid: false }.render().unwrap())
        }
            //.insert_header(("Location", "/"))
            //.finish(),
        Err(SessionError::NotLoggedIn) => {
            utils::respond_html(LoginPageTemplate { invalid: false }.render().unwrap())
        }
        Err(SessionError::NoCookies) => {
            utils::respond_html(LoginPageTemplate { invalid: false }.render().unwrap())
        }//HttpResponse::SeeOther()
            //.insert_header(("Location", "/"))
            //.finish(),
    }
}

async fn logout_internal(req: HttpRequest, username: String) -> HttpResponse {
    remove_value(
        req.clone()
            .app_data::<web::Data<Sender<SyncMessage>>>()
            .unwrap()
            .clone(),
        format!("/employee_list/{username}/session").as_str(),
    )
    .await
    .unwrap();
    redirect("/")
}

#[get("/logout")]
async fn logout(req: HttpRequest) -> HttpResponse {
    check_cookies_and_username(req.clone(), logout_internal, "none").await
}

#[derive(Serialize, Deserialize, Debug, Clone)]
struct LoginPost {
    user: String,
    passwd: String,
}

#[post("/login.rs")]
async fn login_post(req: HttpRequest, form: web::Form<LoginPost>) -> HttpResponse {
    let username = form.clone().user;
    match read_value(
        req.clone()
            .app_data::<web::Data<Sender<SyncMessage>>>()
            .unwrap()
            .clone(),
        format!("/employee_list/{username}/record").as_str(),
    ) {
        Ok(EmployeeRecord::Registered(employee)) => {
            if employee.verify_password(form.clone().passwd).await {
                let session_id = uuid::Uuid::new_v4().to_string();
                write_value(
                    req.clone()
                        .app_data::<web::Data<Sender<SyncMessage>>>()
                        .unwrap()
                        .clone(),
                    format!("/employee_list/{username}/session").as_str(),
                    session_id.clone(),
                )
                .await
                .unwrap();
                HttpResponse::SeeOther()
                    .insert_header(("Location", "/choose_caseload"))
                    .cookie(Cookie::new("username", username))
                    .cookie(Cookie::new("session_id", session_id))
                    .finish()
            } else {
                utils::respond_html(LoginPageTemplate { invalid: true }.render().unwrap())
            }
        }
        Ok(EmployeeRecord::NotRegistered(_)) => {
            error_page(
                "Employee Login",
                "This employee has not been registered yet.",
            )
            .await
        }
        Err(_) => error_page("Employee Login", "No such employee").await,
    }
}

#[derive(Serialize, Deserialize, Debug, Clone)]
struct ClockPost {
    clock_type: Option<String>,
    clock_in: String,
    client_uuid: Option<String>,
    client_got_job: Option<String>,
}

fn redirect(location: impl Into<String>) -> HttpResponse {
    HttpResponse::SeeOther()
        .insert_header(("Location", location.into().as_str()))
        .finish()
}

#[derive(Template)]
#[template(path = "jc_schedule.html")]
struct JCSchedulePageTemplate {
    navigation_bar: NavBarTemplate,
    success: Option<bool>,
}

#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct JCSchedulePost {
    start_hour: usize,
    start_minute: usize,
    start_am: String,
    end_hour: usize,
    end_minute: usize,
    end_am: String,
    //task_label: String,
}

async fn client_schedule_row_post_internal(
    username: String,
    req: HttpRequest,
    form: web::Form<JCSchedulePost>,
) -> HttpResponse {
    if let Some(current_client) = get_current_client(req.clone(), username.clone()).await {
        let client_uuid = current_client.uuid;
        let mut data_vec: Vec<reports::jc::tracker::ClientScheduleRow>;
        if !key_exists!(
            req.clone(),
            "/client_list/{client_uuid}/report_data/jc/client_schedule"
        )
        .await
        {
            data_vec = Vec::new();
        } else {
            data_vec = get_value!(
                req.clone(),
                "/client_list/{client_uuid}/report_data/jc/client_schedule"
            )
            .unwrap();
        }
        let start_am: bool;
        if form.start_am == String::from("true") {
            start_am = true;
        } else if form.start_am == String::from("false") {
            start_am = false;
        } else {
            return error_page(
                "Client Scheduling Systems",
                "start_am must be true or false",
            )
            .await;
        }
        let end_am: bool;
        if form.end_am == String::from("true") {
            end_am = true;
        } else if form.end_am == String::from("false") {
            end_am = false;
        } else {
            return error_page("Client Scheduling Systems", "end_am must be true or false").await;
        }
        let valid_minutes = vec![0, 15, 30, 45];
        if form.start_hour < 1
            || form.start_hour > 12
            || form.end_hour < 1
            || form.end_hour > 12
            || !valid_minutes.contains(&form.start_minute)
            || !valid_minutes.contains(&form.end_minute)
        {
            return error_page("Client Scheduling Systems", "Invalid time").await;
        }
        data_vec.push(reports::jc::tracker::ClientScheduleRow {
            start_hour: form.start_hour,
            start_minute: form.start_minute,
            start_am: start_am,
            end_hour: form.end_hour,
            end_minute: form.end_minute,
            end_am: end_am,
            //label: form.task_label.clone(),
        });
        let success = Some(
            write_value(
                get_app_data(req.clone()),
                format!("/client_list/{client_uuid}/report_data/jc/client_schedule").as_str(),
                data_vec,
            )
            .await
            .is_ok(),
        );
        utils::respond_html(
            JCSchedulePageTemplate {
                navigation_bar: NavBarTemplate::new(req.clone(), username),
                success: success,
            }
            .render()
            .unwrap(),
        )
    } else {
        error_page(
            "Client Scheduling Systems",
            "You are not clocked in for a client",
        )
        .await
    }
}

#[post("/reports/jc/client_schedule.rs")]
async fn client_scheudule_row_post(
    req: HttpRequest,
    form: web::Form<JCSchedulePost>,
) -> HttpResponse {
    check_cookies_and_username_req_form(
        req.clone(),
        form,
        client_schedule_row_post_internal,
        "none",
    )
    .await
}

#[get("/reports/jc/client_schedule")]
async fn client_schedule_page(req: HttpRequest) -> HttpResponse {
    check_cookies_and_username(req.clone(), client_schedule_page_internal, "none").await
}

async fn client_schedule_page_internal(req: HttpRequest, username: String) -> HttpResponse {
    if let Some(current_client) = get_current_client(req.clone(), username.clone()).await {
        if current_client.service_provided == client::ClientService::JobCoaching {
            return utils::respond_html(
                JCSchedulePageTemplate {
                    navigation_bar: NavBarTemplate::new(req.clone(), username),
                    success: None,
                }
                .render()
                .unwrap(),
            );
        }
    }
    error_page("Client Scheduling Systems", "You are not clocked in for a client or the client service you are clocked in for is not job coaching").await
}

#[derive(Template)]
#[template(path = "billing_tracking_vr_hourly.html")]
struct BillingTrackingVRHourlyPageTemplate {
    navigation_bar: NavBarTemplate,
    success: Option<bool>,
}

#[derive(Template)]
#[template(path = "billing_tracking_vr_hourly_table.html")]
struct BillingTrackingVRHourlyTableTemplate {
    navigation_bar: NavBarTemplate,
    table_rows: Vec<(
        String,
        String,
        String,
        String,
        String,
        String,
        String,
        String,
        String,
        String,
        String,
        String,
    )>,
}

#[derive(Template)]
#[template(path = "jc_schedule_table.html")]
struct JCScheduleTablePageTemplate {
    navigation_bar: NavBarTemplate,
    table_rows: Vec<(String, String)>,
}

async fn jc_client_schedule_table_page_internal(
    req: HttpRequest,
    username: String,
) -> HttpResponse {
    let table: Vec<reports::jc::tracker::ClientScheduleRow>;
    if let Some(current_client) = get_current_client(req.clone(), username.clone()).await {
        if current_client.service_provided == client::ClientService::JobCoaching {
            let client_uuid = current_client.uuid;
            if key_exists!(
                req.clone(),
                "/client_list/{client_uuid}/report_data/jc/client_schedule"
            )
            .await
            {
                table = get_value!(
                    req.clone(),
                    "/client_list/{client_uuid}/report_data/jc/client_schedule"
                )
                .unwrap();
            } else {
                table = Vec::new();
            }
            return utils::respond_html(
                JCScheduleTablePageTemplate {
                    navigation_bar: NavBarTemplate::new(req.clone(), username),
                    table_rows: table
                        .iter()
                        .map(|x| {
                            let mut start_hour = x.start_hour - 1;
                            if !x.start_am {
                                start_hour += 12;
                            }
                            let start_time = chrono::NaiveTime::from_hms(
                                start_hour.try_into().unwrap(),
                                x.start_minute.try_into().unwrap(),
                                0,
                            );
                            let mut end_hour = x.end_hour - 1;
                            if !x.end_am {
                                end_hour += 12;
                            }
                            let end_time = chrono::NaiveTime::from_hms(
                                end_hour.try_into().unwrap(),
                                x.end_minute.try_into().unwrap(),
                                0,
                            );
                            (
                                start_time.format("%I:%M %p").to_string(),
                                //x.label.clone(),
                                end_time.format("%I:%M %p").to_string(),
                            )
                        })
                        .collect(),
                }
                .render()
                .unwrap(),
            );
        }
    }
    error_page("Client Scheduling Systems", "You are not clocked in for a client or the client service you are clocked in for is not job coaching").await
}

#[get("/reports/jc/client_schedule_table")]
async fn jc_client_schedule_table_page(req: HttpRequest) -> HttpResponse {
    check_cookies_and_username(
        req.clone(),
        jc_client_schedule_table_page_internal,
        "report",
    )
    .await
}

async fn billing_tracking_table_page_internal(req: HttpRequest, username: String) -> HttpResponse {
    let table: Vec<reports::billing_tracking_vr_hourly::BillingTrackingVRHourlyRow>;
    if key_exists!(req.clone(), "/report_data/billing_tracking_vr_hourly").await {
        table = get_value!(req.clone(), "/report_data/billing_tracking_vr_hourly").unwrap();
    } else {
        table = Vec::new();
    }
    utils::respond_html(
        BillingTrackingVRHourlyTableTemplate {
            navigation_bar: NavBarTemplate::new(req.clone(), username),
            table_rows: table
                .iter()
                .map(|x| {
                    let currently_working_with: String;
                    if x.currently_working_with {
                        currently_working_with = String::from("Y");
                    } else {
                        currently_working_with = String::from("N");
                    }
                    let this_employee =
                        get_employee_record(req.clone(), x.employee_username.clone())
                            .unwrap()
                            .unwrap();
                    let client_uuid = x.client_uuid;
                    //let counselor_uuid = x.vr_counselor_support_coordinator_uuid;
                    let current_client: client::Client =
                        get_value!(req.clone(), "/client_list/{client_uuid}/record").unwrap();
                    /*
                    let counselor: String = get_value!(
                        req.clone(),
                        "/vr_counselor_support_coordinator_list/{counselor_uuid}/record"
                    )
                    .unwrap();
                    */
                    let duration_authed =
                        chrono::Duration::from_std(x.duration_authorized).unwrap();
                    let duration_billed = chrono::Duration::from_std(x.duration_billed).unwrap();
                    let duration_unbilled =
                        //chrono::Duration::from_std(x.duration_unbilled).unwrap();
                        calculate_duration_dates_with_client(req.clone(), this_employee.username.clone(), current_client.uuid.to_string(), Utc::now(), Utc::now()).checked_sub(&duration_billed).unwrap();
                    let duration_remaining = duration_authed
                        .checked_sub(&duration_billed.checked_add(&duration_unbilled).unwrap())
                        .unwrap();
                    let mut durations: Vec<String> = Vec::new();
                    for d in vec![
                        duration_authed,
                        duration_billed,
                        duration_unbilled,
                        duration_remaining,
                    ] {
                        let num_hours = d.num_hours();
                        let num_minutes = d
                            .checked_sub(&chrono::Duration::hours(num_hours))
                            .unwrap()
                            .num_minutes();
                        let string_minutes: String;
                        if num_minutes == 0 {
                            string_minutes = String::new();
                        } else if num_minutes == 15 {
                            string_minutes = String::from(".25");
                        } else if num_minutes == 30 {
                            string_minutes = String::from(".5");
                        } else {
                            //if num_minutes == 45 {
                            string_minutes = String::from(".75");
                        }
                        durations.push(format!("{num_hours}{string_minutes}"));
                    }
                    (
                        format!("{} {}", current_client.first_name, current_client.last_name),
                        current_client.service_area.get_name(),
                        current_client
                            .vr_counselor_support_coordinator
                            .clone()
                            .unwrap(),
                        //format!("{} {}", counselor.first_name, counselor.last_name),
                        format!("{} {}", this_employee.first_name, this_employee.last_name),
                        x.auth_type.to_string().to_uppercase(),
                        x.auth_num.clone(),
                        durations[0].clone(),
                        durations[1].clone(),
                        durations[2].clone(),
                        durations[3].clone(),
                        currently_working_with,
                        x.notes.clone(),
                    )
                })
                .collect(),
        }
        .render()
        .unwrap(),
    )
}

#[get("/reports/billing_tracking_table")]
async fn billing_tracking_table_page(req: HttpRequest) -> HttpResponse {
    check_cookies_and_username(req.clone(), billing_tracking_table_page_internal, "report").await
}

#[derive(Debug, Clone, Serialize, Deserialize)]
struct BillingTrackingVRHourlyPost {
    //auth_num: String,
    //hours_authed: usize,
    hours_billed: usize,
    minutes_billed: usize,
    //hours_unbilled: usize,
    //minutes_unbilled: usize,
    //currently_working_with: Option<String>,
    notes: String,
}

async fn add_billing_tracking_row_post_internal(
    username: String,
    req: HttpRequest,
    form: web::Form<BillingTrackingVRHourlyPost>,
) -> HttpResponse {
    let (client_result, this_employee) =
        get_current_client_employee(req.clone(), username.clone()).await;
    if let Some(current_client) = client_result {
        let client_uuid = current_client.uuid;
        let potential_duration = chrono::Duration::hours(form.hours_billed.try_into().unwrap())
            .checked_add(&chrono::Duration::minutes(
                form.minutes_billed.try_into().unwrap(),
            ))
            .unwrap();
        if potential_duration
            > calculate_duration_dates_with_client(
                req.clone(),
                username.clone(),
                client_uuid.to_string(),
                Utc::now(),
                Utc::now(),
            )
        {
            return error_page(
                "VR Hourly Systems",
                "You have not been clocked in with the client for that long",
            )
            .await;
        }
        let mut data_vec: Vec<reports::billing_tracking_vr_hourly::BillingTrackingVRHourlyRow>;
        if !key_exists!(req.clone(), "/report_data/billing_tracking_vr_hourly").await {
            data_vec = Vec::new();
        } else {
            data_vec = get_value!(req.clone(), "/report_data/billing_tracking_vr_hourly").unwrap();
        }
        data_vec.push(
            reports::billing_tracking_vr_hourly::BillingTrackingVRHourlyRow::new(
                client_uuid,
                username.clone(),
                this_employee.office,
                current_client.vr_counselor_support_coordinator.unwrap(),
                client::ClientService::JobCoaching,
                current_client.auth_num.clone(),
                //form.hours_authed,
                chrono::Duration::from_std(current_client.duration_authed)
                    .unwrap()
                    .num_hours()
                    .try_into()
                    .unwrap(),
                form.hours_billed,
                form.minutes_billed,
                /*
                form.hours_unbilled,
                form.minutes_unbilled,
                */
                // form.currently_working_with.is_some(),
                form.notes.clone(),
            ),
        );
        let success = write_value(
            get_app_data(req.clone()),
            "/report_data/billing_tracking_vr_hourly",
            data_vec,
        )
        .await
        .is_ok();
        return utils::respond_html(
            BillingTrackingVRHourlyPageTemplate {
                navigation_bar: NavBarTemplate::new(req.clone(), username),
                success: Some(success),
            }
            .render()
            .unwrap(),
        );
    }
    error_page(
        "Billing Tracking Systems",
        "You are not clocked in for a client",
    )
    .await
}

#[post("/add_billing_tracking_row.rs")]
async fn add_billing_tracking_row_post(
    req: HttpRequest,
    form: web::Form<BillingTrackingVRHourlyPost>,
) -> HttpResponse {
    check_cookies_and_username_req_form(
        req.clone(),
        form,
        add_billing_tracking_row_post_internal,
        "none",
    )
    .await
}

async fn billing_tracking_vr_hourly_report_page_internal(
    req: HttpRequest,
    username: String,
) -> HttpResponse {
    if let Some(_current_client) = get_current_client(req.clone(), username.clone()).await {
        utils::respond_html(
            BillingTrackingVRHourlyPageTemplate {
                navigation_bar: NavBarTemplate::new(req.clone(), username),
                success: None,
            }
            .render()
            .unwrap(),
        )
    } else {
        error_page(
            "Billing Tracking Reporting Systems",
            "You are not clocked in for a client",
        )
        .await
    }
}

#[get("/reports/billing_tracking_vr_hourly")]
async fn billing_tracking_vr_hourly_report_page(req: HttpRequest) -> HttpResponse {
    check_cookies_and_username(
        req.clone(),
        billing_tracking_vr_hourly_report_page_internal,
        "none",
    )
    .await
}

#[derive(Template)]
#[template(path = "jc_tracker_report.html")]
struct JCTrackerReportPageTemplate {
    navigation_bar: NavBarTemplate,
    success: Option<bool>,
    days: Vec<usize>,
}

async fn jc_tracker_report_page_internal(req: HttpRequest, username: String) -> HttpResponse {
    if let Some(current_client) = get_current_client(req.clone(), username.clone()).await {
        if current_client.service_provided == client::ClientService::JobCoaching {
            let local_tz: Tz =
                get_value!(req.clone(), "/employee_list/{username}/timezone").unwrap();
            let local_now = Utc::now().with_timezone(&local_tz);
            return utils::respond_html(
                JCTrackerReportPageTemplate {
                    navigation_bar: NavBarTemplate::new(req.clone(), username),
                    success: None,
                    days: (1..calculate_num_days_month(chrono::NaiveDate::from_ymd(
                        local_now.year(),
                        local_now.month(),
                        local_now.day(),
                    )) + 1)
                        .collect(),
                }
                .render()
                .unwrap(),
            );
        }
    }
    error_page("Report Exporting Systems", "You are not clocked in for a client or the client service you are clocked in for is not job coaching").await
}

#[get("/reports/jc/tracker")]
async fn jc_tracker_report_page(req: HttpRequest) -> HttpResponse {
    check_cookies_and_username(req.clone(), jc_tracker_report_page_internal, "none").await
}

#[derive(Debug, Clone, Serialize, Deserialize)]
struct JCTrackerReportPost {
    days_month: usize,
    hours: String,
    minutes: String,
    primary: Option<Vec<String>>,
    secondary: Option<Vec<String>>,
}

async fn jc_tracker_report_post_internal(
    username: String,
    req: HttpRequest,
    mut payload: web::Payload,
) -> HttpResponse {
    // payload is a stream of Bytes objects
    let mut body = web::BytesMut::new();
    while let Some(chunk) = payload.next().await {
        let chunk = chunk.unwrap();
        // limit max size of in-memory payload
        if (body.len() + chunk.len()) > 262_144 {
            return error_page("Report Management Systems", "Overflow").await;
        }
        body.extend_from_slice(&chunk);
    }
    let config = serde_qs::Config::new(1, false);
    // body is loaded, now we can deserialize serde-jsoni
    let primary: Vec<reports::jc::tracker::CoachingServices>;
    let form: JCTrackerReportPost = config.deserialize_bytes(&body).unwrap();
    if let Some(primary_orig) = form.primary {
        primary = primary_orig
            .iter()
            .filter_map(|z| {
                if z.to_string().starts_with("s") {
                    if let Ok(num) = z[1..].parse::<usize>() {
                        if num > 0 && num <= 15 {
                            return Some(reports::jc::tracker::CoachingServices::from(z.clone()));
                        }
                    }
                }
                None
            })
            .collect();
    } else {
        return error_page(
            "Report Processing Systems",
            "No valid services input for primary",
        )
        .await;
    }
    let secondary: Vec<reports::jc::tracker::CoachingServices>;
    if let Some(secondary_orig) = form.secondary {
        secondary = secondary_orig
            .iter()
            .filter_map(|z| {
                if z.to_string().starts_with("s") {
                    if let Ok(num) = z[1..].parse::<usize>() {
                        if num > 0 && num <= 15 {
                            return Some(reports::jc::tracker::CoachingServices::from(z.clone()));
                        }
                    }
                }
                None
            })
            .collect();
    } else {
        return error_page(
            "Report Processing Systems",
            "No valid services input for secondary",
        )
        .await;
    }
    let mut successes: Vec<bool> = Vec::new();
    let client_result = get_current_client(req.clone(), username.clone()).await;
    if let Some(current_client) = client_result {
        let client_uuid = current_client.uuid;
        if !key_exists!(
            req.clone(),
            "/client_list/{client_uuid}/report_data/jc/tracker_partial_list"
        )
        .await
        {
            if let Ok(_) = write_value(
                get_app_data(req.clone()),
                format!("/client_list/{client_uuid}/report_data/jc/tracker_partial_list").as_str(),
                Vec::<reports::jc::tracker::ReportBuilder>::new(),
            )
            .await
            {
                successes.push(true);
            }
        }
        let mut tracker_partial_list: Vec<reports::jc::tracker::ReportBuilder> = get_value!(
            req.clone(),
            "/client_list/{client_uuid}/report_data/jc/tracker_partial_list"
        )
        .unwrap();
        let local_tz: Tz = get_value!(req.clone(), "/employee_list/{username}/timezone").unwrap();
        let local_now = Utc::now().with_timezone(&local_tz);
        if tracker_partial_list.len() == 0 {
            tracker_partial_list.push(reports::jc::tracker::ReportBuilder::new(local_tz));
        }
        //let day = Utc::now().with_timezone(&local_tz).day().to_string();
        if let Ok(_) = tracker_partial_list.last_mut().unwrap().add_row(
            form.days_month,
            username.clone(),
            format!("{}:{}", form.hours, form.minutes),
            primary,
            secondary,
        ) {
            successes.push(true);
        }
        if successes.len() >= 1 {
            if let Ok(_) = write_value(
                get_app_data(req.clone()),
                format!("/client_list/{client_uuid}/report_data/jc/tracker_partial_list").as_str(),
                tracker_partial_list.clone(),
            )
            .await
            {
                successes.push(true);
            }
        }
        if tracker_partial_list.len() == 5 {
            email::send_email(&get_smtp_transport(), vec![/*"Hayden Johnson <haydenj@alliessupportedemployment.com>"*/"Westly Ward <sonicrules1234@gmail.com>".parse().unwrap()], None, "Reminder: Job Placement Form 92 Due", format!("Hello,\nThis is a reminder that you need to submit the form 92 for {} {} today\nThank you", current_client.first_name, current_client.last_name));
        }
        if successes.len() >= 2 {
            write_value(
                get_app_data(req.clone()),
                format!("/employee_list/{username}/need_form_95"),
                false,
            )
            .await
            .unwrap();
        }
        //let local_tz: Tz = get_value!(req.clone(), "/employee_list/{username}/timezone").unwrap();
        //let today = Utc::now().with_timezone(&local_tz);
        utils::respond_html(
            JCTrackerReportPageTemplate {
                navigation_bar: NavBarTemplate::new(req.clone(), username),
                success: Some(successes.len() >= 2),
                days: (1..calculate_num_days_month(chrono::NaiveDate::from_ymd(
                    local_now.year(),
                    local_now.month(),
                    local_now.day(),
                )) + 1)
                    .collect(),
            }
            .render()
            .unwrap(),
        )
    } else {
        error_page("JC Tracking Systems", "You are not clocked in for a client").await
    }
}

#[post("/reports/jc/tracker_report.rs")]
async fn jc_tracker_report_post(req: HttpRequest, payload: web::Payload) -> HttpResponse {
    check_cookies_and_username_req_payload(
        req.clone(),
        payload,
        jc_tracker_report_post_internal,
        "none",
    )
    .await
}

async fn clock_internal(
    username: String,
    req: HttpRequest,
    form: web::Form<ClockPost>,
) -> HttpResponse {
    let clock_type = form.clone().clock_type;
    let clocking_in: bool;
    if form.clock_in == "true" {
        clocking_in = true;
    } else {
        clocking_in = false;
    }
    let employee_record: EmployeeRecord = read_value(
        req.clone()
            .app_data::<web::Data<Sender<SyncMessage>>>()
            .unwrap()
            .clone(),
        format!("/employee_list/{username}/record").as_str(),
    )
    .unwrap();
    if let EmployeeRecord::Registered(this_employee) = employee_record {
        let mut employee_clone = this_employee.clone();
        if clocking_in {
            if let Some(clock_typ) = clock_type {
                let client_uuid: Option<Uuid>;
                if form.client_uuid.as_ref().unwrap().to_lowercase() == "none" {
                    client_uuid = None;
                } else {
                    client_uuid =
                        Some(Uuid::parse_str(form.client_uuid.as_ref().unwrap()).unwrap());
                }
                if let Some(c_uuid) = client_uuid {
                    let this_client: client::Client =
                        get_value!(req.clone(), "/client_list/{c_uuid}/record").unwrap();
                    let this_caseload_uuid: String =
                        get_value!(req.clone(), "/employee_list/{username}/caseload").unwrap();
                    let this_caseload: caseload::CaseLoad =
                        get_value!(req.clone(), "/caseload_list/{this_caseload_uuid}/record")
                            .unwrap();
                    //let client_office = this_client.office.clone();
                    if !(this_caseload.employees.contains(&username.clone())
                        && this_caseload
                            .client_uuids
                            .contains(&this_client.uuid.to_string()))
                    {
                        return error_page(
                            "Time Management System",
                            "This client is not in your current caseload",
                        )
                        .await;
                    } else {
                        if this_client.service_provided == client::ClientService::JobDevelopment {
                            let mut row: reports::billing_tracking_vr_jd::BillingTrackingVRJDRow =
                                get_value!(
                                    req.clone(),
                                    "/client_list/{c_uuid}/report_data/billing_tracking_vr_jd"
                                )
                                .unwrap();
                            if !row.cur_job_seek {
                                let now = Utc::now();
                                row.start_tracking_datetime =
                                    Some(Utc.ymd(now.year(), now.month(), now.day()).and_hms(
                                        now.hour(),
                                        now.minute(),
                                        0,
                                    ));
                                row.cur_job_seek = true;
                                row.employee_username = Some(username.clone());
                                write_value(
                                    get_app_data(req.clone()),
                                    format!(
                                        "/client_list/{c_uuid}/report_data/billing_tracking_vr_jd"
                                    )
                                    .as_str(),
                                    row,
                                )
                                .await
                                .unwrap();
                            }
                        }
                    }
                }
                if clock_typ.clone().to_lowercase() == "break" && client_uuid.is_some() {
                    return error_page(
                        "Time Management System",
                        "Client must be set to None for clocking in breaks",
                    )
                    .await;
                }
                match employee_clone.clock_in(clock_typ, client_uuid).await {
                    Ok(_) => {
                        let record = EmployeeRecord::Registered(employee_clone);
                        batch_write_value!(
                            get_app_data(req.clone()),
                            ("/employee_list/{username}/record", record),
                            ("/employee_list/{username}/need_form_95", true)
                        )
                        .unwrap();
                        /*
                        write_value(
                            req.clone()
                                .app_data::<web::Data<Sender<SyncMessage>>>()
                                .unwrap()
                                .clone(),
                            format!("/employee_list/{username}/record").as_str(),
                            record,
                        )
                        .await
                        .unwrap();
                        */
                        redirect("/")
                    }
                    Err(ClockError::WrongDirection) => {
                        error_page("Employee Clocking", "Wrong direction").await
                    }
                    Err(ClockError::WrongType) => {
                        error_page("Employee Clocking", "Invalid clock type").await
                    }
                }
            } else {
                error_page("Employee Clocking", "No clock in type specified").await
            }
        } else {
            if form.client_got_job == Some(String::from("yes")) {
                let this_client = get_current_client(req.clone(), username.clone())
                    .await
                    .unwrap();
                if this_client.billing_office == reports::BillingOffice::StGeorge {
                    email::send_email(
                        &get_smtp_transport(),
                        //vec!["Keenan R <keenanr@alliessupportedemployement.com>".parse().unwrap()],
                        //vec!["Hayden Johnson <haydenj@alliessupportedemployement.com>".parse().unwrap()],
                        vec!["Westly Ward <sonicrules1234@gmail.com>".parse().unwrap()],
                        None,
                        "A client jot a job in your area",
                        "A client got a job in your area.  Please fill out CIE form 60 at PUT_URL_HERE"
                    );
                    return redirect("/reports/jd/cie_job_placement");
                }
            }
            if let Some(this_client) =
                get_current_client(req.clone(), this_employee.username.clone()).await
            {
                if this_client.service_provided == client::ClientService::JobCoaching {
                    if key_exists!(req.clone(), "/employee_list/{username}/need_form_95").await {
                        let needs_form_95: bool =
                            get_value!(req.clone(), "/employee_list/{username}/need_form_95")
                                .unwrap();
                        if needs_form_95 {
                            return redirect("/reports/jc/tracker");
                        }
                    }
                }
            }
            match employee_clone.clock_out().await {
                Ok(_) => {
                    let record = EmployeeRecord::Registered(employee_clone);
                    write_value(
                        req.clone()
                            .app_data::<web::Data<Sender<SyncMessage>>>()
                            .unwrap()
                            .clone(),
                        format!("/employee_list/{username}/record").as_str(),
                        record,
                    )
                    .await
                    .unwrap();
                    redirect("/")
                }
                Err(ClockError::WrongDirection) => {
                    error_page("Employee Clocking", "Wrong direction").await
                }
                Err(ClockError::WrongType) => {
                    error_page("Employee Clocking", "Invalid clock type").await
                }
            }
        }
    } else {
        error_page("Employee Clocking", "Employee not registered").await
    }
}

#[post("/clock.rs")]
async fn clock_post(req: HttpRequest, form: web::Form<ClockPost>) -> HttpResponse {
    check_cookies_and_username_req_form(req, form, clock_internal, "none").await
}

async fn check_cookies(req: HttpRequest) -> Result<String, SessionError> {
    if let Ok(cookies) = req.cookies() {
        let mut username: Option<String> = None;
        let mut session_id: Option<String> = None;
        for cookie in cookies.iter() {
            if cookie.name() == "username" {
                username = Some(cookie.value().to_string());
            } else if cookie.name() == "session_id" {
                session_id = Some(cookie.value().to_string());
            }
        }
        if username.is_some() && session_id.is_some() {
            if let Ok(db_session_id) = read_value::<String>(
                req.clone()
                    .app_data::<web::Data<Sender<SyncMessage>>>()
                    .unwrap()
                    .clone(),
                format!("/employee_list/{}/session", username.as_ref().unwrap()).as_str(),
            ) {
                if db_session_id == session_id.unwrap() {
                    return Ok(username.unwrap());
                } else {
                    return Err(SessionError::SessionIDMismatch);
                }
            } else {
                return Err(SessionError::NotLoggedIn);
            }
        }
    }
    Err(SessionError::NoCookies)
}

#[post("/setup.rs")]
async fn setup_post(req: HttpRequest, form: web::Form<SetupPost>) -> HttpResponse {
    let is_setup: bool = read_value(
        req.clone()
            .app_data::<web::Data<Sender<SyncMessage>>>()
            .unwrap()
            .clone(),
        "/setup_done",
    )
    .unwrap();
    let username = form.clone().user;
    let valid_clock_types: Vec<String> = read_value(
        req.app_data::<web::Data<Sender<SyncMessage>>>()
            .unwrap()
            .clone(),
        "/clock_types",
    )
    .unwrap();
    if !is_setup {
        if form.passwd == form.cpasswd {
            let employee = Employee::new(
                form.user.clone(),
                form.email.clone(),
                form.fname.clone(),
                form.lname.clone(),
                form.passwd.clone(),
                form.dept.clone(),
                AccessLevel::All,
                form.jobd.clone(),
                valid_clock_types,
                OfficeLocation::from(form.office.clone()),
            )
            .await;
            //let c = req.cookies().unwrap();
            match add_employee(req.clone(), employee, false).await {
                Ok(_) => {
                    let session_id = uuid::Uuid::new_v4().to_string();
                    write_value(
                        req.clone()
                            .app_data::<web::Data<Sender<SyncMessage>>>()
                            .unwrap()
                            .clone(),
                        format!("/employee_list/{username}/session").as_str(),
                        session_id.clone(),
                    )
                    .await
                    .unwrap();
                    HttpResponse::SeeOther()
                        .insert_header(("Location", "/choose_caseload"))
                        .cookie(Cookie::new("username", username))
                        .cookie(Cookie::new("session_id", session_id))
                        .finish()
                }
                Err(EmployeeError::UsernameAlreadyExists) => {
                    error_page("Setup", "Username already taken").await
                }
                Err(EmployeeError::InvalidUsername) => {
                    error_page("Setup", "Username is invalid").await
                }
            }
        } else {
            error_page("Setup", "Passwords do not match").await
        }
    } else {
        error_page("Setup", "Setup has already been completed.").await
    }
}

#[derive(Template, Debug, Clone, Serialize, Deserialize)]
#[template(path = "register.html")]
struct RegisterPageTemplate {
    username: String,
    key: Uuid,
    code: Uuid,
}
/*
#[derive(Debug, Clone, Serialize, Deserialize)]
struct RegisterGet {
}
*/
#[get("/register/{username}/{key}/{code}")]
async fn register_page(req: HttpRequest, path: web::Path<RegisterPageTemplate>) -> HttpResponse {
    let username = path.username.clone();
    let key = path.key;
    let code = path.code;
    //let (username, key, code) = path.into_inner();
    if let EmployeeRecord::NotRegistered(employee) =
        get_value!(req.clone(), "/employee_list/{username}/record").unwrap()
    {
        if key == employee.uuid_key && code == employee.uuid_code {
            return tms::utils::respond_html(
                RegisterPageTemplate {
                    username: username,
                    key: Uuid::parse_str(&key.to_string()).unwrap(),
                    code: Uuid::parse_str(&code.to_string()).unwrap(),
                }
                .render()
                .unwrap(),
            );
        }
    }
    HttpResponse::NotFound().finish()
}

#[derive(Debug, Clone, Serialize, Deserialize)]
struct RegisterPost {
    passwd: String,
    cpasswd: String,
    username: String,
    key: Uuid,
    code: Uuid,
}

#[post("/register.rs")]
async fn register_post(req: HttpRequest, form: web::Form<RegisterPost>) -> HttpResponse {
    let username = form.username.clone();
    let passwd = form.passwd.clone();
    let cpasswd = form.cpasswd.clone();
    let key = form.key;
    let code = form.code;
    if passwd != cpasswd {
        return error_page("Registation Systems", "Passwords do not match").await;
    }
    if let Ok(EmployeeRecord::NotRegistered(record)) =
        get_value!(req.clone(), "/employee_list/{username}/record")
    {
        if key == record.uuid_key && code == record.uuid_code {
            remove_value(
                get_app_data(req.clone()),
                format!("/employee_list/{username}/record").as_str(),
            )
            .await
            .unwrap();
            add_employee(req.clone(), record.build(passwd).await, true)
                .await
                .unwrap();
            return redirect("/");
        } else {
            return error_page("Registration Systems", "Key does not match code").await;
        }
    }
    error_page("Registration Systems", "Error registering").await
}

#[derive(Template, Debug, Clone, Serialize, Deserialize)]
#[template(path = "generic_error.html")]
struct GenericErrorPageTemplate {
    error_type: String,
    error_details: String,
}
impl GenericErrorPageTemplate {
    pub fn new(error_type: impl Into<String>, error_details: impl Into<String>) -> Self {
        Self {
            error_type: error_type.into(),
            error_details: error_details.into(),
        }
    }
}

async fn error_page(general: impl Into<String>, details: impl Into<String>) -> HttpResponse {
    let template = GenericErrorPageTemplate::new(general, details);
    tms::utils::respond_html(template.render().unwrap())
}
/*
async fn create_channel_info() {
    let channel_info = ChannelInfo::new().await;
    /*
    let mut mutex_result = GLOBAL_DATA.try_lock();
    while mutex_result.is_err() {
        mutex_result = GLOBAL_DATA.try_lock();

    }
    */
    let mut global_data = mutex_result.unwrap();
    global_data.insert(channel_info.uuid.clone(), channel_info);
}
*/

#[derive(Debug, Clone, Serialize, Deserialize)]
struct ManageCaseloadsPost {
    action_type: String,
    employee_usernames: Option<Vec<String>>,
    client_uuids: Option<Vec<String>>,
    caseload_name: Option<String>,
    caseload: Option<String>,
}

#[post("/manage_caseloads.rs")]
async fn manage_caseloads_post(req: HttpRequest, payload: web::Payload) -> HttpResponse {
    check_cookies_and_username_req_payload(
        req.clone(),
        payload,
        manage_caseloads_post_internal,
        "manage_caseloads",
    )
    .await
}

async fn manage_caseloads_post_internal(
    username: String,
    req: HttpRequest,
    mut payload: web::Payload,
) -> HttpResponse {
    // payload is a stream of Bytes objects
    let mut body = web::BytesMut::new();
    while let Some(chunk) = payload.next().await {
        let chunk = chunk.unwrap();
        // limit max size of in-memory payload
        if (body.len() + chunk.len()) > 262_144 {
            return error_page("Caseload Management Systems", "Overflow").await;
        }
        body.extend_from_slice(&chunk);
    }
    let config = serde_qs::Config::new(2, false);
    // body is loaded, now we can deserialize serde-json
    let form: ManageCaseloadsPost = config.deserialize_bytes(&body).unwrap();
    let mut client_uuid_list: Vec<String> = get_value!(req.clone(), "/client_list").unwrap();
    client_uuid_list.retain(|x| x != "none");
    let employee_username_list: Vec<String> = get_value!(req.clone(), "/employee_list").unwrap();
    let mut caseload_uuid_list: Vec<String> = get_value!(req.clone(), "/caseload_list").unwrap();
    if form.action_type == "create" {
        if form.employee_usernames.is_none()
            || form.client_uuids.is_none()
            || form.caseload_name.is_none()
        {
            return error_page(
                "Caseload Management Systems",
                "At least one employee and one client must be selected along with a caseload name",
            )
            .await;
        }
        let mut valid = true;
        let username_list: Vec<String> = get_value!(req.clone(), "/employee_list").unwrap();
        for user in form.employee_usernames.clone().unwrap() {
            if !username_list.contains(&user) {
                valid = false;
                break;
            }
        }
        if !valid {
            return error_page(
                "Caseload Management Systems",
                "One or more invalid employees were selected",
            )
            .await;
        }
        //let client_uuid_list: Vec<String> = get_value!(req.clone(), "/client_list").unwrap();
        for client_uuid in form.client_uuids.clone().unwrap() {
            if !client_uuid_list.contains(&client_uuid) {
                valid = false;
                break;
            }
        }
        if !valid {
            return error_page(
                "Caseload Management Systems",
                "One or more invalid clients were selected",
            )
            .await;
        }
        let this_caseload_uuid = Uuid::new_v4();
        let new_caseload = caseload::CaseLoad {
            employees: form.employee_usernames.clone().unwrap(),
            client_uuids: form.client_uuids.clone().unwrap(),
            name: form.caseload_name.clone().unwrap(),
            uuid: this_caseload_uuid.clone(),
        };
        //let mut caseload_uuid_list: Vec<String> =
        //    get_value!(req.clone(), "/caseload_list").unwrap();
        caseload_uuid_list.push(this_caseload_uuid.clone().to_string());
        batch_write_value!(
            get_app_data(req.clone()),
            ("/caseload_list", caseload_uuid_list),
            ("/caseload_list/{this_caseload_uuid}/record", new_caseload)
        )
        .unwrap();
        // TODO; Batch the following somehow
        for employee_username in form.employee_usernames.clone().unwrap() {
            let mut caseloads: Vec<String> = get_value!(
                req.clone(),
                "/employee_list/{employee_username}/caseload_list"
            )
            .unwrap();
            caseloads.push(this_caseload_uuid.to_string());
            write_value(
                get_app_data(req.clone()),
                format!("/employee_list/{employee_username}/caseload_list").as_str(),
                caseloads,
            )
            .await
            .unwrap();
        }
        for client_uuid in form.client_uuids.clone().unwrap() {
            let mut caseloads: Vec<String> =
                get_value!(req.clone(), "/client_list/{client_uuid}/caseload_list").unwrap();
            caseloads.push(this_caseload_uuid.to_string());
            write_value(
                get_app_data(req.clone()),
                format!("/client_list/{client_uuid}/caseload_list").as_str(),
                caseloads,
            )
            .await
            .unwrap();
        }
        redirect("/")
    } else if form.action_type == "edit" {
        if form.caseload.is_none() {
            return error_page("Caseload Managment Systems", "No caseload uuid provided").await;
        }
        let caseload = form.caseload.clone().unwrap();
        let this_caseload: caseload::CaseLoad =
            get_value!(req.clone(), "/caseload_list/{caseload}/record").unwrap();
        utils::respond_html(
            ManageCaseLoadPageTemplate {
                navigation_bar: NavBarTemplate::new(req.clone(), username.clone()),
                employees: employee_username_list
                    .clone()
                    .into_iter()
                    .filter_map(|x| get_employee_record(req.clone(), x).unwrap())
                    .collect(),
                clients: client_uuid_list
                    .clone()
                    .into_iter()
                    .map(|x| {
                        //println!("{x}");
                        get_value!(req.clone(), "/client_list/{x}/record").unwrap()
                    })
                    .collect(),
                clients_checked: client_uuid_list
                    .clone()
                    .into_iter()
                    .map(|x| {
                        //println!("{x}");
                        let checked_string: String;
                        if this_caseload.clone().client_uuids.contains(&x) {
                            checked_string = String::from(" checked");
                        } else {
                            checked_string = String::new();
                        }
                        (
                            get_value!(req.clone(), "/client_list/{x}/record").unwrap(),
                            checked_string,
                        )
                    })
                    .collect(),
                employees_checked: employee_username_list
                    .clone()
                    .into_iter()
                    .filter_map(|x| {
                        //println!("{x}");
                        let checked_string: String;
                        if this_caseload.clone().employees.contains(&x) {
                            checked_string = String::from(" checked");
                        } else {
                            checked_string = String::new();
                        }
                        if let Some(employee_rec) = get_employee_record(req.clone(), x).unwrap() {
                            Some((employee_rec, checked_string))
                        } else {
                            None
                        }
                    })
                    .collect(),
                editing_caseload_name: this_caseload.name.clone(),
                editing: true,
                caseloads: Vec::new(),
                caseload_uuid: this_caseload.uuid.to_string(),
            }
            .render()
            .unwrap(),
        )
    } else if form.action_type == "save" {
        if form.employee_usernames.is_none()
            || form.client_uuids.is_none()
            || form.caseload.is_none()
        {
            return error_page(
                "Caseload Management Systems",
                "At least one employee and one client must be selected along with a caseload uuid.",
            )
            .await;
        }
        let caseload_uuid = form.caseload.clone().unwrap();
        let current_caseload: caseload::CaseLoad =
            get_value!(req.clone(), "/caseload_list/{caseload_uuid}/record").unwrap();
        let caseload_to_save = caseload::CaseLoad {
            uuid: Uuid::parse_str(form.caseload.clone().unwrap().as_str()).unwrap(),
            name: current_caseload.name.clone(),
            employees: form.employee_usernames.clone().unwrap(),
            client_uuids: form.client_uuids.clone().unwrap(),
        };
        // TODO; Batch the following somehow
        batch_write_value!(
            get_app_data(req.clone()),
            ("/caseload_list/{caseload_uuid}/record", caseload_to_save)
        )
        .unwrap();
        for employee_username in current_caseload.employees {
            let mut caseloads: Vec<String> = get_value!(
                req.clone(),
                "/employee_list/{employee_username}/caseload_list"
            )
            .unwrap();
            caseloads.retain(|x| x != &current_caseload.uuid.to_string());
            write_value(
                get_app_data(req.clone()),
                format!("/employee_list/{employee_username}/caseload_list").as_str(),
                caseloads,
            )
            .await
            .unwrap();
        }
        for client_uuid in current_caseload.client_uuids.clone() {
            let mut caseloads: Vec<String> =
                get_value!(req.clone(), "/client_list/{client_uuid}/caseload_list").unwrap();
            caseloads.retain(|x| x != &current_caseload.uuid.to_string());
            write_value(
                get_app_data(req.clone()),
                format!("/client_list/{client_uuid}/caseload_list").as_str(),
                caseloads,
            )
            .await
            .unwrap();
        }
        // TODO; Batch the following somehow
        for employee_username in form.employee_usernames.clone().unwrap() {
            let mut caseloads: Vec<String> = get_value!(
                req.clone(),
                "/employee_list/{employee_username}/caseload_list"
            )
            .unwrap();
            caseloads.push(current_caseload.uuid.to_string());
            write_value(
                get_app_data(req.clone()),
                format!("/employee_list/{employee_username}/caseload_list").as_str(),
                caseloads,
            )
            .await
            .unwrap();
        }
        for client_uuid in form.client_uuids.clone().unwrap() {
            let mut caseloads: Vec<String> =
                get_value!(req.clone(), "/client_list/{client_uuid}/caseload_list").unwrap();
            caseloads.push(current_caseload.uuid.to_string());
            write_value(
                get_app_data(req.clone()),
                format!("/client_list/{client_uuid}/caseload_list").as_str(),
                caseloads,
            )
            .await
            .unwrap();
        }
        redirect("/")
    } else {
        error_page("Caseload Management Systems", "Invalid action_type").await
    }
}

#[derive(Debug, Clone, Serialize, Deserialize)]
struct AddEmployeePost {
    user: String,
    fname: String,
    lname: String,
    dept: String,
    jobd: String,
    office: String,
    allowed: Option<Vec<String>>,
    email: String,
    /*
    add_employee: Option<String>,
    add_authorization: Option<String>,
    add_vr_counselor_support_coordinator: Option<String>,
    report: Option<String>,
    */
}

fn get_smtp_transport() -> SmtpTransport {
    let refresh_token = std::fs::read_to_string("refresh_token.txt").unwrap();
    let oauth_inst = GoogleOAuth2::new(
        std::fs::read_to_string("client_id.txt").unwrap(),
        std::fs::read_to_string("client_secret.txt").unwrap(),
    );
    let token = oauth_inst
        .access_from_refresh_string(refresh_token)
        .trim()
        .to_string();
    SmtpTransport::starttls_relay("smtp.gmail.com")
        .unwrap()
        //.auth(mail_send::smtp::auth::XOAuth)
        .credentials(Credentials::new(
            "noreply@alliessupportedemployment.com".to_string(),
            token,
        ))
        .authentication(vec![Mechanism::Xoauth2])
        .build()
}

async fn add_employee_post_internal(
    username: String,
    req: HttpRequest,
    mut payload: web::Payload,
) -> HttpResponse {
    // payload is a stream of Bytes objects
    let mut body = web::BytesMut::new();
    while let Some(chunk) = payload.next().await {
        let chunk = chunk.unwrap();
        // limit max size of in-memory payload
        if (body.len() + chunk.len()) > 262_144 {
            return error_page("Employee Management Systems", "Overflow").await;
        }
        body.extend_from_slice(&chunk);
    }
    let refresh_token = std::fs::read_to_string("refresh_token.txt").unwrap();
    let oauth_inst = GoogleOAuth2::new(
        std::fs::read_to_string("client_id.txt").unwrap(),
        std::fs::read_to_string("client_secret.txt").unwrap(),
    );
    let token = oauth_inst
        .access_from_refresh_string(refresh_token)
        .trim()
        .to_string();
    let config = serde_qs::Config::new(1, false);
    // body is loaded, now we can deserialize serde-json
    let form: AddEmployeePost = config.deserialize_bytes(&body).unwrap();
    let employee_user = form.user.clone();
    let mut employee_list: Vec<String> = get_value!(req.clone(), "/employee_list").unwrap();
    employee_list.push(form.user.clone());
    let valid_clock_types: Vec<String> = get_value!(req.clone(), "/clock_types").unwrap();
    let mut access_levels: Vec<String> = Vec::new();
    if let Some(allowed) = form.allowed.clone() {
        if allowed.contains(&String::from("manage_clients")) {
            access_levels.push(String::from("add_authorization"));
        }
        if allowed.contains(&String::from("manage_employees")) {
            access_levels.push(String::from("add_employee"));
        }
        if allowed.contains(&String::from("manage_vr_counselors_support_coordinators")) {
            access_levels.push(String::from("add_vr_counselor_support_coordinator"));
        }
        if allowed.contains(&String::from("reports")) {
            access_levels.push(String::from("report"));
        }
        if allowed.contains(&String::from("add_billing_office")) {
            access_levels.push(String::from("add_billing_office"));
        }
        if allowed.contains(&String::from("add_service_area")) {
            access_levels.push(String::from("add_service_area"));
        }
        if allowed.contains(&String::from("manage_caseloads")) {
            access_levels.push(String::from("manage_caseloads"));
        }
    }
    let record = EmployeeBuilder::new(
        form.user.clone(),
        form.email.clone(),
        form.fname.clone(),
        form.lname.clone(),
        valid_clock_types,
        form.dept.clone(),
        AccessLevel::Custom(access_levels),
        form.jobd.clone(),
        OfficeLocation::from(form.office.clone()),
    )
    .await;
    let success = match batch_write_value!(
        get_app_data(req.clone()),
        ("/employee_list", employee_list),
        (
            "/employee_list/{employee_user}/record",
            EmployeeRecord::NotRegistered(record.clone())
        )
    ) {
        Ok(_) => Some(true),
        Err(_) => Some(false),
    };
    let (email_user, email_host) = form.email.split_once("@").unwrap();
    let address = Address::new(email_user, email_host).unwrap();
    let mailbox = Mailbox::new(Some(format!("{} {}", form.fname, form.lname)), address);
    let message = lettre::message::MessageBuilder::new()
        .from(
            "Allies Supported Employment <noreply@alliessupportedemployment.com>"
                .parse()
                .unwrap(),
        )
        .to(mailbox)
        .subject("TMS Setup")
        .body(String::from(format!(
            "You've been chosen for a job at Allies Supported Employment:\n\nTo set up your account on our Time Management System, please click the following link: https://sonicrules.fun:8080/register/{}/{}/{}\n",
            &record.username, &record.uuid_key, &record.uuid_code
        )))
        .unwrap();
    SmtpTransport::starttls_relay("smtp.gmail.com")
        .unwrap()
        //.auth(mail_send::smtp::auth::XOAuth)
        .credentials(Credentials::new(
            "noreply@alliessupportedemployment.com".to_string(),
            token,
        ))
        .authentication(vec![Mechanism::Xoauth2])
        .build()
        .send(&message)
        .unwrap();
    println!("{:#?}", record.clone());
    println!(
        "/register/{}/{}/{}",
        &record.username, &record.uuid_key, &record.uuid_code
    );
    utils::respond_html(
        AddEmployeePageTemplate {
            success: success,
            navigation_bar: NavBarTemplate::new(req.clone(), username),
            offices: get_value!(req.clone(), "/office_location_list").unwrap(),
            checkboxes: vec![
                (
                    String::from("report"),
                    String::from("Reports"),
                    String::from("reports"),
                ),
                make_string_tuple("add_employee", "Manage Employees", "manage_employees"),
                /*
                make_string_tuple(
                    "add_vr_counselor_support_coordinator",
                    "Manage VR Counselors / Support Coordinators",
                    "manage_vr_couneslors_support_coordinators",
                ),
                */
                make_string_tuple("add_authorization", "Manage Clients", "manage_clients"),
                make_string_tuple(
                    "add_billing_office",
                    "Add Billing Offices",
                    "add_billing_office",
                ),
                make_string_tuple("add_service_area", "Add Service Areas", "add_service_area"),
            ],
        }
        .render()
        .unwrap(),
    )
}

async fn notif_reschedule(
    notification_type: scheduled_notifications::NotificationType,
    reschedule_in: std::time::Duration,
    tx: Sender<SyncMessage>,
) {
    match notification_type {
        scheduled_notifications::NotificationType::WSA14_7(client_uuid, due_date) => {
            let this_client: client::Client =
                get_value_tx!(tx.clone(), "/client_list/{client_uuid}/record").unwrap();
            if Utc::now() < due_date {
                append_to_schedule(tx.clone(), scheduled_notifications::ScheduledNotification::new(chrono::Duration::weeks(1), Some(reschedule_in), scheduled_notifications::NotificationType::WSA14_7(client_uuid, due_date), Vec::new(), None, "Reminder: Work Strategy Assesment due", format!("Hello,\nthis is a reminder that you have a Work Strategy Assessment for {} {} due on {}.\nThanks", this_client.first_name, this_client.last_name, due_date))).await.unwrap();
            }
        }
        scheduled_notifications::NotificationType::Discovery30(client_uuid, due_date) => {
            let this_client: client::Client =
                get_value_tx!(tx.clone(), "/client_list/{client_uuid}/record").unwrap();
            if Utc::now() < due_date {
                append_to_schedule(tx.clone(), scheduled_notifications::ScheduledNotification::new(chrono::Duration::days(30), Some(reschedule_in), scheduled_notifications::NotificationType::Discovery30(client_uuid, due_date), Vec::new(), None, "Reminder: DSR Due", format!("Hello,\nthis is a reminder that you have a Discovery Assessment for {} {} due on {}.\nThanks", this_client.first_name, this_client.last_name, due_date))).await.unwrap();
            }
        }
    }
}

async fn scheduler(tx: Sender<SyncMessage>) {
    let mut interval = tokio::time::interval(tokio::time::Duration::from_secs(60));
    interval.tick().await;
    let tx_clone = tx.clone();
    let refresh_token = std::fs::read_to_string("refresh_token.txt").unwrap();
    let oauth_inst = GoogleOAuth2::new(
        std::fs::read_to_string("client_id.txt").unwrap(),
        std::fs::read_to_string("client_secret.txt").unwrap(),
    );
    let token = oauth_inst
        .access_from_refresh_string(refresh_token)
        .trim()
        .to_string();
    loop {
        let now_minutes = get_now_minutes();
        let mut schedule: Vec<scheduled_notifications::ScheduledNotification> =
            get_value_tx!(tx_clone.clone(), "/schedule").unwrap();
        //schedule = do_checks(tx_clone.clone(), schedule).await;
        let relevent_notifs: Vec<scheduled_notifications::ScheduledNotification> = schedule
            .clone()
            .into_iter()
            .filter(|x| &x.scheduled_for <= &now_minutes)
            .collect();
        if relevent_notifs.len() > 0 {
            let transport = SmtpTransport::starttls_relay("smtp.gmail.com")
                .unwrap()
                //.auth(mail_send::smtp::auth::XOAuth)
                .credentials(Credentials::new(
                    "noreply@alliessupportedemployment.com".to_string(),
                    token.clone(),
                ))
                .authentication(vec![Mechanism::Xoauth2])
                .build();
            for n in relevent_notifs.clone() {
                let n_clone = match n.msg_type {
                    scheduled_notifications::NotificationType::WSA14_7(client_uuid, _) => {
                        let mut n_c = n.clone();
                        let mut mail_box_list: Vec<Mailbox> = Vec::new();
                        let caseload_uuid_list: Vec<String> =
                            get_value_tx!(tx.clone(), "/client_list/{client_uuid}/caseload_list")
                                .unwrap();
                        for caseload_uuid in caseload_uuid_list {
                            let this_caseload: caseload::CaseLoad =
                                get_value_tx!(tx.clone(), "/caseload_list/{caseload_uuid}/record")
                                    .unwrap();
                            for username in this_caseload.employees {
                                let employee_record: employee::EmployeeRecord =
                                    get_value_tx!(tx.clone(), "/employee_list/{username}/record")
                                        .unwrap();
                                if let employee::EmployeeRecord::Registered(this_employee) =
                                    employee_record
                                {
                                    if !mail_box_list.contains(&this_employee.mail_box) {
                                        mail_box_list.push(this_employee.mail_box);
                                    }
                                }
                            }
                        }
                        n_c.mail_boxes = mail_box_list;
                        if n_c.mail_boxes.len() <= 1 {
                            n_c.use_cc = None;
                        } else {
                            n_c.use_cc = Some(true);
                        }
                        n_c
                    }
                    _ => n.clone(),
                };
                email::send_email(
                    &transport,
                    //n_clone.name,
                    //n_clone.email,
                    n.mail_boxes.clone(),
                    n.use_cc,
                    n_clone.subject,
                    n_clone.message,
                );
                remove_from_schedule(tx.clone(), n.clone()).await.unwrap();
                if let Some(resched_duration) = n.reschedule_in {
                    notif_reschedule(n.msg_type, resched_duration, tx.clone()).await;
                }
            }
        }
        //for scheduled_notification in schedule {}
        interval.tick().await;
    }
}
/*
async fn do_checks(
    tx: Sender<SyncMessage>,
    schedule: Vec<scheduled_notifications::ScheduledNotification>,
) -> Vec<scheduled_notifications::ScheduledNotification> {
    let mut sched = schedule.clone();
    let minutes_now = get_now_minutes();
    let mut client_uuid_list: Vec<String> = get_value_tx!(tx.clone(), "/client_list").unwrap();
    client_uuid_list.retain(|x| x.to_lowercase() != "none");
    for client_uuid in client_uuid_list {
        let this_client: client::Client =
            get_value_tx!(tx.clone(), "/client_list/{client_uuid}/record").unwrap();
        if vec![
            client::ClientService::WsaTier1,
            client::ClientService::WsaTier2,
        ]
        .contains(&this_client.service_provided)
        {
            if this_client
                .start_date
                .checked_add_signed(chrono::Duration::weeks(3))
                .unwrap()
                > get_now_minutes()
                && !sched.iter().by_ref().any(|z| match z.msg_type {
                    scheduled_notifications::NotificationType::WSA14_7(c_uuid, _) => {
                        c_uuid.to_string() == client_uuid
                    }
                    _ => false,
                })
            {
                let due_date = minutes_now
                    .checked_add_signed(chrono::Duration::weeks(3))
                    .unwrap();
                sched.push(scheduled_notifications::ScheduledNotification::new(
                    chrono::Duration::seconds(0),
                    Some(chrono::Duration::weeks(1).to_std().unwrap()),
                    scheduled_notifications::NotificationType::WSA14_7(
                        Uuid::parse_str(&client_uuid).unwrap(),
                        due_date.clone()
                    ), vec!["Hayden Johnson <haydenj@alliessupportedemployment.com>".parse().unwrap()], None, "Reminder: Work Strategy Assesment due", format!("Hello, this is a reminder that you have a Work Strategy Assessment for {} {} due on {}", this_client.first_name, this_client.last_name, due_date))
                );
            }
        } else if this_client.service_provided == client::ClientService::Discovery {
        }
    }
    sched
}
*/
#[post("/add_employee.rs")]
async fn add_employee_post(req: HttpRequest, payload: web::Payload) -> HttpResponse {
    check_cookies_and_username_req_payload(
        req.clone(),
        payload,
        add_employee_post_internal,
        "add_employee",
    )
    .await
}

#[derive(Template)]
#[template(path = "add_employee.html")]
struct AddEmployeePageTemplate {
    success: Option<bool>,
    navigation_bar: NavBarTemplate,
    offices: Vec<String>,
    checkboxes: Vec<(String, String, String)>,
}

fn make_string_tuple(
    name: impl Into<String>,
    label: impl Into<String>,
    val: impl Into<String>,
) -> (String, String, String) {
    (name.into(), label.into(), val.into())
}

async fn add_employee_page_internal(req: HttpRequest, username: String) -> HttpResponse {
    utils::respond_html(
        AddEmployeePageTemplate {
            success: None,
            navigation_bar: NavBarTemplate::new(req.clone(), username),
            offices: get_value!(req.clone(), "/office_location_list").unwrap(),

            checkboxes: vec![
                (
                    String::from("report"),
                    String::from("Reports"),
                    String::from("reports"),
                ),
                make_string_tuple("add_employee", "Manage Employees", "manage_employees"),
                /*
                make_string_tuple(
                    "add_vr_counselor_support_coordinator",
                    "Manage VR Counselors / Support Coordinators",
                    "manage_vr_couneslors_support_coordinators",
                ),
                */
                make_string_tuple("add_authorization", "Manage Clients", "manage_clients"),
                make_string_tuple(
                    "add_billing_office",
                    "Add Billing Offices",
                    "add_billing_office",
                ),
                make_string_tuple("add_service_area", "Add Service Areas", "add_service_area"),
            ],
        }
        .render()
        .unwrap(),
        /*            checkboxes: vec![
                    (
                        String::from("report"),
                        String::from("Reports"),
                        String::from("true"),
                    ),
                    make_string_tuple("add_employee", "Manage Employees", "true"),
                    make_string_tuple(
                        "add_vr_counselor_support_coordinator",
                        "Manage VR Counselors",
                        "true",
                    ),
                    make_string_tuple("add_authorization", "Manage Clients", "true"),
                ],
            }
            .render()
            .unwrap(),
        */
    )
}

#[get("/add_employee")]
async fn add_employee_page(req: HttpRequest) -> HttpResponse {
    check_cookies_and_username(req.clone(), add_employee_page_internal, "add_employee").await
}

async fn manage_db(password: String, rx: Receiver<SyncMessage>) {
    let mut repo: ValueRepo<String> = OpenOptions::new()
        .mode(OpenMode::Create)
        .encryption(acid_store::repo::Encryption::XChaCha20Poly1305)
        .password(password.as_bytes())
        .memory_limit(ResourceLimit::Sensitive)
        .operations_limit(ResourceLimit::Sensitive)
        .open(&DirectoryConfig {
            path: PathBuf::from("storage"),
        })
        .unwrap();
    if !repo.contains("/schedule") {
        repo.insert(
            "/schedule".to_string(),
            &rmp_serde::to_vec(&Vec::<scheduled_notifications::ScheduledNotification>::new())
                .unwrap(),
        )
        .unwrap();
    }
    if !repo.contains("/caseload_list") {
        repo.insert(
            "/caseload_list".to_string(),
            &rmp_serde::to_vec(&Vec::<String>::new()).unwrap(),
        )
        .unwrap();
    }
    if !repo.contains("/waiting_clients_list") {
        repo.insert(
            "/waiting_clients_list".to_string(),
            &rmp_serde::to_vec(&Vec::<String>::new()).unwrap(),
        )
        .unwrap();
    }
    if !repo.contains("/employee_list") {
        repo.insert(
            "/employee_list".to_string(),
            &rmp_serde::to_vec(&Vec::<String>::new()).unwrap(),
        )
        .unwrap();
    }
    if !repo.contains("/service_area_list") {
        repo.insert(
            "/service_area_list".to_string(),
            &rmp_serde::to_vec(&client::ServiceArea::list_all()).unwrap(),
        )
        .unwrap();
    }
    if !repo.contains("/billing_office_list") {
        repo.insert(
            "/billing_office_list".to_string(),
            &rmp_serde::to_vec(&reports::BillingOffice::list_all()).unwrap(),
        )
        .unwrap();
    }
    if !repo.contains("/client_list") {
        repo.insert(
            "/client_list".to_string(),
            &rmp_serde::to_vec(&vec![String::from("none")]).unwrap(),
        )
        .unwrap();
    }
    /*
    if !repo.contains("/vr_counselor_support_coordinator_list") {
        repo.insert(
            "/vr_counselor_support_coordinator_list".to_string(),
            &rmp_serde::to_vec(&vec![String::from("none")]).unwrap(),
        )
        .unwrap();
    }
    */
    if !repo.contains("/setup_done") {
        repo.insert(
            "/setup_done".to_string(),
            &rmp_serde::to_vec(&false).unwrap(),
        )
        .unwrap();
    }
    //if !repo.contains("/clock_types") {
    repo.insert(
        "/clock_types".to_string(),
        &rmp_serde::to_vec(
            &vec![
                "Client Service",
                "Driving",
                "Paperwork",
                "Shadowing",
                "Training",
                "Admin Tasks",
                "Meeting",
                "Other",
            ]
            .iter()
            .map(|x| x.to_string())
            .collect::<Vec<String>>(),
        )
        .unwrap(),
    )
    .unwrap();
    //}
    //if !repo.contains("/office_location_list") {
    repo.insert(
        "/office_location_list".to_string(),
        &rmp_serde::to_vec(
            &vec![
                "Corporate",
                "Orem",
                "Spanish Fork",
                "Salt Lake City",
                "St. George",
                "All",
            ]
            .iter()
            .map(|x| x.to_string())
            .collect::<Vec<String>>(),
        )
        .unwrap(),
    )
    .unwrap();
    //}
    //if !repo.contains("/permission_list") {
    repo.insert(
        "/permission_list".to_string(),
        &rmp_serde::to_vec(
            &vec![
                "add_employee",
                "report",
                "add_authorization",
                "add_service_area",
                "add_billing_office",
            ]
            .iter()
            .map(|x| x.to_string())
            .collect::<Vec<String>>(),
        )
        .unwrap(),
    )
    .unwrap();

    //}
    repo.insert(
        "/report_index_list".to_string(),
        &rmp_serde::to_vec(&vec![
            (
                String::from("Ongoing Supports Monthly Job Coaching"),
                String::from("/reports/ongoing_supports/monthly_jc"),
            ),
            (
                String::from("Job Coaching Tracker"),
                String::from("/reports/jc/tracker"),
            ),
            (
                String::from("Billing Tracking VR Hourly Report"),
                String::from("/reports/billing_tracking_vr_hourly"),
            ),
            (
                String::from("JC Client Schedule"),
                String::from("/reports/jc/client_schedule"),
            ),
            (
                String::from("Billing Tracking VR JD Report"),
                String::from("/reports/billing_tracking_vr_jd"),
            ),
            (
                String::from("CIE Job Placement Report"),
                String::from("/reports/jd/cie_job_placement"),
            ),
        ])
        .unwrap(),
    )
    .unwrap();
    //if !repo.contains("/client_list")
    repo.commit().unwrap();
    let mut shut_down = false;
    let mut received = match rx.recv() {
        Ok(z) => z,
        Err(_) => SyncMessage::Shutdown,
    };
    if let SyncMessage::Shutdown = received {
        shut_down = true;
    }
    while !shut_down {
        match received {
            SyncMessage::Get(ref key_name, ref response_tx) => {
                match repo.get::<str, Vec<u8>>(key_name) {
                    Ok(val) => {
                        response_tx.send(SyncMessage::GetSuccess(val)).unwrap();
                    }
                    Err(e) => {
                        response_tx.send(SyncMessage::GetError(e)).unwrap();
                    }
                }
            }
            SyncMessage::Shutdown => {
                shut_down = true;
                continue;
            }
            SyncMessage::KeyExists(ref key_name, ref response_tx) => {
                response_tx
                    .send(SyncMessage::KeyExistsResponse(repo.contains(key_name)))
                    .unwrap();
            }
            SyncMessage::Insert(ref key_name, ref value, ref response_tx) => {
                match repo.insert(key_name.clone(), value) {
                    Ok(_) => {
                        repo.commit().unwrap();
                        response_tx.send(SyncMessage::InsertSuccess).unwrap();
                    }
                    Err(e) => {
                        response_tx.send(SyncMessage::InsertError(e)).unwrap();
                    }
                }
            }
            SyncMessage::BatchInsert(ref insert_commands, ref response_tx) => {
                let mut worked = true;
                for (key_name, value) in insert_commands {
                    if let Err(e) = repo.insert(key_name.clone(), value) {
                        response_tx.send(SyncMessage::BatchInsertError(e)).unwrap();
                        worked = false;
                        break;
                    }
                }
                if worked {
                    repo.commit().unwrap();
                    response_tx.send(SyncMessage::BatchInsertSuccess).unwrap();
                } else {
                    repo.rollback().unwrap();
                }
            }
            SyncMessage::Remove(ref key_name, ref response_tx) => {
                let worked = repo.remove(key_name);
                repo.commit().unwrap();
                response_tx
                    .send(SyncMessage::RemoveResponse(worked))
                    .unwrap();
            }
            SyncMessage::AppendToSchedule(scheduled_notification, ref response_tx) => {
                match repo.get::<str, Vec<u8>>("/schedule") {
                    Ok(val) => {
                        let mut notification_list: Vec<
                            scheduled_notifications::ScheduledNotification,
                        > = rmp_serde::from_slice(val.as_slice()).unwrap();
                        notification_list.push(scheduled_notification);
                        match repo.insert(
                            String::from("/schedule"),
                            &rmp_serde::to_vec(&notification_list).unwrap(),
                        ) {
                            Ok(_) => {
                                repo.commit().unwrap();
                                response_tx
                                    .send(SyncMessage::SchedAppendResponse(None))
                                    .unwrap();
                            }
                            Err(e) => {
                                response_tx
                                    .send(SyncMessage::SchedAppendResponse(Some(e)))
                                    .unwrap();
                            }
                        }
                        //response_tx.send(SyncMessage::GetSuccess(val)).unwrap();
                    }
                    Err(e) => {
                        response_tx
                            .send(SyncMessage::SchedAppendResponse(Some(e)))
                            .unwrap();
                    }
                }
            }
            SyncMessage::RemoveFromSchedule(scheduled_notification, ref response_tx) => {
                match repo.get::<str, Vec<u8>>("/schedule") {
                    Ok(val) => {
                        let mut notification_list: Vec<
                            scheduled_notifications::ScheduledNotification,
                        > = rmp_serde::from_slice(val.as_slice()).unwrap();
                        notification_list.retain(|x| x != &scheduled_notification);
                        match repo.insert(
                            String::from("/schedule"),
                            &rmp_serde::to_vec(&notification_list).unwrap(),
                        ) {
                            Ok(_) => {
                                repo.commit().unwrap();
                                response_tx
                                    .send(SyncMessage::SchedRemoveResponse(None))
                                    .unwrap();
                            }
                            Err(e) => {
                                response_tx
                                    .send(SyncMessage::SchedRemoveResponse(Some(e)))
                                    .unwrap();
                            }
                        }
                        //response_tx.send(SyncMessage::GetSuccess(val)).unwrap();
                    }
                    Err(e) => {
                        response_tx
                            .send(SyncMessage::SchedRemoveResponse(Some(e)))
                            .unwrap();
                    }
                }
            }
            _ => (),
        }
        received = match rx.recv() {
            Ok(z) => z,
            Err(_) => SyncMessage::Shutdown,
        };
    }
}

fn format_duration_hm(duration: chrono::Duration) -> String {
    let correct_duration = duration;
    let num_hours = correct_duration.num_hours();
    let num_minutes = correct_duration.num_minutes() % 60;
    format!("{num_hours}h{num_minutes}m")
}

async fn calculate_clocks_today(
    req: HttpRequest,
    employee: Employee,
) -> Option<(Vec<(String, String, String, String)>, String)> {
    let username = employee.username.clone();
    let local_tz: Tz = get_value!(req.clone(), "/employee_list/{username}/timezone").unwrap();
    //let local_start_date = chrono::Local::today();
    let now = Utc::now();
    let now_time = now.time();
    let date_time =
        Utc.ymd(now.year(), now.month(), now.day())
            .and_hms(now_time.hour(), now_time.minute(), 0);
    let local_now = date_time.with_timezone(&local_tz);
    let local_start_date: DateTime<Tz> = local_tz
        .ymd(local_now.year(), local_now.month(), local_now.day())
        .and_hms(0, 0, 0);
    //.from_local_datetime(&chrono::Local::today().naive_local().and_hms(0, 0, 0))
    //.unwrap();
    //let local_end_date: DateTime<Tz> = local_tz
    //    .from_local_datetime(&form.end_date.and_hms(0, 0, 0))
    //    .unwrap();
    let mut clock_times: Vec<(String, String, String, String)> = Vec::new();
    let local_end_date = local_start_date + chrono::Duration::hours(24);
    let utc_start_date = local_start_date.with_timezone(&Utc);
    let utc_end_date = local_end_date.with_timezone(&Utc);
    let mut clocked_in_duration = chrono::Duration::seconds(0);
    let mut clocked_times = employee
        .clocked_times
        .iter()
        .filter_map(|x| {
            if x.extract_datetime() >= utc_start_date && x.extract_datetime() < utc_end_date {
                Some(x.clone())
            } else {
                None
            }
        })
        .collect::<Vec<ClockedTime>>();
    //println!("{:#?}", clocked_times[0]);
    if employee.clocked_times.len() < 1 {
        return None;
    }
    if clocked_times.len() == 0 {
        clocked_times.push(
            employee
                .clocked_times
                .last()
                .unwrap()
                .clone()
                .with_modified_datetime(utc_start_date.clone()),
        );
    }
    let mut current_set: Vec<String> = Vec::new();
    if !clocked_times[0].clone().is_clocking_in() {
        let position_result = employee
            .clocked_times
            .iter()
            .position(|x| x == &clocked_times[0].clone());
        let position: usize;
        if position_result.is_none() {
            return None;
        } else {
            position = position_result.unwrap()
        }
        //.unwrap();
        let mut prev_clocked_time = employee.clocked_times[position - 1].clone();
        prev_clocked_time =
            prev_clocked_time.with_modified_datetime(local_start_date.with_timezone(&Utc));
        clocked_times.insert(0, prev_clocked_time);
    }
    let final_num = clocked_times.len() - 1;
    for (num, clocked_time) in clocked_times.iter().enumerate() {
        //println!("{num}");
        let this_time = clocked_time.extract_datetime();
        if final_num == num {
            if num % 2 == 0 {
                let final_comparison: DateTime<Utc>;
                if utc_end_date >= date_time {
                    final_comparison = date_time.clone();
                } else {
                    final_comparison = utc_end_date.clone();
                }
                clocked_in_duration = clocked_in_duration
                    .checked_add(&final_comparison.signed_duration_since(this_time))
                    .unwrap();
                current_set.push(format!("{}", clocked_time.clock_type_string()));
                current_set.push(
                    this_time
                        .with_timezone(&local_tz)
                        .format("%I:%M %p %Z")
                        .to_string(),
                );
                current_set.push(
                    /*
                    this_time
                        .with_timezone(&local_tz)
                        .format("%r %Z")
                        .to_string()
                    */
                    String::from("Now"),
                );
                current_set
                    .push(format_duration_hm(final_comparison.signed_duration_since(
                        clocked_times[num].extract_datetime().clone(),
                    )));
                clock_times.push((
                    current_set[0].clone(),
                    current_set[1].clone(),
                    current_set[2].clone(),
                    current_set[3].clone(),
                ));
                current_set = Vec::new();
            } else {
                clocked_in_duration =
                    clocked_in_duration
                        .checked_add(&this_time.signed_duration_since(
                            clocked_times[num - 1].extract_datetime().clone(),
                        ))
                        .unwrap();
                current_set.push(
                    this_time
                        .with_timezone(&local_tz)
                        .format("%I:%M %p %Z")
                        .to_string(),
                );
                current_set.push(format_duration_hm(this_time.signed_duration_since(
                    clocked_times[num - 1].extract_datetime().clone(),
                )));
                clock_times.push((
                    current_set[0].clone(),
                    current_set[1].clone(),
                    current_set[2].clone(),
                    current_set[3].clone(),
                ));
                current_set = Vec::new();
            }
        } else if num % 2 == 1 {
            clocked_in_duration = clocked_in_duration
                .checked_add(
                    &this_time
                        .signed_duration_since(clocked_times[num - 1].extract_datetime().clone()),
                )
                .unwrap();
            current_set.push(
                this_time
                    .with_timezone(&local_tz)
                    .format("%I:%M %p %Z")
                    .to_string(),
            );
            current_set.push(format_duration_hm(this_time.signed_duration_since(
                clocked_times[num - 1].extract_datetime().clone(),
            )));
            clock_times.push((
                current_set[0].clone(),
                current_set[1].clone(),
                current_set[2].clone(),
                current_set[3].clone(),
            ));
            current_set = Vec::new();
        } else {
            current_set.push(format!("{}", clocked_time.clock_type_string()));
            current_set.push(
                this_time
                    .with_timezone(&local_tz)
                    .format("%I:%M %p %Z")
                    .to_string(),
            );
        }
    }
    if clock_times.len() == 0 {
        None
    } else {
        Some((clock_times, format_duration_hm(clocked_in_duration)))
    }
}

#[get("/static/logo.png")]
async fn logo() -> HttpResponse {
    HttpResponse::Ok().content_type("image/png").body(LOGO_PNG)
}

#[get("/static/proud_partner.png")]
async fn proud_partner() -> HttpResponse {
    HttpResponse::Ok()
        .content_type("image/png")
        .body(PROUD_PARTNER_PNG)
}

#[get("/static/dws_usor.png")]
async fn dws_usor_logo() -> HttpResponse {
    HttpResponse::Ok()
        .content_type("image/png")
        .body(DWS_USOR_PNG)
}

#[tokio::main]
async fn main() {
    let (tx, rx) = unbounded::<SyncMessage>();
    //let (sched_tx, sched_rx) = unbounded::<bool>();
    let passwd = scanpw::scanpw!("Password: ").trim().to_string();
    if !std::path::Path::new("storage").exists() {
        if passwd != scanpw::scanpw!("\nConfirm Password: ").trim().to_string() {
            eprintln!("\n\nError: Passwords do not match.");
            std::process::exit(1);
        }
    }
    let db_task = tokio::task::spawn(manage_db(passwd, rx));
    let scheduler = tokio::task::spawn(scheduler(tx.clone()));
    //GLOBAL_DATA.read().await;
    let tx_clone = tx.clone();
    let mut cert_file = BufReader::new(std::fs::File::open("fullchain.pem").unwrap());
    let mut key_file = BufReader::new(std::fs::File::open("privkey.pem").unwrap());

    let cert_chain = certs(&mut cert_file)
        .unwrap()
        .into_iter()
        .map(Certificate)
        .collect();
    let mut keys = pkcs8_private_keys(&mut key_file).unwrap();

    let server_config = ServerConfig::builder()
        .with_safe_defaults()
        .with_no_client_auth()
        .with_single_cert(cert_chain, PrivateKey(keys.remove(0)))
        .unwrap();
    HttpServer::new(move || {
        App::new()
            .service(services![
                register_page,
                setup_page,
                setup_post,
                home_page,
                login_post,
                clock_post,
                logout,
                report_page,
                report_post,
                settings_page,
                settings_post,
                add_authorization_page
            ])
            .service(services![
                logo,
                add_authorization_post,
                add_budget_line_page,
                add_budget_line_post,
                //add_vr_counselor_support_coordinator_page,
                //add_vr_counselor_support_coordinator_post,
                add_employee_page,
                add_employee_post,
                register_post,
                monthly_jc_report_page,
                report_index_page,
                monthly_jc_report_post,
                monthly_jc_report_export,
                dws_usor_logo
            ])
            .service(services![
                //proud_partner,
                jc_tracker_report_page,
                jc_tracker_report_post,
                billing_tracking_vr_hourly_report_page,
                add_billing_tracking_row_post,
                billing_tracking_table_page,
                client_schedule_page,
                client_scheudule_row_post,
                jc_client_schedule_table_page,
                grab_jc_monthly_pdf,
                billing_tracking_vr_jd_page,
                add_billing_office_page,
                add_service_area_page
            ])
            .service(services![
                add_service_area_post,
                add_billing_office_post,
                save_billing_tracking_vr_jd_post,
                choose_caseload_page,
                change_caseload_post,
                management_index_page,
                manage_caseloads_page,
                manage_caseloads_post,
                billing_tracking_vr_jd_report_page,
                save_billing_tracking_vr_jd_report_post,
                cie_job_placement_page,
                cie_job_placement_post
            ])
            .service(services![
                client_intake_page,
                jc_tracker_report_export_page,
                client_intake_post
            ])
            .app_data(web::Data::new(tx_clone.clone()))
    })
    .bind_rustls(("0.0.0.0", 8080), server_config)
    .unwrap()
    .run()
    .await
    .unwrap();
    tx.send(SyncMessage::Shutdown).unwrap();
    scheduler.abort();
    db_task.await.unwrap();

    //GLOBAL_DATA.write().unlock().unwrap();
}
