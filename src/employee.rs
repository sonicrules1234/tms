//use crate::client::Client;
use chrono::{DateTime, Datelike, TimeZone, Timelike, Utc};
use lettre::{message::Mailbox, Address};
use rand::{RngCore, SeedableRng};
use serde::{Deserialize, Serialize};
use sha2::{Digest, Sha512};
use std::fmt;
use uuid::Uuid;
#[derive(Serialize, Deserialize, Debug, Clone, PartialEq, Eq)]
pub struct Employee {
    pub username: String,
    salt: [u8; 16],
    pass_hash: Vec<u8>,
    pub first_name: String,
    pub last_name: String,
    pub clocked_times: Vec<ClockedTime>,
    pub valid_clock_types: Vec<String>,
    pub department: String,
    pub access_level: AccessLevel,
    pub job_description: String,
    pub office: OfficeLocation,
    pub email: String,
    pub mail_box: Mailbox,
    //pub caseloads: Vec<String>,
}

#[derive(Serialize, Deserialize, Debug, Clone, PartialEq, Eq)]
pub enum OfficeLocation {
    Corporate,
    Orem,
    SpanishFork,
    SaltLakeCity,
    StGeorge,
    All,
}

impl std::fmt::Display for OfficeLocation {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match self {
            Self::Corporate => write!(f, "corporate"),
            Self::Orem => write!(f, "orem"),
            Self::SpanishFork => write!(f, "spanishfork"),
            Self::SaltLakeCity => write!(f, "saltlakecity"),
            Self::StGeorge => write!(f, "stgeorge"),
            Self::All => write!(f, "all"),
        }
    }
}

impl OfficeLocation {
    pub fn from(office: impl Into<String>) -> Self {
        let location = office.into();
        if location.to_lowercase() == "corporate" {
            OfficeLocation::Corporate
        } else if location.to_lowercase() == "orem" {
            OfficeLocation::Orem
        } else if location.to_lowercase() == "spanishfork"
            || location.to_lowercase() == "spanish fork"
        {
            OfficeLocation::SpanishFork
        } else if location.to_lowercase() == "saltlakecity"
            || location.to_lowercase() == "salt lake city"
        {
            OfficeLocation::SaltLakeCity
        } else if location.to_lowercase() == "stgeorge" || location.to_lowercase() == "st. george" {
            OfficeLocation::StGeorge
        } else {
            OfficeLocation::All
        }
    }
    pub fn matches(&self, office_location: Self) -> bool {
        if self == &office_location
            || office_location == Self::All
            || office_location == Self::Corporate
        {
            true
        } else {
            false
        }
    }
    pub fn string_proper_name(&self) -> String {
        match self {
            Self::Corporate => String::from("Corporate"),
            Self::Orem => String::from("Orem"),
            Self::SpanishFork => String::from("Spanish Fork"),
            Self::SaltLakeCity => String::from("Salt Lake City"),
            Self::StGeorge => String::from("St. George"),
            Self::All => String::from("All"),
        }
    }
}

#[derive(Serialize, Deserialize, Debug, Clone)]
pub enum EmployeeRecord {
    Registered(Employee),
    NotRegistered(EmployeeBuilder),
}

#[derive(Serialize, Deserialize, Debug, Clone)]
pub struct EmployeeBuilder {
    pub username: String,
    pub email: String,
    pub valid_clock_types: Vec<String>,
    pub department: String,
    pub access_level: AccessLevel,
    pub job_description: String,
    pub office: OfficeLocation,
    pub uuid_key: Uuid,
    pub uuid_code: Uuid,
    pub first_name: String,
    pub last_name: String,
}

impl EmployeeBuilder {
    pub async fn new(
        username: String,
        email: String,
        //employee: String,
        first_name: String,
        last_name: String,
        valid_clock_types: Vec<String>,
        department: String,
        access_level: AccessLevel,
        job_description: String,
        office: OfficeLocation,
    ) -> Self {
        Self {
            username: username,
            email: email,
            first_name: first_name,
            last_name: last_name,
            valid_clock_types: valid_clock_types,
            department: department,
            access_level: access_level,
            job_description: job_description,
            office: office,
            uuid_key: uuid::Uuid::new_v4(),
            uuid_code: uuid::Uuid::new_v4(),
        }
    }
    pub async fn build(&self, password: String) -> Employee {
        Employee::new(
            self.username.clone(),
            self.email.clone(),
            self.first_name.clone(),
            self.last_name.clone(),
            password,
            self.department.clone(),
            self.access_level.clone(),
            self.job_description.clone(),
            self.valid_clock_types.clone(),
            self.office.clone(),
        )
        .await
    }
}

pub enum ClockError {
    WrongType,
    WrongDirection,
}

#[derive(Serialize, Deserialize, Debug, Clone, PartialEq, Eq)]
pub enum AccessLevel {
    All,
    None,
    Custom(Vec<String>),
}

impl Employee {
    pub async fn new(
        username: String,
        email: String,
        first_name: String,
        last_name: String,
        password: String,
        department: String,
        access_level: AccessLevel,
        job_description: String,
        valid_clock_types: Vec<String>,
        office: OfficeLocation,
    ) -> Self {
        let mut salt: [u8; 16] = [0; 16];
        rand::rngs::StdRng::from_entropy().fill_bytes(&mut salt);
        let mut salt_pass = salt.to_vec();
        salt_pass.append(&mut password.bytes().collect());
        let mut hasher = Sha512::new();
        hasher.update(salt_pass);
        let pass_hash = hasher.finalize();
        Self {
            username: username,
            email: email.clone(),
            salt: salt,
            pass_hash: pass_hash.to_vec(),
            first_name: first_name.clone(),
            last_name: last_name.clone(),
            clocked_times: Vec::new(),
            valid_clock_types: valid_clock_types,
            department: department,
            access_level: access_level,
            job_description: job_description,
            office: office,
            mail_box: format!("{first_name} {last_name} <{email}>")
                .parse()
                .unwrap(), //caseloads: Vec::new(),
        }
    }
    pub async fn verify_password(&self, password: String) -> bool {
        let mut salt_pass = self.salt.to_vec();
        salt_pass.append(&mut password.bytes().collect());
        let mut hasher = Sha512::new();
        hasher.update(salt_pass);
        let pass_hash = hasher.finalize();
        pass_hash.to_vec() == self.pass_hash
    }
    pub async fn is_clocked_in(&self) -> bool {
        if self.clocked_times.len() > 0 {
            self.clocked_times.iter().last().unwrap().is_clocking_in()
        } else {
            false
        }
    }
    pub async fn clock_in(
        &mut self,
        clock_type: String,
        client_uuid: Option<Uuid>,
    ) -> Result<(), ClockError> {
        if self.valid_clock_types.contains(&clock_type) {
            let clocked_clone = self.clocked_times.clone();
            if clocked_clone.len() == 0 || !clocked_clone.iter().last().unwrap().is_clocking_in() {
                self.clocked_times
                    .push(ClockedTime::new(clock_type, true, client_uuid).await);
                Ok(())
            } else {
                Err(ClockError::WrongDirection)
            }
        } else {
            Err(ClockError::WrongType)
        }
    }
    pub async fn clock_out(&mut self) -> Result<(), ClockError> {
        let clocked_clone = self.clocked_times.clone();
        let last_clocked = clocked_clone.last().unwrap();
        if last_clocked.is_clocking_in() {
            self.clocked_times
                .push(ClockedTime::new(last_clocked.clock_type_string(), false, None).await);
            Ok(())
        } else {
            Err(ClockError::WrongDirection)
        }
    }
    pub fn is_allowed(&self, permission: impl Into<String>) -> bool {
        let permission_string = permission.into();
        if permission_string.to_lowercase() == "none" {
            return true;
        }
        match self.access_level {
            AccessLevel::All => true,
            AccessLevel::Custom(ref allowed_list) => allowed_list.contains(&permission_string),
            AccessLevel::None => false,
        }
    }
}

#[derive(Serialize, Deserialize, Debug, Clone, PartialEq, Eq)]
pub enum Direction {
    In(DateTime<Utc>, String),
    Out(DateTime<Utc>),
}

#[derive(Serialize, Deserialize, Debug, Clone, PartialEq, Eq)]
pub enum ClockedTime {
    Break(Direction),
    Driving(Direction),
    Paperwork(Direction),
    Shadowing(Direction),
    Training(Direction),
    AdminTasks(Direction),
    Other(Direction),
    ClientService(Direction),
    Meeting(Direction),
}

impl ClockedTime {
    pub async fn new(
        clock_type: String,
        clocking_in: bool,
        client_uuid: Option<Uuid>,
    ) -> ClockedTime {
        let direction: Direction;
        let now = Utc::now();
        let now_time = now.time();
        let date_time = Utc.ymd(now.year(), now.month(), now.day()).and_hms(
            now_time.hour(),
            now_time.minute(),
            0,
        );
        if clocking_in {
            direction = match client_uuid {
                Some(client_id) => Direction::In(date_time, client_id.to_string()),
                None => Direction::In(date_time, String::from("none")),
            };
        } else {
            direction = Direction::Out(date_time);
        }
        if clock_type.to_lowercase() == "break" {
            ClockedTime::Break(direction)
        } else if clock_type.to_lowercase() == "client service" {
            ClockedTime::ClientService(direction)
        /*} else if clock_type.to_lowercase() == "jc" {
            ClockedTime::JobCoaching(direction)
        } else if clock_type.to_lowercase() == "ls" {
            ClockedTime::LifeSkills(direction)
        } else if clock_type.to_lowercase() == "jd" {
            ClockedTime::JobDevelopment(direction)
        } else if clock_type.to_lowercase() == "sb" {
            ClockedTime::ServiceBrokering(direction)
        } else if clock_type.to_lowercase() == "wsatier1" {
            ClockedTime::WsaTier1(direction)
        } else if clock_type.to_lowercase() == "wsatier2" {
            ClockedTime::WsaTier2(direction)
        } else if clock_type.to_lowercase() == "discovery" {
            ClockedTime::Discovery(direction)*/
        } else if clock_type.to_lowercase() == "driving" {
            ClockedTime::Driving(direction)
        } else if clock_type.to_lowercase() == "paperwork" {
            ClockedTime::Paperwork(direction)
        } else if clock_type.to_lowercase() == "shadowing" {
            ClockedTime::Shadowing(direction)
        } else if clock_type.to_lowercase() == "training" {
            ClockedTime::Training(direction)
        } else if clock_type.to_lowercase() == "admin tasks" {
            ClockedTime::AdminTasks(direction)
        } else if clock_type.to_lowercase() == "meeting" {
            ClockedTime::Meeting(direction)
        } else {
            ClockedTime::Other(direction)
        }
    }
    pub fn clock_type_string(&self) -> String {
        match self {
            //Self::Break(_) => String::from("break"),
            Self::Break(_direction) => String::from("Break"), /*
            Self::JobCoaching(_direction) => String::from("JC"),
            Self::LifeSkills(_direction) => String::from("LS"),
            Self::JobDevelopment(_direction) => String::from("JD"),
            Self::ServiceBrokering(_direction) => String::from("SB"),
            Self::WsaTier1(_direction) => String::from("WSA Tier 1"),
            Self::WsaTier2(_direction) => String::from("WSA Tier 2"),
            Self::Discovery(_direction) => String::from("Discovery"),*/
            Self::Driving(_direction) => String::from("Driving"),
            Self::Paperwork(_direction) => String::from("Paperwork"),
            Self::Shadowing(_direction) => String::from("Shadowing"),
            Self::Training(_direction) => String::from("Training"),
            Self::AdminTasks(_direction) => String::from("Admin Tasks"),
            Self::Other(_direction) => String::from("Other"),
            Self::ClientService(_duration) => String::from("Client Service"),
            Self::Meeting(_direction) => String::from("Meeting"),
        }
    }
    pub fn extract_datetime(&self) -> DateTime<Utc> {
        let direction = match self {
            Self::Break(direction) => direction,
            /*
            Self::JobCoaching(direction) => direction,
            Self::LifeSkills(direction) => direction,
            Self::JobDevelopment(direction) => direction,
            Self::ServiceBrokering(direction) => direction,
            Self::WsaTier1(direction) => direction,
            Self::WsaTier2(direction) => direction,
            Self::Discovery(direction) => direction,
            */
            Self::Driving(direction) => direction,
            Self::Paperwork(direction) => direction,
            Self::Shadowing(direction) => direction,
            Self::Training(direction) => direction,
            Self::AdminTasks(direction) => direction,
            Self::Other(direction) => direction,
            Self::ClientService(direction) => direction,
            Self::Meeting(direction) => direction,
        };
        match direction {
            Direction::In(date_time, _) => date_time.clone(),
            Direction::Out(date_time) => date_time.clone(),
        }
    }
    pub fn extract_client_uuid(&self) -> Option<String> {
        let direction = self.extract_direction();
        if let Direction::In(_, client_uuid) = direction {
            Some(client_uuid)
        } else {
            None
        }
    }
    pub fn is_clocking_in(&self) -> bool {
        let direction = match self {
            Self::Break(direction) => direction,
            /*
            Self::JobCoaching(direction) => direction,
            Self::LifeSkills(direction) => direction,
            Self::JobDevelopment(direction) => direction,
            Self::ServiceBrokering(direction) => direction,
            Self::WsaTier1(direction) => direction,
            Self::WsaTier2(direction) => direction,
            Self::Discovery(direction) => direction,
            */
            Self::Driving(direction) => direction,
            Self::Paperwork(direction) => direction,
            Self::Shadowing(direction) => direction,
            Self::Training(direction) => direction,
            Self::AdminTasks(direction) => direction,
            Self::Other(direction) => direction,
            Self::ClientService(direction) => direction,
            Self::Meeting(direction) => direction,
        };
        if let Direction::In(_, _) = direction {
            true
        } else {
            false
        }
    }
    pub fn extract_direction(&self) -> Direction {
        let direction = match self {
            Self::Break(direction) => direction,
            /*
            Self::JobCoaching(direction) => direction,
            Self::LifeSkills(direction) => direction,
            Self::JobDevelopment(direction) => direction,
            Self::ServiceBrokering(direction) => direction,
            Self::WsaTier1(direction) => direction,
            Self::WsaTier2(direction) => direction,
            Self::Discovery(direction) => direction,
            */
            Self::Driving(direction) => direction,
            Self::Paperwork(direction) => direction,
            Self::Shadowing(direction) => direction,
            Self::Training(direction) => direction,
            Self::AdminTasks(direction) => direction,
            Self::Other(direction) => direction,
            Self::ClientService(direction) => direction,
            Self::Meeting(direction) => direction,
        };
        direction.clone()
    }
    pub fn with_modified_datetime(&self, datetime: DateTime<Utc>) -> Self {
        let direction = self.extract_direction();
        let new_direction = match direction {
            Direction::In(_, ref client) => Direction::In(datetime, client.clone()),
            Direction::Out(_) => Direction::Out(datetime),
        };
        match self {
            Self::Break(_) => Self::Break(new_direction),
            Self::Driving(_) => Self::Driving(new_direction),
            Self::Paperwork(_) => Self::Paperwork(new_direction),
            Self::Shadowing(_) => Self::Shadowing(new_direction),
            Self::Training(_) => Self::Training(new_direction),
            Self::AdminTasks(_) => Self::AdminTasks(new_direction),
            Self::Other(_) => Self::Other(new_direction),
            Self::ClientService(_) => Self::ClientService(new_direction),
            Self::Meeting(_) => Self::Meeting(new_direction),
        }
    }
}
