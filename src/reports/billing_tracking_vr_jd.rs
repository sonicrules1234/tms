use chrono::{offset::Utc, DateTime, Datelike, TimeZone, Timelike};
//use chrono_tz::Tz;
use serde::{Deserialize, Serialize};
use std::fmt;
use uuid::Uuid;

#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct BillingTrackingVRJDRow {
    pub client_uuid: Uuid,
    //pub office_location: crate::employee::OfficeLocation,
    //pub vr_counselor_support_coordinator: String,
    pub employee_username: Option<String>,
    //pub auth_num: String,
    //pub auth_type: crate::client::ClientService,
    /*
    pub jan: Option<std::time::Duration>,
    pub feb: Option<std::time::Duration>,
    pub march: Option<std::time::Duration>,
    pub april: Option<std::time::Duration>,
    pub may: Option<std::time::Duration>,
    pub june: Option<std::time::Duration>,
    pub july: Option<std::time::Duration>,
    pub aug: Option<std::time::Duration>,
    pub sept: Option<std::time::Duration>,
    pub oct: Option<std::time::Duration>,
    pub nov: Option<std::time::Duration>,
    pub dec: Option<std::time::Duration>,
    */
    //pub total_jd_duration: std::time::Duration,
    //pub vr_billing_location: crate::reports::BillingOffice,
    pub start_tracking_datetime: Option<DateTime<Utc>>,
    pub cur_job_seek: bool,
    pub comments: String,
}

#[derive(Debug, Clone, Serialize, Deserialize)]
pub enum Month {
    January,
    February,
    March,
    April,
    May,
    June,
    July,
    August,
    September,
    October,
    November,
    December,
}

impl std::fmt::Display for Month {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match self {
            Self::January => write!(f, "Jan"),
            Self::February => write!(f, "Feb"),
            Self::March => write!(f, "March"),
            Self::April => write!(f, "April"),
            Self::May => write!(f, "May"),
            Self::June => write!(f, "June"),
            Self::July => write!(f, "July"),
            Self::August => write!(f, "Aug"),
            Self::September => write!(f, "Sept"),
            Self::October => write!(f, "Oct"),
            Self::November => write!(f, "Nov"),
            Self::December => write!(f, "Dec"),
        }
    }
}

impl Month {
    pub fn from(val: String) -> Self {
        if val.to_lowercase() == "jan" {
            Self::January
        } else if val.to_lowercase() == "feb" {
            Self::February
        } else if val.to_lowercase() == "march" {
            Self::March
        } else if val.to_lowercase() == "april" {
            Self::April
        } else if val.to_lowercase() == "may" {
            Self::May
        } else if val.to_lowercase() == "june" {
            Self::June
        } else if val.to_lowercase() == "july" {
            Self::July
        } else if val.to_lowercase() == "aug" {
            Self::August
        } else if val.to_lowercase() == "sept" {
            Self::September
        } else if val.to_lowercase() == "oct" {
            Self::October
        } else if val.to_lowercase() == "nov" {
            Self::November
        } else {
            Self::December
        }
    }
    pub fn list_all() -> [Self; 12] {
        [
            Self::January,
            Self::February,
            Self::March,
            Self::April,
            Self::May,
            Self::June,
            Self::July,
            Self::August,
            Self::September,
            Self::October,
            Self::November,
            Self::December,
        ]
    }
}
/*
#[derive(Debug, Clone, Serialize, Deserialize)]
pub enum VRBillingLocation {
    SpanishFork,
    Provo,
    StGeorge,
    CedarCity,
    Taylorsville,
    SaltLakeCityDowntown,
    ValleyWest,
    SouthJordan,
}

impl std::fmt::Display for VRBillingLocation {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match self {
            Self::SpanishFork => write!(f, "Spanish Fork"),
            Self::Provo => write!(f, "Provo"),
            Self::StGeorge => write!(f, "St. George"),
            Self::CedarCity => write!(f, "Cedar City"),
            Self::Taylorsville => write!(f, "Taylorsville"),
            Self::SaltLakeCityDowntown => write!(f, "Salt Lake City Downtown"),
            Self::ValleyWest => write!(f, "Valley West"),
            Self::SouthJordan => write!(f, "South Jordan"),
        }
    }
}

impl VRBillingLocation {
    pub fn from(val: String) -> Self {
        let new_val = val.to_lowercase().replace(" ", "").replace(".", "");
        if new_val == "spanishfork" {
            Self::SpanishFork
        } else if new_val == "provo" {
            Self::Provo
        } else if new_val == "stgeorge" {
            Self::StGeorge
        } else if new_val == "cedarcity" {
            Self::CedarCity
        } else if new_val == "taylorsville" {
            Self::Taylorsville
        } else if new_val == "saltlakecitydowntown" {
            Self::SaltLakeCityDowntown
        } else if new_val == "valleywest" {
            Self::ValleyWest
        } else {
            Self::SouthJordan
        }
    }
    pub fn list_all() -> Vec<VRBillingLocation> {
        vec![
            Self::SpanishFork,
            Self::Provo,
            Self::StGeorge,
            Self::CedarCity,
            Self::Taylorsville,
            Self::SaltLakeCityDowntown,
            Self::ValleyWest,
            Self::SouthJordan,
        ]
    }
}
*/

#[derive(Debug, Clone, Serialize, Deserialize)]
pub enum Service {
    SJBT,
    TWE,
    WBLE,
    SE,
    CE,
}

impl Service {
    pub fn from(val: String) -> Self {
        if val.to_lowercase() == "sjbt" {
            Self::SJBT
        } else if val.to_lowercase() == "twe" {
            Self::TWE
        } else if val.to_lowercase() == "wble" {
            Self::WBLE
        } else if val.to_lowercase() == "se" {
            Self::SE
        } else {
            Self::CE
        }
    }
    pub fn list_all() -> Vec<Self> {
        vec![Self::SJBT, Self::TWE, Self::WBLE, Self::SE, Self::CE]
    }
}

impl std::fmt::Display for Service {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match self {
            Self::SJBT => write!(f, "SJBT"),
            Self::TWE => write!(f, "TWE"),
            Self::WBLE => write!(f, "WBLE"),
            Self::SE => write!(f, "SE"),
            Self::CE => write!(f, "CE"),
        }
    }
}

impl BillingTrackingVRJDRow {
    pub fn new(
        client_uuid: Uuid,
        employee_username: Option<String>,
        //office_location: crate::employee::OfficeLocation,
        //vr_counselor_support_coordinator: String,
        //auth_type: crate::client::ClientService,
        //auth_num: String,
        //service: Service,
        //vr_billing_location: crate::reports::BillingOffice,
        //cur_job_seek: bool,
        comments: String,
    ) -> Self {
        Self {
            client_uuid: client_uuid,
            start_tracking_datetime: None,
            employee_username: employee_username,
            //office_location: office_location,
            //vr_counselor_support_coordinator: vr_counselor_support_coordinator,
            //auth_num: auth_num,
            //auth_type: auth_type,
            /*
            jan: None,
            feb: None,
            march: None,
            april: None,
            may: None,
            june: None,
            july: None,
            aug: None,
            sept: None,
            oct: None,
            nov: None,
            dec: None,
            */
            //vr_billing_location: vr_billing_location,
            cur_job_seek: false,
            comments: comments.clone(),
            //total_jd_duration: std::time::Duration::from_secs(0),
        }
    }
    pub fn start_tracking_now(&mut self) {
        let now = Utc::now();
        self.start_tracking_datetime = Some(Utc.ymd(now.year(), now.month(), now.day()).and_hms(
            now.hour(),
            now.minute(),
            0,
        ));
    }
}
