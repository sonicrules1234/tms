use chrono::{DateTime, Utc};
use serde::{Deserialize, Serialize};
//use std::fmt;

#[derive(Serialize, Deserialize, Debug, Clone)]
pub struct CIEJobPlacement {
    pub employer: String,
    pub employer_address: String,
    pub job_title: String,
    pub employment_start_date: DateTime<Utc>,
    pub wage: f32,
    pub hours_per_week: String,
    pub benefits: String,
    pub supervisor_name: String,
    pub supervisor_contact: String,
    pub q_client_interact_other_exp: Option<String>,
    pub q_coworker_prev_exp: Option<String>,
    pub q_wage_ge_customary_exp: Option<String>,
    pub q_client_same_opp_adv_exp: Option<String>,
    pub q_access_same_non_exp: Option<String>,
    pub crp_signature: String,
    pub sig_date: DateTime<Utc>,
}
