use serde::{Deserialize, Serialize};
use std::fmt;
pub mod billing_tracking_vr_hourly;
pub mod billing_tracking_vr_jd;
pub mod jc;
pub mod jd;
pub mod ongoing_supports;

#[derive(Debug, Clone, Serialize, Deserialize, PartialEq, Eq)]
pub enum BillingOffice {
    SpanishFork,
    Provo,
    StGeorge,
    CedarCity,
    Taylorsville,
    SaltLakeCityDowntown,
    ValleyWest,
    SouthJordan,
    Delta,
    HeberCity,
    Custom(String),
}

impl std::fmt::Display for BillingOffice {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match self {
            Self::SpanishFork => write!(f, "Spanish Fork"),
            Self::Provo => write!(f, "Provo"),
            Self::StGeorge => write!(f, "St. George"),
            Self::CedarCity => write!(f, "Cedar City"),
            Self::Taylorsville => write!(f, "Taylorsville"),
            Self::SaltLakeCityDowntown => write!(f, "Salt Lake City Downtown"),
            Self::ValleyWest => write!(f, "Valley West"),
            Self::SouthJordan => write!(f, "South Jordan"),
            Self::Delta => write!(f, "Delta"),
            Self::HeberCity => write!(f, "Heber City"),
            Self::Custom(x) => write!(f, "{x}"),
        }
    }
}

impl BillingOffice {
    pub fn from(val: String, billing_office_list: Vec<Self>) -> Option<Self> {
        let new_val = val.to_lowercase().replace(" ", "").replace(".", "");
        /*
        if new_val == "spanishfork" {
            return Some(Self::SpanishFork);
        } else if new_val == "provo" {
            return Some(Self::Provo);
        } else if new_val == "stgeorge" {
            return Some(Self::StGeorge);
        } else if new_val == "cedarcity" {
            return Some(Self::CedarCity);
        } else if new_val == "taylorsville" {
            return Some(Self::Taylorsville);
        } else if new_val == "saltlakecitydowntown" {
            return Some(Self::SaltLakeCityDowntown);
        } else if new_val == "valleywest" {
            return Some(Self::ValleyWest);
        } else if new_val == "delta" {
            return Some(Self::Delta);
        } else if new_val == "hebercity" {
            return Some(Self::HeberCity);
        } else if new_val == "southjordan" {
            return Some(Self::SouthJordan);
        } else {
            */
        for billing_office in billing_office_list {
            if new_val
                == format!("{billing_office}")
                    .replace(".", "")
                    .replace(" ", "")
                    .to_lowercase()
            {
                return Some(billing_office);
            }
        }
        //}
        None
    }
    pub fn list_all() -> Vec<Self> {
        vec![
            Self::SpanishFork,
            Self::Provo,
            Self::StGeorge,
            Self::CedarCity,
            Self::Taylorsville,
            Self::SaltLakeCityDowntown,
            Self::ValleyWest,
            Self::SouthJordan,
            Self::Delta,
            Self::HeberCity,
        ]
    }
}

#[derive(Debug, Clone, Serialize, Deserialize)]
pub enum Rating {
    R1,
    R2,
    R3,
    R4,
    R5,
    R6,
    R7,
    R8,
    R9,
    R10,
}

impl std::fmt::Display for Rating {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match self {
            Self::R1 => write!(f, "1"),
            Self::R2 => write!(f, "2"),
            Self::R3 => write!(f, "3"),
            Self::R4 => write!(f, "4"),
            Self::R5 => write!(f, "5"),
            Self::R6 => write!(f, "6"),
            Self::R7 => write!(f, "7"),
            Self::R8 => write!(f, "8"),
            Self::R9 => write!(f, "9"),
            Self::R10 => write!(f, "10"),
        }
    }
}

#[derive(Debug, Clone, Serialize, Deserialize)]
pub enum RatingError {
    InvalidRating,
}

impl Rating {
    pub fn from(num: impl Into<String>) -> Result<Self, RatingError> {
        let number = num.into();
        if number == "1" {
            Ok(Self::R1)
        } else if number == "2" {
            Ok(Self::R2)
        } else if number == "3" {
            Ok(Self::R3)
        } else if number == "4" {
            Ok(Self::R4)
        } else if number == "5" {
            Ok(Self::R5)
        } else if number == "6" {
            Ok(Self::R6)
        } else if number == "7" {
            Ok(Self::R7)
        } else if number == "8" {
            Ok(Self::R8)
        } else if number == "9" {
            Ok(Self::R9)
        } else if number == "10" {
            Ok(Self::R10)
        } else {
            Err(RatingError::InvalidRating)
        }
    }
}
