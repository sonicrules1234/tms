use chrono::Duration;
use serde::{Deserialize, Serialize};
//use std::fmt;
use uuid::Uuid;
#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct BillingTrackingDSPDRow {
    pub client_uuid: Uuid,
    pub office_location: crate::employee::OfficeLocation,
    pub vr_counselor_support_coordinator: String,
    pub employee_username: String,
    pub auth_type: crate::client::ClientService,
    pub duration_authorized: std::time::Duration,
    pub duration_billed: std::time::Duration,
    pub duration_unbilled: std::time::Duration,
}

impl BillingTrackingVRHourlyRow {
    pub fn new(
        client_uuid: Uuid,
        employee_username: String,
        office_location: crate::employee::OfficeLocation,
        vr_counselor_support_coordinator: String,
        auth_type: crate::client::ClientService,
        auth_num: String,
        hours_authed: usize,
        hours_billed: usize,
        minutes_billed: usize,
        hours_unbilled: usize,
        minutes_unbilled: usize,
        currently_working_with: bool,
        notes: String,
    ) -> Self {
        Self {
            client_uuid: client_uuid,
            employee_username: employee_username,
            office_location: office_location,
            vr_counselor_support_coordinator: vr_counselor_support_coordinator,
            auth_num: auth_num,
            auth_type: auth_type,
            duration_authorized: chrono::Duration::hours(hours_authed.try_into().unwrap())
                .to_std()
                .unwrap(),
            duration_billed: chrono::Duration::hours(hours_billed.try_into().unwrap())
                .checked_add(&Duration::minutes(minutes_billed.try_into().unwrap()))
                .unwrap()
                .to_std()
                .unwrap(),
            duration_unbilled: chrono::Duration::hours(hours_unbilled.try_into().unwrap())
                .checked_add(&Duration::minutes(minutes_unbilled.try_into().unwrap()))
                .unwrap()
                .to_std()
                .unwrap(),
            currently_working_with: currently_working_with,
            notes: notes,
        }
    }
}
