use crate::reports::Rating;
use chrono::{offset::TimeZone, DateTime, Datelike, Utc};
use serde::{Deserialize, Serialize};
#[derive(Debug, Clone, Serialize, Deserialize, PartialEq, Eq)]
pub enum ReportState {
    First,
    NotFirst,
    AlreadyTwo,
    Approved,
}

#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct Report {
    pub date: DateTime<Utc>,
    pub employer: String,
    pub crp: String,
    pub vr_counselor_support_coordinator: String,
    pub supervisor: String,
    pub observations: [Observation; 2],
    pub overall_appraisal: String,
    pub problems: String,
    pub interventions_used: String,
    pub recommended_interventions: String,
    pub req_counselor_contact: bool,
    pub crp_signature: String,
    pub crp_signature_date: chrono::DateTime<chrono::Utc>,
}

#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct ReportBuilder {
    start_date: DateTime<Utc>,
    pub employer: String,
    pub crp: String,
    pub vr_counselor_support_coordinator: String,
    pub supervisor: String,
    pub req_counselor_contact: bool,
    pub observations: Vec<Observation>,
}

#[derive(Debug, Clone, Serialize, Deserialize)]
pub enum ReportBuilderError {
    AlreadyTwoObservations,
    NotEnoughObservations,
}

impl ReportBuilder {
    pub fn new(
        employer: String,
        crp: String,
        vr_counselor_support_coordinator: String,
        supervisor: String,
        req_counselor_contact: bool,
        time_zone: chrono_tz::Tz,
    ) -> Self {
        Self {
            start_date: time_zone
                .from_utc_datetime(&Utc::now().naive_utc())
                .date()
                .and_hms(0, 0, 0)
                .with_timezone(&Utc),
            employer: employer,
            crp: crp,
            req_counselor_contact: req_counselor_contact,
            vr_counselor_support_coordinator: vr_counselor_support_coordinator,
            supervisor: supervisor,
            observations: Vec::new(),
        }
    }
    pub fn add_observation(&mut self, observation: Observation) -> Result<(), ReportBuilderError> {
        if self.observations.len() < 2 {
            self.observations.push(observation);
            Ok(())
        } else {
            Err(ReportBuilderError::AlreadyTwoObservations)
        }
    }
    pub fn is_overdue(&self, time_zone: chrono_tz::Tz) -> bool {
        let now = time_zone.from_utc_datetime(&Utc::now().naive_utc());
        let then = time_zone.from_utc_datetime(&self.start_date.naive_utc());
        let now_tuple = (now.month(), now.year());
        let then_tuple = (then.month(), then.year());
        if now_tuple != then_tuple {
            if now.day() > 15 {
                return true;
            }
        }
        false
    }
    pub fn build(
        &self,
        overall_appraisal: String,
        problems: String,
        interventions_used: String,
        recommended_interventions: String,
        req_counselor_contact: bool,
        crp_signature: String,
    ) -> Result<Report, ReportBuilderError> {
        if self.observations.len() == 2 {
            Ok(Report {
                date: self.start_date.clone(),
                employer: self.employer.clone(),
                crp: self.crp.clone(),
                vr_counselor_support_coordinator: self.vr_counselor_support_coordinator.clone(),
                supervisor: self.supervisor.clone(),
                observations: [self.observations[0].clone(), self.observations[1].clone()],
                overall_appraisal: overall_appraisal,
                problems: problems,
                interventions_used: interventions_used,
                recommended_interventions: recommended_interventions,
                req_counselor_contact: req_counselor_contact,
                crp_signature: crp_signature,
                crp_signature_date: Utc::now(),
            })
        } else {
            Err(ReportBuilderError::NotEnoughObservations)
        }
    }
}
#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct Observation {
    pub date: chrono::DateTime<chrono::Utc>,
    pub attendance: Rating,
    pub time_management: Rating,
    pub appearance: Rating,
    pub communication: Rating,
    pub jt_completion_rate: Rating,
    pub jt_quality: Rating,
}
