use chrono::{DateTime, Datelike, Utc};
use serde::{Deserialize, Serialize};
use std::fmt;

#[derive(Serialize, Deserialize, Debug, Clone)]
pub struct ReportRow {
    pub day: String,
    pub job_coach_username: String,
    pub hours: String,
    pub primary: Vec<CoachingServices>,
    pub secondary: Vec<CoachingServices>,
}

#[derive(Serialize, Deserialize, Debug, Clone)]
pub enum CoachingServices {
    S1,
    S2,
    S3,
    S4,
    S5,
    S6,
    S7,
    S8,
    S9,
    S10,
    S11,
    S12,
    S13,
    S14,
    S15,
}

impl CoachingServices {
    pub fn list_all() -> Vec<Self> {
        vec![
            Self::S1,
            Self::S2,
            Self::S3,
            Self::S4,
            Self::S5,
            Self::S6,
            Self::S7,
            Self::S8,
            Self::S9,
            Self::S10,
            Self::S11,
            Self::S12,
            Self::S13,
            Self::S14,
            Self::S15,
        ]
    }
    pub fn from(input: String) -> Self {
        if input.to_lowercase() == "s1" {
            Self::S1
        } else if input.to_lowercase() == "s2" {
            Self::S2
        } else if input.to_lowercase() == "s3" {
            Self::S3
        } else if input.to_lowercase() == "s4" {
            Self::S4
        } else if input.to_lowercase() == "s5" {
            Self::S5
        } else if input.to_lowercase() == "s6" {
            Self::S6
        } else if input.to_lowercase() == "s7" {
            Self::S7
        } else if input.to_lowercase() == "s8" {
            Self::S8
        } else if input.to_lowercase() == "s9" {
            Self::S9
        } else if input.to_lowercase() == "s10" {
            Self::S10
        } else if input.to_lowercase() == "s11" {
            Self::S11
        } else if input.to_lowercase() == "s12" {
            Self::S12
        } else if input.to_lowercase() == "s13" {
            Self::S13
        } else if input.to_lowercase() == "s14" {
            Self::S14
        } else {
            Self::S15
        }
    }
}

impl std::fmt::Display for CoachingServices {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match self {
            Self::S1 => write!(f, "1. Attend employer training (client and job coach)"),
            Self::S2 => write!(f, "2. Meet with worksite sups and natural supports"),
            Self::S3 => write!(
                f,
                "3. Review, train, teach essential job duties with client"
            ),
            Self::S4 => write!(
                f,
                "4. Provide individualized training for learning job tasks"
            ),
            Self::S5 => write!(f, "5. Perform onsite follow-up checks with client"),
            Self::S6 => write!(f, "6. Provide direct interventions on the job"),
            Self::S7 => write!(f, "7. Identify and set up accommodations (employer & VR)"),
            Self::S8 => write!(
                f,
                "8. Build and coordinate natural supports for continued work success"
            ),
            Self::S9 => write!(f, "9. Shadow and observe client while on worksite"),
            Self::S10 => write!(
                f,
                "10. Develop and implement support plan after job coach fades"
            ),
            Self::S11 => write!(
                f,
                "11. Develop work culture skills (breaks, sick days, etc.)"
            ),
            Self::S12 => write!(f, "12. Develop work conditioning and hardening"),
            Self::S13 => write!(f, "13. Provide support and encouragement"),
            Self::S14 => write!(
                f,
                "14. Provide *Other Support. *(Approved in advance by VR)"
            ),
            Self::S15 => write!(f, "15. Provide transportation training"),
        }
    }
}

#[derive(Serialize, Deserialize, Debug, Clone)]
pub struct Report {
    pub client_uuid: String,
    pub auth_number: String,
    pub vr_counselor_support_coordinator_uuid: String,
    pub coaching_hours: String,
    pub independent_hours: String,
    pub hours_used: String,
    pub hours_remaining: String,
    pub employer: String,
    pub ratio_percentage: String,
    pub originally_authorized_hours: String,
    pub month_year: String,
    pub report_rows: Vec<ReportRow>,
}

#[derive(Serialize, Deserialize, Debug, Clone)]
pub struct ReportBuilder {
    pub start_date: DateTime<Utc>,
    pub report_rows: Vec<ReportRow>,
    pub local_tz: chrono_tz::Tz,
}

#[derive(Serialize, Deserialize, Debug, Clone)]
pub enum ReportError {
    NoSuchDay,
}

impl ReportBuilder {
    pub fn new(local_tz: chrono_tz::Tz) -> Self {
        Self {
            start_date: Utc::now(),
            report_rows: Vec::new(),
            local_tz: local_tz,
        }
    }
    pub fn add_row(
        &mut self,
        day: usize,
        employee_username: String,
        hours: String,
        primary: Vec<CoachingServices>,
        secondary: Vec<CoachingServices>,
    ) -> Result<(), ReportError> {
        let date = self.start_date.with_timezone(&self.local_tz);
        let naive_date = chrono::NaiveDate::from_ymd(date.year(), date.month(), date.day());
        let end_date: chrono::NaiveDate;
        if naive_date.month() == 12 {
            end_date = chrono::NaiveDate::from_ymd(naive_date.year() + 1, 1, 1);
        } else {
            end_date = chrono::NaiveDate::from_ymd(naive_date.year(), naive_date.month() + 1, 1);
        }
        let num_days_month: usize = end_date
            .signed_duration_since(chrono::NaiveDate::from_ymd(date.year(), date.month(), 1))
            .num_days()
            .try_into()
            .unwrap();
        if !(day > 0 && day <= num_days_month) {
            return Err(ReportError::NoSuchDay);
        }
        self.report_rows.push(ReportRow {
            day: day.to_string(),
            job_coach_username: employee_username,
            hours: hours,
            primary: primary,
            secondary: secondary,
        });
        Ok(())
    }
    //pub fn build(&self, client_uuid: String, auth_number: String, vr_counselor_support_coordinator_uuid: String, employer: String, ratio_percentage: String, originally_authorized_hours: String) -> Report {
    //
    //}
}

#[derive(Serialize, Deserialize, Debug, Clone)]
pub struct ClientScheduleRow {
    pub start_hour: usize,
    pub start_minute: usize,
    pub start_am: bool,
    pub end_hour: usize,
    pub end_minute: usize,
    pub end_am: bool,
    //pub label: String,
}
